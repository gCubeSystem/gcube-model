package org.gcube.resourcemanagement.model;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.discovery.knowledge.Knowledge;
import org.gcube.informationsystem.discovery.knowledge.ModelKnowledge;
import org.gcube.informationsystem.tree.Tree;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class DiscoveryTest {

	private static final Logger logger = LoggerFactory.getLogger(DiscoveryTest.class);
	
	
	@Test
	public void testDiscovery() throws Exception {
		ModelKnowledge modelKnowledge = Knowledge.getInstance().getAllKnowledge();
		
		AccessType[] accessTypes = AccessType.getModelTypes();
		for(AccessType accessType : accessTypes) {
			
			Tree<Class<Element>> classesTree = modelKnowledge.getClassesTree(accessType);
			logger.info("Classes tree for {} is\n{}", accessType.getName(), classesTree.toString());
			
		}
	}
	
}
