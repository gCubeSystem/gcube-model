package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.core.JsonGenerationException;
import org.gcube.com.fasterxml.jackson.core.JsonParseException;
import org.gcube.com.fasterxml.jackson.databind.JsonMappingException;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.entities.FacetType;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.CPUFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.ContactFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Serializer {
	
	private static Logger logger = LoggerFactory.getLogger(Serializer.class);
	
	@Test
	public void serializeAccessPoint() throws Exception {
		logger.trace(TypeMapper.serializeType(AccessPointFacet.class));
	}
	
	@Test
	public void serializeFacet() throws JsonGenerationException, JsonMappingException, IOException {
		CPUFacetImpl cpuFacetImpl = new CPUFacetImpl();
		cpuFacetImpl.setClockSpeed("1 GHz");
		cpuFacetImpl.setModel("Opteron");
		cpuFacetImpl.setVendor("AMD");
		
		cpuFacetImpl.setAdditionalProperty("Test", "MyTest");
		cpuFacetImpl.setAdditionalProperty("Other", 1);
		cpuFacetImpl.setAdditionalProperty("MYLong", 3.56);
		
		StringWriter stringWriter = new StringWriter();
		ElementMapper.marshal(cpuFacetImpl, stringWriter);
		logger.trace(stringWriter.toString());
		
		StringReader stringReader = new StringReader(stringWriter.toString());
		CPUFacet cpuFacet = ElementMapper.unmarshal(CPUFacet.class, stringReader);
		logger.trace("Deserialized : {} ", cpuFacet);
	}
	
	@Test
	public void testArraySerialization() throws Exception {
		CPUFacetImpl cpuFacetImpl = new CPUFacetImpl();
		cpuFacetImpl.setClockSpeed("1 GHz");
		cpuFacetImpl.setModel("Opteron");
		cpuFacetImpl.setVendor("AMD");
		
		cpuFacetImpl.setAdditionalProperty("Test", "MyTest");
		cpuFacetImpl.setAdditionalProperty("Other", 1);
		cpuFacetImpl.setAdditionalProperty("MYLong", 3.56);
		
		logger.debug(ElementMapper.marshal(cpuFacetImpl));
		
		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");
		logger.debug(ElementMapper.marshal(cpuFacet));
		
		ContactFacet contactFacet = new ContactFacetImpl();
		contactFacet.setName("Luca");
		contactFacet.setSurname("Frosini");
		contactFacet.setEMail("luca.frosini@isti.cnr.it");
		logger.debug(ElementMapper.marshal(contactFacet));
		
		List<Facet> list = new ArrayList<>();
		list.add(cpuFacetImpl);
		list.add(cpuFacet);
		list.add(contactFacet);
		
		logger.debug(ElementMapper.marshal(list));
		
		Facet[] array = new Facet[] {cpuFacetImpl, cpuFacet, contactFacet};
		logger.debug(ElementMapper.marshal(array));
	}
	
	@Test
	public void serializeDeserializeResource() throws JsonGenerationException, JsonMappingException, IOException {
		HostingNode hostingNode = new HostingNodeImpl();
		
		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");
		
		ContactFacet contactFacet = new ContactFacetImpl();
		contactFacet.setName("Luca");
		contactFacet.setSurname("Frosini");
		contactFacet.setEMail("luca.frosini@isti.cnr.it");
		
		hostingNode.addFacet(cpuFacet);
		hostingNode.addFacet(contactFacet);
		
		StringWriter stringWriter = new StringWriter();
		ElementMapper.marshal(hostingNode, stringWriter);
		logger.trace(stringWriter.toString());
		
		StringReader stringReader = new StringReader(stringWriter.toString());
		
		HostingNode hn = ElementMapper.unmarshal(HostingNode.class, stringReader);
		
		logger.trace("Deserialized : {} ", hn);
		
	}
	
	@Test
	public void serializeRelation() throws JsonGenerationException, JsonMappingException, IOException {
		HostingNode hostingNode = new HostingNodeImpl();
		
		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");
		
		ConsistsOf<Resource,Facet> consistsOf = new ConsistsOfImpl<Resource,Facet>(hostingNode, cpuFacet, null);
		StringWriter stringWriter = new StringWriter();
		ElementMapper.marshal(consistsOf, stringWriter);
		logger.trace(stringWriter.toString());
	}
	
	@Test
	public void deserializeResource() throws JsonGenerationException, JsonMappingException, IOException {
		
		StringReader stringReader = new StringReader("{\"" + Element.TYPE_PROPERTY + "\":\"HostingNode\"," + "\"metadata\":null,"
				+ "\"consistsOf\":[{" + "\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\"," + "\"metadata\":null," + "\"target\":{"
				+ "\"" + Element.TYPE_PROPERTY + "\":\"CPUFacet\"," + "\"metadata\":null," + "\"model\":\"Opteron\"," + "\"vendor\":\"AMD\","
				+ "\"clockSpeed\":\"1 GHz\"" + "}," + "\"relationProperty\":null" + "}," + "{"
				+ "\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\"," + "\"metadata\":null," + "\"target\":{" + "\"" + Element.TYPE_PROPERTY + "\":\"ContactFacet\","
				+ "\"metadata\":null," + "\"title\":null," + "\"name\":\"Luca\"," + "\"middleName\":null,"
				+ "\"surname\":\"Frosini\"," + "\"eMail\":\"luca.frosini@isti.cnr.it\"" + "},"
				+ "\"relationProperty\":null" + "}" + "]," + "\"isRelatedTo\":[]" + "}");
		
		HostingNode hn = ElementMapper.unmarshal(HostingNode.class, stringReader);
		
		logger.trace("Deserialized : {} ", hn);
		
	}
	
	@Test
	public void deserializeContext() throws JsonParseException, JsonMappingException, IOException {
		String contextString = "{\"name\":\"gcube\",\"" + IdentifiableElement.ID_PROPERTY + "\":\"fe44822a-d8bb-418b-ba79-59b4aef01cf9\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":\"UNKNOWN_USER\",\"creationTime\":\"2019-02-06 11:08:33.706 +0100\",\"modifiedBy\":\"UNKNOWN_USER\",\"lastUpdateTime\":\"2019-02-06 11:08:33.706 +0100\"},\"" + Element.TYPE_PROPERTY + "\":\"Context\",\"children\":[{\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":\"UNKNOWN_USER\",\"creationTime\":\"2019-02-06 11:08:34.627 +0100\",\"modifiedBy\":\"UNKNOWN_USER\",\"lastUpdateTime\":\"2019-02-06 11:08:34.627 +0100\"},\"propagationConstraint\":{\"" + Element.TYPE_PROPERTY + "\":\"PropagationConstraint\",\"add\":\"unpropagate\",\"remove\":\"keep\"},\"" + Element.TYPE_PROPERTY + "\":\"IsParentOf\",\"target\":{\"name\":\"devsec\",\"" + IdentifiableElement.ID_PROPERTY + "\":\"007f9154-25fa-4f52-9cd4-ec1f8c3c3baf\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":\"UNKNOWN_USER\",\"creationTime\":\"2019-02-06 11:08:34.663 +0100\",\"modifiedBy\":\"UNKNOWN_USER\",\"lastUpdateTime\":\"2019-02-06 11:08:34.663 +0100\"},\"" + Element.TYPE_PROPERTY + "\":\"Context\"}},{\"" + IdentifiableElement.ID_PROPERTY + "\":\"aeec2ea9-c3d2-43af-998e-d19953fc2c42\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":\"UNKNOWN_USER\",\"creationTime\":\"2019-02-06 11:08:36.658 +0100\",\"modifiedBy\":\"UNKNOWN_USER\",\"lastUpdateTime\":\"2019-02-06 11:08:36.658 +0100\"},\"propagationConstraint\":{\"" + Element.TYPE_PROPERTY + "\":\"PropagationConstraint\",\"add\":\"unpropagate\",\"remove\":\"keep\"},\"" + Element.TYPE_PROPERTY + "\":\"IsParentOf\",\"target\":{\"name\":\"devNext\",\"" + IdentifiableElement.ID_PROPERTY + "\":\"b16bc587-3fd8-4c0a-9196-8515c4501649\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":\"UNKNOWN_USER\",\"creationTime\":\"2019-02-06 11:08:36.695 +0100\",\"modifiedBy\":\"UNKNOWN_USER\",\"lastUpdateTime\":\"2019-02-06 11:08:36.695 +0100\"},\"" + Element.TYPE_PROPERTY + "\":\"Context\"}},{\"" + IdentifiableElement.ID_PROPERTY + "\":\"eba6cd76-525c-4037-9095-a712b054fd1b\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":\"UNKNOWN_USER\",\"creationTime\":\"2019-02-06 11:08:38.224 +0100\",\"modifiedBy\":\"UNKNOWN_USER\",\"lastUpdateTime\":\"2019-02-06 11:08:38.224 +0100\"},\"propagationConstraint\":{\"" + Element.TYPE_PROPERTY + "\":\"PropagationConstraint\",\"add\":\"unpropagate\",\"remove\":\"keep\"},\"" + Element.TYPE_PROPERTY + "\":\"IsParentOf\",\"target\":{\"name\":\"preprod\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":\"UNKNOWN_USER\",\"creationTime\":\"2019-02-06 11:08:38.248 +0100\",\"modifiedBy\":\"UNKNOWN_USER\",\"lastUpdateTime\":\"2019-02-06 11:08:38.248 +0100\"},\"" + Element.TYPE_PROPERTY + "\":\"Context\"}}]}";
		logger.trace("Source Context String {}",contextString);
		StringReader stringReader = new StringReader(contextString);
		Context c = ElementMapper.unmarshal(Context.class, stringReader);
		logger.trace("Deserialized Context : {} ", c);
	}
	
	
	@Test
	public void testTypeSerialization() throws Exception {
		String serialized = TypeMapper.serializeType(IdentifierFacet.class);
		logger.info(serialized);
		FacetType propertyType = (FacetType) TypeMapper.deserializeTypeDefinition(serialized);
		Version typeVersion = propertyType.getVersion();
		logger.debug("Version {}", typeVersion.toString());
		logger.info(ElementMapper.marshal(propertyType));
	}
	
	
	@Test
	public void testEventFacetTypeSerialization() throws Exception {
		String serialized = TypeMapper.serializeType(EventFacet.class);
		logger.info(serialized);
		FacetType propertyType = (FacetType) TypeMapper.deserializeTypeDefinition(serialized);
		logger.info(ElementMapper.marshal(propertyType));
	}

	@Test
	public void testDiscoveryFacetTypeSerialization() throws Exception {
		String serialized = TypeMapper.serializeType(DiscoveryFacet.class);
		logger.info(serialized);
		FacetType propertyType = (FacetType) TypeMapper.deserializeTypeDefinition(serialized);
		logger.info(ElementMapper.marshal(propertyType));
	}
	

}
