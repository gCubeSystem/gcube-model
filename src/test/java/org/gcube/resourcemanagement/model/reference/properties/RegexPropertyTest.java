package org.gcube.resourcemanagement.model.reference.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.gcube.resourcemanagement.model.impl.properties.RegexPropertyImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

/**
 * Test cases for {@link RegexProperty}.
 * 
 * @author Manuele Simi (ISTI CNR)
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class RegexPropertyTest {

	RegexProperty property = new RegexPropertyImpl();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		String patternString = ".*https://.*";
        property.setSchema(patternString);
	}

	/**
	 * Test method for {@link org.gcube.resourcemanagement.model.reference.properties.RegexProperty#validate()}.
	 */
	@Test
	public void testValidate() {
		 String value =
		            "This is the text to be searched " +
		            "for occurrences of the https:// pattern.";
		property.setValue(value);
		assertTrue("Text was not recognized as a valid pattern. But it is!", property.validate().isSuccess());
	}
	
	/**
	 * Test method for {@link org.gcube.resourcemanagement.model.reference.properties.RegexProperty#validate()}.
	 */
	@Test
	public void testNotValidate() {
		String value = "This text does not have the pattern.";
		property.setValue(value);
		assertFalse("Text was recognized as a valid pattern. But it is NOT!", property.validate().isSuccess());
	}
	/**
	 * Test method for {@link org.gcube.resourcemanagement.model.reference.properties.utilities.TypedProperty#getValue()}.
	 */
	@Test
	public void testGetValue() {
		String value = "This text is the value.";
		property.setValue(value);
		assertEquals("Invalid value returned.", value, property.getValue());
	}

}
