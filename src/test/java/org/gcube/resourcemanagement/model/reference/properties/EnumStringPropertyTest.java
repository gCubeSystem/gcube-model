package org.gcube.resourcemanagement.model.reference.properties;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;

import org.gcube.resourcemanagement.model.impl.properties.EnumStringPropertyImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

/**
 * Test cases for {@link EnumStringProperty}.
 * 
 * @author Manuele Simi (ISTI CNR)
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class EnumStringPropertyTest {

	private EnumStringProperty property = new EnumStringPropertyImpl();
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		property.setSchema(new HashSet<String>(Arrays.asList("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT")));
	}

	/**
	 * Test method for {@link org.gcube.resourcemanagement.model.reference.properties.EnumStringProperty#validate()}.
	 */
	@Test
	public void testValidate() {
		property.setValue("MON");
		assertTrue("MON is not a valid day", property.validate().isSuccess());
	}
	
	/**
	 * Test method for {@link org.gcube.resourcemanagement.model.reference.properties.EnumStringProperty#validate()}.
	 */
	@Test
	public void testNotValidate() {
		property.setValue("BON");
		assertFalse("BON was recognized as valid day", property.validate().isSuccess());
	}

	/**
	 * Test method for {@link org.gcube.resourcemanagement.model.reference.properties.utilities.TypedProperty#setValue(java.lang.Object)}.
	 */
	@Test
	public void testSetValue() {
		property.setValue("MON");
		assertTrue("MON is not set", property.getValue() == "MON");
	}

}
