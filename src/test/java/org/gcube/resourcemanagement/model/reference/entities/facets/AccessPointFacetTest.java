package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.net.URI;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.impl.properties.EncryptedImpl;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.properties.Encrypted;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.PropertyTypeName.BaseType;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.resourcemanagement.model.impl.entities.facets.AccessPointFacetImpl;
import org.gcube.resourcemanagement.model.impl.properties.ValueSchemaImpl;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AccessPointFacetTest {
	
	private static Logger logger = LoggerFactory.getLogger(ContactFacetImplTest.class);
	
	@Test
	public void serializeDeserialize() throws Exception {
		
		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		accessPointFacet.setEndpoint(new URI("https://localhost"));
		accessPointFacet.setEntryName("port1");

		ValueSchema authorization = new ValueSchemaImpl();
		authorization.setValue("pwd");
		URI uri = new URI("https://www.gcube-system.org");
		authorization.setSchema(uri);
		accessPointFacet.setAuthorization(authorization);
		
		accessPointFacet.setAdditionalProperty("Test", "MyTest");
		
		Encrypted vault = new EncryptedImpl();
		vault.setValue("Encrypted");
		accessPointFacet.setAdditionalProperty("Enc", vault);
		
		String marshalled = ElementMapper.marshal(accessPointFacet);
		logger.debug(marshalled);
		
		AccessPointFacet apf = ElementMapper.unmarshal(AccessPointFacet.class, marshalled);
		Encrypted vlt = (Encrypted) apf.getAdditionalProperty("Enc");
		logger.debug(ElementMapper.marshal(vlt));
		String reMarshalled = ElementMapper.marshal(apf);
		logger.debug(reMarshalled);
		
	}
	
	@Test
	public void testEncryptedSpecilization() throws Exception {
		String marshalled = "{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"" + IdentifiableElement.ID_PROPERTY + "\":null,\"metadata\":null,\"entryName\":\"port1\",\"endpoint\":\"https://localhost\",\"protocol\":null,\"description\":null,\"authorization\":{\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"pwd\",\"" + Element.TYPE_PROPERTY + "\":\"https://www.gcube-system.org\"},\"Test\":\"MyTest\",\"Enc\":{\"" + Element.TYPE_PROPERTY + "\":\"MyEncrypted\",\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"Encrypted\", \"Property\"],\"value\":\"Encrypted\"}}";
		AccessPointFacet apf = ElementMapper.unmarshal(AccessPointFacet.class, marshalled);
		Encrypted vlt = (Encrypted) apf.getAdditionalProperty("Enc");
		logger.debug(ElementMapper.marshal(vlt));
		String reMarshalled = ElementMapper.marshal(apf);
		logger.debug(reMarshalled);
	}

	@Test
	public void testTypeDefinition() throws Exception {
		Class<? extends Element> clz = AccessPointFacet.class;
		Type type = TypeMapper.createTypeDefinition(clz);
		Assert.assertTrue(type.getName().compareTo(AccessPointFacet.NAME)==0);
		for(PropertyDefinition propertyDefinition : type.getProperties()) {
			if(propertyDefinition.getName().compareTo(AccessPointFacet.AUTHORIZATION_PROPERTY)==0) {
				Assert.assertTrue(propertyDefinition.getPropertyType().compareTo(BaseType.PROPERTY.toString())==0);
				logger.debug("{}", propertyDefinition);
			}
		}
		String typeDefinitionJsonString = TypeMapper.serializeTypeDefinition(type);
		logger.debug(typeDefinitionJsonString);
	}
	
}
