package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.resourcemanagement.model.reference.properties.AccessPolicy;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceTypeDefintionsTest {

	private static final Logger logger = LoggerFactory.getLogger(ResourceTypeDefintionsTest.class);
	
	@Test
	public void serialize() throws Exception{
		TypeMapper.serializeType(EntityElement.class);
		TypeMapper.serializeType(Entity.class);
		TypeMapper.serializeType(Resource.class);
		TypeMapper.serializeType(GCubeResource.class);
		TypeMapper.serializeType(Actor.class);
		TypeMapper.serializeType(Dataset.class);
	}
	
	@Test
	public void testEntityTypeDefinition() throws Exception {
		Class<? extends Element> clz = EntityElement.class;
		Type type = TypeMapper.createTypeDefinition(clz);
		Assert.assertTrue(type.getName().compareTo(EntityElement.NAME)==0);
		for(PropertyDefinition propertyDefinition : type.getProperties()) {
			if(propertyDefinition.getName().compareTo(EntityElement.METADATA_PROPERTY)==0) {
				Assert.assertTrue(propertyDefinition.getPropertyType().compareTo(Metadata.NAME)==0);
				logger.debug("{}", propertyDefinition);
			}
		}
		String typeDefinitionJsonString = TypeMapper.serializeTypeDefinition(type);
		logger.debug(typeDefinitionJsonString);
	}
	
	@Test
	public void testPropertyTypeDefinition() throws Exception {
		Class<? extends Element> clz = AccessPolicy.class;
		Type type = TypeMapper.createTypeDefinition(clz);
		Assert.assertTrue(type.getName().compareTo(AccessPolicy.NAME)==0);
		for(PropertyDefinition propertyDefinition : type.getProperties()) {
			if(propertyDefinition.getName().compareTo("policy")==0) {
				Assert.assertTrue(propertyDefinition.getPropertyType().compareTo(ValueSchema.NAME)==0);
				logger.debug("{}", propertyDefinition);
			}
		}
		String typeDefinitionJsonString = TypeMapper.serializeTypeDefinition(type);
		logger.debug(typeDefinitionJsonString);
	}
}
