/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.core.JsonParseException;
import org.gcube.com.fasterxml.jackson.databind.JsonMappingException;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.impl.properties.EventImpl;
import org.gcube.informationsystem.model.impl.properties.MetadataImpl;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Event;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.contexts.impl.entities.GCubeContext;
import org.gcube.resourcemanagement.contexts.impl.properties.BasicInformation;
import org.gcube.resourcemanagement.model.impl.entities.resources.EServiceImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.ActivatesImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PolymorphismTest {

	private static Logger logger = LoggerFactory
			.getLogger(PolymorphismTest.class);
	
	public static final String NYESERVICE = "{\"" + Element.TYPE_PROPERTY + "\":\"EService\",\"" + IdentifiableElement.ID_PROPERTY + "\":\"3ace4bd0-e5cd-49a3-97a8-a0a9468ce6d4\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":null, \"creationTime\":null, \"lastUpdateTime\":null},\"consistsOf\":[{\"" + Element.TYPE_PROPERTY + "\":\"IsIdentifiedBy\",\"metadata\":null,\"target\":{\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"SoftwareFacet\", \"Facet\", \"Entity\"],\"" + Element.TYPE_PROPERTY + "\":\"MySoftwareFacet\",\"metadata\":null,\"name\":\"WhnManager\",\"group\":\"VREManagement\",\"version\":\"2.0.0-SNAPSHOT\",\"description\":\"Web Hosting Node Service\",\"qualifier\":null,\"optional\":false}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"whnmanager\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/vremanagement/ws/whnmanager\",\"protocol\":null,\"description\":null,\"authorization\": {\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"WhnManager-remote-management\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/resource\",\"protocol\":null,\"description\":null,\"authorization\":{\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}}],\"isRelatedTo\":[]}";
	public static final String MYESERVICE = "{\"" + Element.TYPE_PROPERTY + "\":\"EService\",\"" + IdentifiableElement.ID_PROPERTY + "\":\"3ace4bd0-e5cd-49a3-97a8-a0a9468ce6d4\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":null, \"creationTime\":null, \"lastUpdateTime\":null},\"consistsOf\":[{\"" + Element.TYPE_PROPERTY + "\":\"IsIdentifiedBy\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"MySoftwareFacet\",\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"SoftwareFacet\", \"Facet\", \"Entity\"],\"metadata\":null,\"name\":\"WhnManager\",\"group\":\"VREManagement\",\"version\":\"2.0.0-SNAPSHOT\",\"description\":\"Web Hosting Node Service\",\"qualifier\":null,\"optional\":false}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"Facet\", \"Entity\"],\"metadata\":null,\"entryName\":\"whnmanager\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/vremanagement/ws/whnmanager\",\"protocol\":null,\"description\":null,\"authorization\": {\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"WhnManager-remote-management\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/resource\",\"protocol\":null,\"description\":null,\"authorization\":{\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}}],\"isRelatedTo\":[]}";
	public static final String MYOTHERESERVICE = "{\"" + Element.TYPE_PROPERTY + "\":\"EService\",\"" + IdentifiableElement.ID_PROPERTY + "\":\"3ace4bd0-e5cd-49a3-97a8-a0a9468ce6d4\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":null, \"creationTime\":null, \"lastUpdateTime\":null},\"consistsOf\":[{\"" + Element.TYPE_PROPERTY + "\":\"IsIdentifiedBy\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"MySoftwareFacet\",\"metadata\":null,\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"SoftwareFacet\", \"Facet\", \"Entity\"],\"name\":\"WhnManager\",\"group\":\"VREManagement\",\"version\":\"2.0.0-SNAPSHOT\",\"description\":\"Web Hosting Node Service\",\"qualifier\":null,\"optional\":false}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"whnmanager\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/vremanagement/ws/whnmanager\",\"protocol\":null,\"description\":null,\"authorization\": {\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"WhnManager-remote-management\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/resource\",\"protocol\":null,\"description\":null,\"authorization\":{\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}}],\"isRelatedTo\":[]}";
	public static final String MYANOTHERESERVICE = "{\"" + Element.TYPE_PROPERTY + "\":\"EService\",\"" + IdentifiableElement.ID_PROPERTY + "\":\"3ace4bd0-e5cd-49a3-97a8-a0a9468ce6d4\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":null, \"creationTime\":null, \"lastUpdateTime\":null},\"consistsOf\":[{\"" + Element.TYPE_PROPERTY + "\":\"IsIdentifiedBy\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"MySoftwareFacet\",\"metadata\":null,\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"SoftwareFacet\", \"Facet\", \"Entity\"],\"name\":\"WhnManager\",\"group\":\"VREManagement\",\"version\":\"2.0.0-SNAPSHOT\",\"description\":\"Web Hosting Node Service\",\"qualifier\":null,\"optional\":false}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"whnmanager\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/vremanagement/ws/whnmanager\",\"protocol\":null,\"description\":null,\"authorization\": {\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"WhnManager-remote-management\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/resource\",\"protocol\":null,\"description\":null,\"authorization\":{\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}}],\"isRelatedTo\":[]}";
	
	public static final String NYESERVICE2 = "{\"" + Element.TYPE_PROPERTY + "\":\"MyEService\",\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"EService\",\"Service\",\"Resource\"],\"" + IdentifiableElement.ID_PROPERTY + "\":\"3ace4bd0-e5cd-49a3-97a8-a0a9468ce6d4\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":null, \"creationTime\":null, \"lastUpdateTime\":null},\"consistsOf\":[{\"" + Element.TYPE_PROPERTY + "\":\"IsIdentifiedBy\",\"metadata\":null,\"target\":{\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"SoftwareFacet\", \"Facet\", \"Entity\"],\"" + Element.TYPE_PROPERTY + "\":\"MySoftwareFacet\",\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"SoftwareFacet\", \"Facet\", \"Entity\"],\"metadata\":null,\"name\":\"WhnManager\",\"group\":\"VREManagement\",\"version\":\"2.0.0-SNAPSHOT\",\"description\":\"Web Hosting Node Service\",\"qualifier\":null,\"optional\":false}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"whnmanager\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/vremanagement/ws/whnmanager\",\"protocol\":null,\"description\":null,\"authorization\": {\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"WhnManager-remote-management\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/resource\",\"protocol\":null,\"description\":null,\"authorization\":{\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}}],\"isRelatedTo\":[]}";
	public static final String MYANOTHERESERVICE2 = "{\"" + Element.TYPE_PROPERTY + "\":\"MyEService\",\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"EService\",\"Service\",\"Resource\"],\"" + IdentifiableElement.ID_PROPERTY + "\":\"3ace4bd0-e5cd-49a3-97a8-a0a9468ce6d4\",\"metadata\":{\"" + Element.TYPE_PROPERTY + "\":\"Metadata\",\"creator\":null, \"creationTime\":null, \"lastUpdateTime\":null},\"consistsOf\":[{\"" + Element.TYPE_PROPERTY + "\":\"IsIdentifiedBy\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"MySoftwareFacet\",\""+ ModelElement.SUPERTYPES_PROPERTY + "\":[\"SoftwareFacet\", \"Facet\", \"Entity\"],\"metadata\":null,\"name\":\"WhnManager\",\"group\":\"VREManagement\",\"version\":\"2.0.0-SNAPSHOT\",\"description\":\"Web Hosting Node Service\",\"qualifier\":null,\"optional\":false}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"whnmanager\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/vremanagement/ws/whnmanager\",\"protocol\":null,\"description\":null,\"authorization\": {\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}},{\"" + Element.TYPE_PROPERTY + "\":\"ConsistsOf\",\"metadata\":null,\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"AccessPointFacet\",\"metadata\":null,\"entryName\":\"WhnManager-remote-management\",\"endpoint\":\"https://pc-frosini.isti.cnr.it:8080/whn-manager/gcube/resource\",\"protocol\":null,\"description\":null,\"authorization\":{\"" + Element.TYPE_PROPERTY + "\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null},\"properties\":null}}],\"isRelatedTo\":[]}";
	
	public static final String MY_SOFTWARE_FACET_TYPE = "MySoftwareFacetTest";
	public static final String MY_TEST_FACET = "{\"" + Element.TYPE_PROPERTY + "\":\"" + MY_SOFTWARE_FACET_TYPE + "\",\"" + ModelElement.SUPERTYPES_PROPERTY + "\":[\"SoftwareFacet\", \"Facet\" ],\"name\":\"WhnManager\",\"group\":\"VREManagement\",\"version\":\"2.0.0-SNAPSHOT\",\"description\":\"Web Hosting Node Service\",\"qualifier\":null,\"optional\":false}";
	
	public static final String MY_IS_IDENTIFIED_BY_TYPE = "MyIsIdentifiedByTest";
	public static final String MY_CONSISTS_OF = "{\"" + Element.TYPE_PROPERTY + "\":\"" + MY_IS_IDENTIFIED_BY_TYPE + "\",\"" + ModelElement.SUPERTYPES_PROPERTY + "\":[\"IsIdentifiedBy\", \"ConsistsOf\" ],\"target\":" + MY_TEST_FACET + "}";
	
	@Test
	public void testER() throws Exception {
		
		logger.debug(MY_TEST_FACET);
		Facet f = ElementMapper.unmarshal(Facet.class, MY_TEST_FACET);
		logger.debug("{} {}", f.getTypeName(), f);
		Assert.assertTrue(f.getExpectedtype()!=null);
		Assert.assertTrue(f.getExpectedtype().compareTo(MY_SOFTWARE_FACET_TYPE)==0);
		
		logger.debug(MY_CONSISTS_OF);
		ConsistsOf<?,?> c = ElementMapper.unmarshal(ConsistsOf.class, MY_CONSISTS_OF);
		logger.debug("{} {}", c.getTypeName(), c);
		Assert.assertTrue(c.getExpectedtype()!=null);
		Assert.assertTrue(c.getExpectedtype().compareTo(MY_IS_IDENTIFIED_BY_TYPE)==0);
		
		String[] eServices = new String[]{NYESERVICE, MYESERVICE, MYOTHERESERVICE, MYANOTHERESERVICE, NYESERVICE2, MYANOTHERESERVICE2};
		// String[] eServices = new String[]{NYESERVICE2, MYANOTHERESERVICE2};
		// String[] eServices = new String[]{NYESERVICE};
		// String[] eServices = new String[]{};
		for(String eService : eServices){
			logger.debug("Going to unmarshal {}", eService);
			Resource resource = ElementMapper.unmarshal(Resource.class, eService);
			logger.debug("{} {}", resource.getClass().getSimpleName(), resource);
			
			List<ConsistsOf<? extends Resource, ? extends Facet>> consistsOfs = resource.getConsistsOf();
			for(ConsistsOf<? extends Resource, ? extends Facet> consistsOf : consistsOfs){
				logger.debug("{}", consistsOf);
				Facet facet = consistsOf.getTarget();
				logger.debug("{} {}", facet.getClass().getSimpleName(), facet);
			}
		}
		
	}
	
	@Test
	public void testRelation() throws Exception {
		HostingNode hostingNode = new HostingNodeImpl();
		Metadata metadataHn = new MetadataImpl();
		hostingNode.setMetadata(metadataHn);
		
		EService eService = new EServiceImpl();
		Metadata metadataES = new MetadataImpl();
		eService.setMetadata(metadataES);
		
		Activates<HostingNode, EService> activates = new ActivatesImpl<HostingNode, EService>(hostingNode, eService, null);
		
		hostingNode.attachResource(activates);
		
		String string = ElementMapper.marshal(activates);
		logger.debug(string);
		
	}
	
	public static final String AUX = "{\"type\":\"Activates\",\"id\":\"6f727f32-4c2d-477f-997b-1f792168875f\",\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\"},\"source\":{\"type\":\"HostingNode\",\"supertypes\":[\"Service\",\"GCubeResource\",\"Resource\"],\"id\":\"1c7e980d-cbd0-4a97-a7b7-2e6f530864c4\"},\"target\":{\"type\":\"EService\",\"id\":\"18854a80-7f72-4655-82cc-22884c912bfe\",\"consistsOf\":[{\"type\":\"IsIdentifiedBy\",\"id\":null,\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\"},\"source\":{\"type\":\"EService\",\"id\":\"18854a80-7f72-4655-82cc-22884c912bfe\"},\"target\":{\"type\":\"SoftwareFacet\",\"id\":null,\"name\":\"gcat\",\"group\":\"org.gcube.data-catalogue\",\"version\":\"2.5.1-SNAPSHOT\",\"description\":\"This service allows any client to publish on the gCube Catalogue.\",\"qualifier\":null,\"optional\":false}},{\"type\":\"ConsistsOf\",\"id\":null,\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\"},\"source\":{\"type\":\"EService\",\"id\":\"18854a80-7f72-4655-82cc-22884c912bfe\"},\"target\":{\"type\":\"AccessPointFacet\",\"id\":null,\"entryName\":\"org.gcube.gcat.ResourceInitializer\",\"endpoint\":\"http://gcat.dev.int.d4science.net:80/gcat\",\"protocol\":null,\"description\":null,\"authorization\":{\"type\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null}}},{\"type\":\"ConsistsOf\",\"id\":null,\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\"},\"source\":{\"type\":\"EService\",\"id\":\"18854a80-7f72-4655-82cc-22884c912bfe\"},\"target\":{\"type\":\"AccessPointFacet\",\"id\":null,\"entryName\":\"gcat-remote-management\",\"endpoint\":\"http://gcat.dev.int.d4science.net:80/gcat/gcube/resource\",\"protocol\":null,\"description\":null,\"authorization\":{\"type\":\"ValueSchema\",\"value\":\"gcube-token\",\"schema\":null}}},{\"type\":\"ConsistsOf\",\"id\":null,\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\"},\"source\":{\"type\":\"EService\",\"id\":\"18854a80-7f72-4655-82cc-22884c912bfe\"},\"target\":{\"type\":\"StateFacet\",\"id\":null,\"value\":\"started\",\"date\":\"2023-05-15 12:36:47.299 +0200\"}},{\"type\":\"ConsistsOf\",\"id\":null,\"propagationConstraint\":{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascade\"},\"source\":{\"type\":\"EService\",\"id\":\"18854a80-7f72-4655-82cc-22884c912bfe\"},\"target\":{\"type\":\"EventFacet\",\"id\":null,\"date\":\"2023-05-15 10:36:47.299 +0000\",\"event\":\"started\"}}],\"isRelatedTo\":[]}}"; 
	
	@Test
	public void testUnmarshalling() throws JsonParseException, JsonMappingException, IOException {
		ElementMapper.unmarshal(Activates.class, AUX);
	}
	
	
	@Test
	public void testGCubeContextImplementation() throws Exception {
		Context c = new ContextImpl("test");
		c.setID(UUID.randomUUID());
		c.setState("created");
		String s = c.toString();
		logger.debug(s);
		
		c = ElementMapper.unmarshal(Context.class, s);
		logger.debug(c.toString());
		
		GCubeContext gcubeContext = new GCubeContext(c);
		Event created = new EventImpl();
		created.setWhat(c.getState());
		created.setWho("luca.frosini");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -1);
		created.setWhen(cal.getTime());
		gcubeContext.addEvent(created);
		
		
		gcubeContext.setState("pending");
		Event pending = new EventImpl();
		pending.setWhat(gcubeContext.getState());
		pending.setWho("luca.frosini");
		pending.setWhen(Calendar.getInstance().getTime());
		pending.setAdditionalProperty("report", "This is a report");
		
		
		BasicInformation info = new BasicInformation();
		info.addDesigner("luca.frosini");
		info.addManager("luca.frosini");
		info.addManager("pasquale.pagano");
		Calendar from = Calendar.getInstance();
		info.setFrom(from.getTime());
		info.setDescription("This is a test VRE");
		gcubeContext.setInformation(info);
		
		gcubeContext.addEvent(pending);
		logger.debug(gcubeContext.toString());
	}
	
}
