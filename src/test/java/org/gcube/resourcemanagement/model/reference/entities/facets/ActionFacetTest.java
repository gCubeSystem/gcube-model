package org.gcube.resourcemanagement.model.reference.entities.facets;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.impl.entities.facets.ActionFacetImpl;
import org.gcube.resourcemanagement.model.impl.properties.EnumStringPropertyImpl;
import org.gcube.resourcemanagement.model.reference.properties.EnumStringProperty;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

/**
 * Test cases for {@link ActionFacet}
 * 
 * @author Manuele Simi (ISTI CNR)
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class ActionFacetTest {

	private Set<String> values = new HashSet<>(Arrays.asList("ANSIBLE", "EXECUTABLE", "REMOTE_SERVICE", "COMMAND"));

	@Test
	public void serialize() {
		ActionFacet facet = new ActionFacetImpl();
		facet.setName("FirstAction");
		EnumStringProperty type = new EnumStringPropertyImpl();
		type.setValue("ANSIBLE");
		type.setSchema(values);
		facet.setType(type);
		facet.setSource("git@myrepo:playbook.yml");
		facet.setCommand("ansible-pull");
		facet.setOptions("playbook.yml");
		String marshalled = "";
		try {
			marshalled = ElementMapper.marshal(facet);
		} catch (JsonProcessingException e) {
			assertFalse("Failed to marshal the action.", false);
		}
		assertTrue("Unexpected content", marshalled.contains("ansible-pull"));
	}

	//@Test
	public void deserialize() {
		String marshalled = "{\"" + Element.TYPE_PROPERTY + "\":\"ActionFacet\",\"metadata\":null,\"name\":\"FirstAction\","
				+ "\"" + Element.TYPE_PROPERTY + "\":{\"" + Element.TYPE_PROPERTY + "\":\"EnumStringPropertyImpl\",\"" + Element.TYPE_PROPERTY + "\":[\"EXECUTABLE\",\"ANSIBLE\",\"COMMAND\",\"REMOTE_SERVICE\"]," 
				+ "\"value\":\"ANSIBLE\"},\"source\":\"git@myrepo:playbook.yml\",\"options\":\"playbook.yml\",\"command\":\"ansible-pull\"}";
		ActionFacet facet = null;
		try {
			facet = ElementMapper.unmarshal(ActionFacetImpl.class, marshalled);
		} catch (Exception e) {
			assertFalse("Failed to unmarshal the context.", false);
		}
		assertTrue("Unexpected content for command", facet.getCommand().compareTo("ansible-pull") == 0);
		assertTrue("Unexpected content for option", facet.getOptions().compareTo("playbook.yml") == 0);
		assertTrue("Unexpected content for TYPE", facet.getType().getValue().compareTo("ANSIBLE") == 0);
		assertTrue("Unexpected content for TYPE", facet.getSource().compareTo("git@myrepo:playbook.yml") == 0);

	}
}
