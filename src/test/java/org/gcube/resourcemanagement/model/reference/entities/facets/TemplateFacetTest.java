package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.PropertyTypeName;
import org.gcube.informationsystem.types.PropertyTypeName.BaseType;
import org.gcube.resourcemanagement.model.impl.entities.facets.TemplateFacetImpl;
import org.gcube.resourcemanagement.model.impl.properties.AttributePropertyImpl;
import org.gcube.resourcemanagement.model.reference.properties.AttributeProperty;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TemplateFacetTest {

	private static Logger logger = LoggerFactory.getLogger(TemplateFacetTest.class);
	
	@Test
	public void test() throws Exception {
		TemplateFacet templateFacet = new TemplateFacetImpl();
		templateFacet.setName("Test Template Facet");
		templateFacet.setDescription("Test Template Facet with examples for catalogue");
		
		AttributeProperty doAttributeProperty = new AttributePropertyImpl();
		String doVarName = "defaultOrganization";
		doAttributeProperty.setName(doVarName);
		doAttributeProperty.setDescription("Default Organization. The default organization should be the lower case name of the VRE (plese check thta it respect the regex");
		doAttributeProperty.setDefaultValue(null);
		doAttributeProperty.setPropertyType("String");
		doAttributeProperty.setRegexp("^[a-z\\-]{2,100}$");
		doAttributeProperty.setMin(2);
		doAttributeProperty.setMax(100);
		templateFacet.addProperty(doAttributeProperty);

		AttributeProperty soAttributeProperty = new AttributePropertyImpl();
		String soVarName = "supportedOrganizations";
		soAttributeProperty.setName(soVarName);
		soAttributeProperty.setDescription("The list of the CKAN organization were is enabled read and write in the current context. At leat it must contains the defaultOrganization");
		soAttributeProperty.setDefaultValue(null);
		soAttributeProperty.setPropertyType("Set<String>");
		soAttributeProperty.setMin(1);
		templateFacet.addProperty(soAttributeProperty);
		
		PropertyTypeName ptn = soAttributeProperty.getPropertyTypeName();
		Assert.assertTrue(ptn.getBaseType() == BaseType.SET);
		Assert.assertTrue(ptn.getGenericBaseType() == BaseType.STRING);

		AttributeProperty satAttributeProperty = new AttributePropertyImpl();
		String satVarName = "sysAdminToken";
		satAttributeProperty.setName(satVarName);
		satAttributeProperty.setDescription("The sysAdmin token to dial with CKAN");
		satAttributeProperty.setDefaultValue(null);
		satAttributeProperty.setPropertyType("Encrypted");
		templateFacet.addProperty(satAttributeProperty);
		
		AttributeProperty cuAttributeProperty = new AttributePropertyImpl();
		String cuVarName = "ckanURL";
		cuAttributeProperty.setName(cuVarName);
		cuAttributeProperty.setDescription("The CKAN URL");
		cuAttributeProperty.setDefaultValue(null);
		cuAttributeProperty.setPropertyType("String");
		templateFacet.addProperty(cuAttributeProperty);
		
		AttributeProperty suAttributeProperty = new AttributePropertyImpl();
		String suVarName = "solrURL";
		suAttributeProperty.setName(suVarName);
		suAttributeProperty.setDescription("The Solr URL");
		suAttributeProperty.setDefaultValue(null);
		suAttributeProperty.setPropertyType("String");
		templateFacet.addProperty(suAttributeProperty);
		
		AttributeProperty speAttributeProperty = new AttributePropertyImpl();
		String speVarName = "socialPostEnabled";
		speAttributeProperty.setName(speVarName);
		speAttributeProperty.setDescription("");
		speAttributeProperty.setDefaultValue(false);
		speAttributeProperty.setPropertyType("Boolean");
		templateFacet.addProperty(speAttributeProperty);
		
		AttributeProperty ntueAttributeProperty = new AttributePropertyImpl();
		String ntueVarName = "notificationToUsersEnabled";
		ntueAttributeProperty.setName(ntueVarName);
		ntueAttributeProperty.setDescription("");
		ntueAttributeProperty.setDefaultValue(true);
		ntueAttributeProperty.setPropertyType("Boolean");
		templateFacet.addProperty(ntueAttributeProperty);
		
		AttributeProperty meAttributeProperty = new AttributePropertyImpl();
		String meVarName = "moderationEnabled";
		meAttributeProperty.setName(meVarName);
		meAttributeProperty.setDescription("");
		meAttributeProperty.setDefaultValue(true);
		meAttributeProperty.setPropertyType("Boolean");
		templateFacet.addProperty(meAttributeProperty);
		
		String json = ElementMapper.marshal(templateFacet);
		logger.info("{} is {}", TemplateFacet.NAME, json);
		
		TemplateFacet gotTemplateFacet = ElementMapper.unmarshal(TemplateFacet.class, json);
		logger.info("{} {}", TemplateFacet.NAME, ElementMapper.marshal(gotTemplateFacet));

	}
	
}
