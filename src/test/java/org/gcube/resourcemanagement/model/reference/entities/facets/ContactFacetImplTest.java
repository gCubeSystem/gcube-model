package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.io.IOException;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.impl.entities.facets.ContactFacetImpl;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContactFacetImplTest {

	private static Logger logger = LoggerFactory.getLogger(ContactFacetImplTest.class);
	
	@Test
	public void serializeDeserialize() throws IOException {
		
		ContactFacetImpl contactFacet = new ContactFacetImpl();
		
		contactFacet.setName("luca.frosini");
		contactFacet.setEMail("test@d4science.org");
		
		String marshalled = ElementMapper.marshal(contactFacet);
		logger.debug(marshalled);
		
		ContactFacetImpl cf = ElementMapper.unmarshal(ContactFacetImpl.class, marshalled);
		String reMarshalled = ElementMapper.marshal(cf);
		logger.debug(reMarshalled);
		
	}
	
	@Test
	public void test() throws Exception{
		String json = "{ "
			+ "\"" + Element.TYPE_PROPERTY + "\":\"ContactFacet\","	
			+ "\"metadata\" : {"
				+ "\"" + Element.TYPE_PROPERTY + "\":\"Metadata\","
				+ "\"creator\":\"luca.frosini\""
			+ "},"
			+ "\"name\":\"Luca\","
			+ "\"eMail\":\"test@d4science.org\","
			+ "\"website\":\"https://www.d4science.org\","
			+ "\"address\":\"Via G. Moruzzi, 1\","
			+ "\"phoneNumber\":\"123456789\""
		+ "}";
		
		ContactFacet contactFacet = ElementMapper.unmarshal(ContactFacet.class, json);
		
		String marshalled = ElementMapper.marshal(contactFacet);
		logger.debug(marshalled);
	}
	
}
