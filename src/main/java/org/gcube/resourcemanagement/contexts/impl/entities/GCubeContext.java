package org.gcube.resourcemanagement.contexts.impl.entities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.properties.Event;
import org.gcube.resourcemanagement.contexts.impl.properties.BasicInformation;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Context.NAME)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GCubeContext extends ContextImpl {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -8929392680534884169L;

	/**
	 * The events occurred to the Contexts.
	 * creation, renaming, parent change.
	 * Some of the event are managed by the resource-registry.
	 * Others can be added by an authorized client.
	 * This create a sort of journal. See #27707
	 */
	public static final String EVENTS_PROPERTY = "events";
	
	/**
	 * It contains the basic information for the context
	 */
	public static final String INFORMATION_PROPERTY = "information";
	
	/**
	 * This information is provided to allowed user only (by role)
	 * The symmetric key for the context
	 */
	public static final String KEY_PROPERTY = "key";
	
	/**
	 * {
	 * 	...
	 * 	"availableAt" : [
	 * 		"https://i-marine.d4science.org/group/alienandinvasivespecies", 
	 * 		"https://services.d4science.org/group/alienandinvasivespecies"
	 * 	]
	 * 	...
	 * }
	 * 
	 * For non VRE context this field could be null or could have multiple value.
	 * For VRE it is normally one value only (but some exception could exists)
	 */
	public static final String AVAILABLE_AT_PROPERTY = "availableAt";
	
	protected SortedSet<Event> events;
	protected BasicInformation information;
	protected String key;
	protected List<String> availableAt;
	
	public GCubeContext(Context c) {
		this.uuid = c.getID();
		this.metadata = c.getMetadata();
		
		this.name = c.getName();
		
		this.parent = c.getParent();
		this.children = c.getChildren();
		
		this.state = c.getState();
		
		this.additionalProperties = new HashMap<>();
		Map<String, Object> ap = c.getAdditionalProperties();
		for(String key : ap.keySet()) {
			Object obj = ap.get(key);
			switch (key) {
				case EVENTS_PROPERTY:
					@SuppressWarnings("unchecked") 
					SortedSet<Event> events = (SortedSet<Event>) obj;
					setEvents(events);
					break;
	
				case KEY_PROPERTY:
					setKey((String) obj);
					break;
				
				case INFORMATION_PROPERTY:
					setInformation((BasicInformation) obj);
					break;
				
				case AVAILABLE_AT_PROPERTY:
					@SuppressWarnings("unchecked")
					List<String> availableAt = (List<String>) obj;
					setAvailableAt(availableAt);
					break;
					
				default:
					this.additionalProperties.put(key, obj);
					break;
			}
		}
		
	}
	
	protected GCubeContext() {
		super();
	}
	
	public GCubeContext(UUID uuid) {
		this(null, uuid);
	}
	
	public GCubeContext(String name) {
		this(name, null);
	}
	
	public GCubeContext(String name, UUID uuid) {
		super(name, uuid);
	}
	
	@JsonGetter(EVENTS_PROPERTY)
	public SortedSet<Event> getEvents() {
		return events;
	}

	@JsonSetter(EVENTS_PROPERTY)
	public void setEvents(SortedSet<Event> events) {
		this.events = events;
	}
	
	@JsonIgnore
	public void addEvent(Event event) {
		if(this.events==null) {
			this.events = new TreeSet<>();
		}
		this.events.add(event);
	}

	@JsonGetter(INFORMATION_PROPERTY)
	public BasicInformation getInformation() {
		return information;
	}

	@JsonSetter(INFORMATION_PROPERTY)
	public void setInformation(BasicInformation information) {
		this.information = information;
	}

	@JsonGetter(KEY_PROPERTY)
	public String getKey() {
		return key;
	}

	@JsonSetter(KEY_PROPERTY)
	public void setKey(String key) {
		this.key = key;
	}

	@JsonGetter(AVAILABLE_AT_PROPERTY)
	public List<String> getAvailableAt() {
		return availableAt;
	}

	@JsonSetter(AVAILABLE_AT_PROPERTY)
	public void setAvailableAt(List<String> availableAt) {
		this.availableAt = availableAt;
	}
	
}
