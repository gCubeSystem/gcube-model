package org.gcube.resourcemanagement.contexts.impl.properties;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonFormat;
import org.gcube.informationsystem.base.reference.Element;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class BasicInformation {

	protected String description;
	protected Set<String> designers;
	protected Set<String> managers;
	protected Date from;
	/**
	 * Can be null. It means no end date defined.
	 */
	protected Date to;
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Set<String> getDesigners() {
		return designers;
	}
	
	public void setDesigners(Set<String> designers) {
		this.designers = designers;
	}
	
	public void addDesigner(String designer) {
		if(this.designers==null) {
			this.designers = new HashSet<>();
		}
		this.designers.add(designer);
	}
	
	public Set<String> getManagers() {
		return managers;
	}
	
	public void setManagers(Set<String> managers) {
		this.managers = managers;
	}
	
	public void addManager(String manager) {
		if(this.managers==null) {
			this.managers = new HashSet<>();
		}
		this.managers.add(manager);
	}
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Element.DATETIME_PATTERN)
	public Date getFrom() {
		return from;
	}
	
	public void setFrom(Date from) {
		this.from = from;
	}
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Element.DATETIME_PATTERN)
	public Date getTo() {
		return to;
	}
	
	public void setTo(Date to) {
		this.to = to;
	}
	
}
