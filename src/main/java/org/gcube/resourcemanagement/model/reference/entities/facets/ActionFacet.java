package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.ActionFacetImpl;
import org.gcube.resourcemanagement.model.reference.properties.EnumStringProperty;
import org.gcube.resourcemanagement.model.reference.properties.utilities.Named;

/**
 * 
 * @author Manuele Simi (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Action_Facet
 */
@JsonDeserialize(as=ActionFacetImpl.class)
@TypeMetadata(
	name = ActionFacet.NAME, 
	description = "This facet is expected to capture information on which action perform while a resource is added or removed from a context.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface ActionFacet extends Facet, Named {

	public static final String NAME = "ActionFacet"; // ActionFacet.class.getSimpleName();
		
	public static final String TYPE_PROPERTY = "type";

	public static final String WHEN_PROPERTY = "when";

	/**
	 * Type of action.
	 * @return an instance of {@link EnumStringProperty}
	 */
	@ISProperty(name=TYPE_PROPERTY, description = "Type of action", mandatory=true, nullable=false)
	public EnumStringProperty getType();
	
	public void setType(EnumStringProperty type);
	
	/**
	 * From where to download the action.
	 * A remote source used by the command. Could be the endpoint of a service, the location of a 
	 * remote ansible playbook, etc.
	 * @return URL or Endpoint
	 */
	@ISProperty(description = "From where to download the action.", mandatory=false, nullable=false)
	public String getSource();
	
	/**
	 * A remote source used by the command. Could be the endpoint of a service, the location of a 
	 * remote ansible playbook, etc.
	 * @param source
	 */
	public void setSource(String source);
	
	/**
	 * The options/params to use when executing the action.
	 * @return the command to execute to lauch the action
	 */
	@ISProperty(description = "The options/params to use when executing the action.", mandatory=true, nullable=false)
	public String getOptions();

	/**
	 * The options for the actions.
	 * @param options
	 */
	public void setOptions(String options);

	/**
	 * Gets the command to execute.
	 * @return the command
	 */
	@ISProperty(description = "The command to execute.")
	public String getCommand();
	
	/**
	 * Sets the command to execute.
	 * @param command
	 */
	public void setCommand(String command);
	
	
}
