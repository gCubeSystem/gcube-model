/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.BelongsToImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.LegalBody;
import org.gcube.resourcemanagement.model.reference.entities.resources.Person;

/**
 * BelongsTo indicates that a {@link Person} belong to the target {@link LegalBody}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#BelongsTo
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=BelongsToImpl.class)
@TypeMetadata(
	name = BelongsTo.NAME, 
	description = "BelongsTo indicates that a {@link Person} belong to the target {@link LegalBody}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface BelongsTo<Out extends Person, In extends LegalBody> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "BelongsTo"; //BelongsTo.class.getSimpleName();
	
}
