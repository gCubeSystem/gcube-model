/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.queries.templates.reference.properties.QueryTemplateReference;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.DiscoveryFacetImpl;
/**
 * 
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=DiscoveryFacetImpl.class)
@TypeMetadata(
	name = DiscoveryFacet.NAME, 
	description = "This facet captures information on the discovery of resources in a group.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface DiscoveryFacet extends Facet {
	
	public static final String NAME = "DiscoveryFacet"; // DiscoveryFacet.class.getSimpleName();
	
	public static final String GROUP_PROPERTY = "group";
	public static final String DESCRIPTION_PROPERTY = "description";
	public static final String MAX_PROPERTY = "max";
	public static final String MIN_PROPERTY = "min";
	public static final String QUERIES_PROPERTY = "queries";
	public static final String QUERY_TEMPLATES_PROPERTY = "queryTemplates";

	@ISProperty(name = GROUP_PROPERTY, description = "The name of the 'group' of resources to discover.", mandatory = true, nullable = false)
	public String getGroup();

	public void setGroup(String group);

	@ISProperty(name = DESCRIPTION_PROPERTY, description = "The description to display for the group of discovered resources")
	public String getDescription();

	public void setDescription(String description);

	@ISProperty(name = MIN_PROPERTY, readonly = false, mandatory = true, nullable = false, defaultValue = "0")
	public int getMin();
	
	public void setMin(int min);

	@ISProperty(name = MAX_PROPERTY, readonly = false, mandatory = true, nullable = true, defaultValue = "null")
	public Integer getMax();
	
	public void setMax(Integer max);

	@ISProperty(name = QUERIES_PROPERTY, description = "A list of queries to invoke to retrieve the resources", readonly = false, mandatory = true, nullable = true, defaultValue = "null")
	public ArrayNode getQueries();

	public void setQueries(ArrayNode queries);

	public void addQuery(ObjectNode query);

	@ISProperty(name = QUERY_TEMPLATES_PROPERTY, description = "A list of query templates to invoke to retrieve the resources", readonly = false, mandatory = true, nullable = true, defaultValue = "null")
	public List<QueryTemplateReference> getQueryTemplates();

	public void setQueryTemplates(List<QueryTemplateReference> queryTemplates);

	public void addQueryTemplates(QueryTemplateReference queryTemplates);

}