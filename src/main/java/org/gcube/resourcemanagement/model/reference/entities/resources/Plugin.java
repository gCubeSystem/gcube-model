/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.PluginImpl;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.DependsOn;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsPluginOf;


/**
 * Plugin is a piece of Software extending the capabilities of another 
 * {@link Software} (main) and requiring the main {@link Software} to be executed.
 * 
 * The relation between the main {@link Software} and the Plugin 
 * is expressed by {@link IsPluginOf} relation. 
 * {@link IsPluginOf} is an extension of {@link DependsOn}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Plugin
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=PluginImpl.class)
@ResourceSchema(
	resources= {
		@RelatedResourcesEntry(source=Plugin.class, relation=IsPluginOf.class, target=Software.class, description="A reference to the Software this Plugin is conceived to extend the capabilities.")
	}
)
@TypeMetadata(
	name = Plugin.NAME, 
	description = "Collect Plugin information through the list of its facets.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Plugin extends Software {
	
	public static final String NAME = "Plugin"; // Plugin.class.getSimpleName();

}
