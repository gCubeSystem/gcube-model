/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.net.URL;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.ContactReferenceFacetImpl;

/**
 * ContactReferenceFacet captures information on the primary and 
 * authoritative contact for the resource it is associated with.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Contact_Reference_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ContactReferenceFacetImpl.class)
@TypeMetadata(
	name = ContactReferenceFacet.NAME, 
	description = "ContactReferenceFacet captures information on the primary "
			+ "and authoritative contact for the resource it is associated with.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface ContactReferenceFacet extends Facet {
	
	public static final String NAME = "ContactReferenceFacet"; // ContactReferenceFacet.class.getSimpleName();
	
	@ISProperty(description = "The main website")
	public URL getWebsite();
	
	public void setWebsite(URL website);
	
	@ISProperty(description = "A physical address")
	public String getAddress();
	
	public void setAddress(String address);
	
	@ISProperty(description = "A phone number")
	public String getPhoneNumber();
	
	public void setPhoneNumber(String phoneNumber);

}
