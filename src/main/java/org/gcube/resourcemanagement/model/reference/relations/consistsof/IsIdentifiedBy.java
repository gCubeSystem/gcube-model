/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.IsIdentifiedByImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.GCubeResource;

/**
 * Each {@link GCubeResource} has been defined to have at least a facet linked with 
 * an IsIdentifiedBy relation. 
 * IsIdentifiedBy indicates that the target facet represents a sort of 
 * identification for the source resource. 
 * 
 * For instance, a software can consist of one or more {@link SoftwareFacet} but the one 
 * related with IsIdentifiedBy represents the identify of the software.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#IsIdentifiedBy
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=IsIdentifiedByImpl.class)
@TypeMetadata(
	name = IsIdentifiedBy.NAME, 
	description = "Each {@link GCubeResource} has been defined to have at least a facet linked with an IsIdentifiedBy relation. "
			+ "IsIdentifiedBy indicates that the target facet represents a sort of identification for the source resource. "
			+ "For instance, a software can consist of one or more {@link SoftwareFacet} but the one related with "
			+ "IsIdentifiedBy represents the identify of the software.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsIdentifiedBy<S extends Resource, T extends Facet>
		extends ConsistsOf<S, T> {
	
	public static final String NAME = "IsIdentifiedBy"; //IsIdentifiedBy.class.getSimpleName();
	
}