/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasManagerImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;

/**
 * HasManager indicates that the target {@link ContactFacet} contains the information 
 * related to a manager of the source resource,
 * e.g., the contact points of the manager of a research infrastructure or a data centre.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasManager
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasManagerImpl.class)
@TypeMetadata(
	name = HasManager.NAME, 
	description = "HasManager indicates that the target {@link ContactFacet} contains the information related "
			+ "to a manager of the source resource, "
			+ "e.g., the contact points of the manager of a research infrastructure or a data centre.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasManager<Out extends Resource, In extends ContactFacet> 
	extends HasContact<Out, In> {

	public static final String NAME = "HasManager"; // HasManager.class.getSimpleName();
}
