/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasOwnerImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;

/**
 * HasOwner indicates that the target {@link ContactFacet} contains the information related to 
 * the owner of the source resource, e.g., the contact points of the owner of dataset.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasOwner
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasOwnerImpl.class)
@TypeMetadata(
	name = HasOwner.NAME, 
	description = "HasOwner indicates that the target {@link ContactFacet} contains the information related "
			+ "to the owner of the source resource, e.g., the contact points of the owner of dataset.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasOwner<Out extends Resource, In extends ContactFacet> 
	extends HasContact<Out, In> {

	public static final String NAME = "HasOwner"; // HasOwner.class.getSimpleName();
}
