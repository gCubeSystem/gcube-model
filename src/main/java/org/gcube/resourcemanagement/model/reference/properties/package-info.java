/**
 * Common properties used across the gcube model.
 * 
 * @author Manuele Simi (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 *
 */
package org.gcube.resourcemanagement.model.reference.properties;