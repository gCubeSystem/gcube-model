/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.ConcreteDatasetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasMaintainer;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsPartOf;

/**
 * ConcreteDataset is any incarnation/manifestation of a dataset or part of it. 
 * The relation {@link IsPartOf} is used when a ConcreteDataset is part of a {@link Dataset}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Concrete_Dataset
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ConcreteDatasetImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=HasMaintainer.class, facet=ContactFacet.class, min=1, description="The contact information of the entity responsible for the maintenance of the concrete dataset"),
		@ResourceSchemaEntry(facet=AccessPointFacet.class, min=1, description="The access point to use for having access to the concrete dataset. The embargoState can be modeled through the access policy defined in the consistsOf relation.")
	},
	resources = {
		@RelatedResourcesEntry(source=ConcreteDataset.class, relation=IsPartOf.class, target=Dataset.class, description="The reference to the 'incarnations'/'manifestations' contributing to a dataset."),
	}
)
@TypeMetadata(
	name = ConcreteDataset.NAME, 
	description = "ConcreteDataset is any incarnation/manifestation of a dataset or part of it. "
			+ "The relation {@link IsPartOf} is used when a ConcreteDataset is part of a {@link Dataset}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface ConcreteDataset extends Dataset {

	public static final String NAME = "ConcreteDataset"; // ConcreteDataset.class.getSimpleName();
	
}
