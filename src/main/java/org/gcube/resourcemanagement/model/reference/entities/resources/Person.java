/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.PersonImpl;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.BelongsTo;

/**
 * Person represents any human playing the role of Actor.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Person
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as = PersonImpl.class)
@ResourceSchema(
	resources = {
			@RelatedResourcesEntry(source = Person.class, relation = BelongsTo.class, target = LegalBody.class)
	}
)
@TypeMetadata(
	name = Person.NAME, 
	description = "Person represents any human playing the role of Actor.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Person extends Actor {
	
	public static final String NAME = "Person"; // Person.class.getSimpleName();
	
}
