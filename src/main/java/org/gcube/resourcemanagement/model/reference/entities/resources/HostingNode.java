/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CPUFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.NetworkingFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SimplePropertyFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.StateFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasPersistentMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasVolatileMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;

/**
 * The HostingNode represent a container capable of managing the lifecycle of an 
 * electronic service, i.e., being capable to host and operate an {@link EService}. 
 * Examples are docker, tomcat. 
 * 
 * A container is a service which is conceived to enable any services respecting 
 * the container rules to be operative. The container does not typically provide any 
 * functionality rather than allowing the hosted service to operate.
 * 
 * The HostingNode characterisation in terms of facets reflects the one presented for {@link VirtualMachine}.
 * 
 * In particular, facets representing memory, CPU and networking interface are used to describe 
 * the HostingNode when the VirtualMachine enabling the HostingNode is not represented.
 * 
 * Federated systems can provide virtual machines as resource or containers as resources.
 * In some cases, the description of a container includes (virtual) hardware information.
 * 
 * It is important to highlight that HostingNode is not a subtype of VirtualMachine.
 * 
 * HostingNode could be though as a specialisation of the entity 
 * PE6_Software_Hosting_Service defined in PARTHENOS Entities Model (PE Model).
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Hosting_Node
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HostingNodeImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=IsIdentifiedBy.class, facet=NetworkingFacet.class, min=1, max=1, description="The Network ID characterising the Hosting Node."),
		@ResourceSchemaEntry(facet=CPUFacet.class, min=1, description="The CPU equipping the Hosting Node."),
		@ResourceSchemaEntry(relation=HasPersistentMemory.class, facet=MemoryFacet.class, min=1, description="The Disk Space Capacity of the Hosting Node."),
		@ResourceSchemaEntry(relation=HasVolatileMemory.class, facet=MemoryFacet.class, min=1, description="The RAM Capacity of the Hosting Node."),
		@ResourceSchemaEntry(facet=EventFacet.class, min=1, description="Every event characterizing the life cycle of the Hosting Node, e.g. the activation time."),
		@ResourceSchemaEntry(facet=StateFacet.class, min=1, max=1, description="The current state of the Hosting Node, e.g. started, ready, certified, down, failed."),
		@ResourceSchemaEntry(facet=SimplePropertyFacet.class, description="Any <key,value> pair property worth associating with the Hosting Node, e.g. Environment Variables"),
		@ResourceSchemaEntry(facet=SoftwareFacet.class, description=" Any Software characterising the Hosting Node. Useful to report the hosted software that are not registered in the Resource Registry as Software Resource, e.g. Operating System")
	}
)
@TypeMetadata(
	name = HostingNode.NAME, 
	description = "The HostingNode represent a container capable of managing the lifecycle of "
			+ "an electronic service, i.e., being capable to host and operate an {@link EService}. "
			+ "Examples are docker, tomcat. "
			+ "A container is a service which is conceived to enable any services respecting "
			+ "the container rules to be operative. The container does not typically provide any "
			+ "functionality rather than allowing the hosted service to operate. "
			+ "The HostingNode characterisation in terms of facets reflects the one presented for {@link VirtualMachine}. "
			+ "In particular, facets representing memory, CPU and networking interface are used to describe "
			+ "the HostingNode when the {@link VirtualMachine} enabling the HostingNode is not represented. "
			+ "Federated systems can provide virtual machines as resource or containers as resources. "
			+ "In some cases, the description of a container includes (virtual) hardware information. "
			+ "It is important to highlight that HostingNode is not a subclass of {@link VirtualMachine}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HostingNode extends Service {

	public static final String NAME = "HostingNode"; //HostingNode.class.getSimpleName();
	
}
