/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.EnablesImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.entities.resources.Plugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.RunningPlugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;

/**
 * Any {@link Service} representing running code of a specific software has the relation 
 * Enables targeted to the corresponding {@link Software}. 
 * Enables is used for example by {@link EService} to indicates the running software; 
 * from {@link HostingNode} to indicate the running software container; 
 * within {@link RunningPlugin} and the software represented as {@link Plugin}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Enables
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=EnablesImpl.class)
@TypeMetadata(
	name = Enables.NAME, 
	description = "Any {@link Service} representing running code of a specific software has the "
			+ "relation Enables targeted to the corresponding {@link Software}. "
			+ "Enables is used for example by {@link EService} to indicates the running software; "
			+ "from {@link HostingNode} to indicate the running software container; "
			+ "within {@link RunningPlugin} and the software represented as {@link Plugin}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Enables<Out extends Service, In extends Software> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "Enables"; // Runs.class.getSimpleName();
	
}
