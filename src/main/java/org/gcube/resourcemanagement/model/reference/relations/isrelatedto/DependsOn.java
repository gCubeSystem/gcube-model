/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.DependsOnImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;

/**
 * DependsOn indicates that a {@link Software} requires another one to work.
 * This relation must not be confused with {@link Requires}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#DependsOn
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as = DependsOnImpl.class)
@TypeMetadata(
	name = DependsOn.NAME, 
	description = "DependsOn indicates that a {@link Software} requires another one to work. "
			+ "This relation must not be confused with {@link Requires}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface DependsOn<Out extends Software, In extends Software> extends IsRelatedTo<Out,In> {
	
	public static final String NAME = "DependsOn"; // DependsOn.class.getSimpleName();
	
}
