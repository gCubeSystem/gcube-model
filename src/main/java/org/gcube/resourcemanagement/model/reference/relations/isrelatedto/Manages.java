/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.ManagesImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;

/**
 * Manages indicates that the source {@link Service} manages the target
 * {@link Dataset}, e.g. read, manipulate, etc.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Manages
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ManagesImpl.class)
@TypeMetadata(
	name = Manages.NAME, 
	description = "Manages indicates that of the source {@link Service} manages the target "
			+ "{@link Dataset}, e.g. read, manipulate, etc.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Manages<Out extends Service, In extends Dataset> 
	extends IsRelatedTo<Out, In> {
	
	public static final String NAME = "Manages"; // Manages.class.getSimpleName();
	
}
