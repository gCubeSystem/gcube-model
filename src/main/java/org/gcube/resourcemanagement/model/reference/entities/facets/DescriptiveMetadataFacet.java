/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.DescriptiveMetadataFacetImpl;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * DescriptiveMetadataFacet captures information on descriptive metadata 
 * to be associated with the resource.
 * This facet is mainly used to attach metadata to a Dataset.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Descriptive_Metadata_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=DescriptiveMetadataFacetImpl.class)
@TypeMetadata(
	name = DescriptiveMetadataFacet.NAME, 
	description = "DescriptiveMetadataFacet captures information on descriptive metadata to be associated with the resource. "
			+ "This facet is mainly used to attach metadata to a Dataset.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface DescriptiveMetadataFacet extends Facet {
	
	public static final String NAME = "DescriptiveMetadataFacet"; // DescriptiveMetadataFacet.class.getSimpleName();

	@ISProperty(description = "A metadata record representing the descriptive metadata according to the schema", mandatory=true, nullable=false)
	public ValueSchema getDescriptiveMetadata();
	
	public void setDescriptiveMetadata(ValueSchema metadata);

}