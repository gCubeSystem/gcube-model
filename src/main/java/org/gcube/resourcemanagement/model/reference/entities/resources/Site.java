/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.SiteImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.LocationFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.NetworkingFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasContact;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasMaintainer;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasManager;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Hosts;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsOwnedBy;

/**
 * Site is a resource representing the location (physical or virtual) hosting the 
 * resources associated.
 * 
 * Site allows to identify all the services that will be affected by downtime due to a
 * scheduled maintenance, as well as the impact on the infrastructure that an accidental
 * loss of connectivity could cause. 
 * 
 * This resource allows to study and define the replication scenarios or to provide 
 * an adequate redundancy level to a VRE.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Site
 * 
 * @author Luca Frosini (ISTI - CNR)
 * 
 */
@JsonDeserialize(as=SiteImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=IsIdentifiedBy.class, facet=IdentifierFacet.class, min=1, max=1, description="The Site Identifier."),
		@ResourceSchemaEntry(relation=HasContact.class, facet=ContactFacet.class, min=1, description="The main contact for the Site."),
		@ResourceSchemaEntry(relation=HasMaintainer.class, facet=ContactFacet.class, min=1, description="Contact information of the maintainer of the Site."),
		@ResourceSchemaEntry(relation=HasManager.class, facet=ContactFacet.class, min=1, description="Contact information of the Site Manager."),
		@ResourceSchemaEntry(facet=LocationFacet.class, min=1),
		@ResourceSchemaEntry(facet=NetworkingFacet.class, min=1),
	},
	resources= {
		@RelatedResourcesEntry(source=Site.class, relation=IsOwnedBy.class, target=Actor.class),
		@RelatedResourcesEntry(source=Site.class, relation=Hosts.class, target=Service.class)
	}
)
@TypeMetadata(
	name = Site.NAME, 
	description = "Site is a resource representing the location (physical or virtual) hosting "
			+ "the resources associated. "
			+ "Site allows to identify all the services that will be affected by downtime due to a "
			+ "scheduled maintenance, as well as the impact on the infrastructure that an accidental"
			+ "loss of connectivity could cause. "
			+ "This resource allows to study and define the replication scenarios or to provide "
			+ "an adequate redundancy level to a VRE.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Site extends GCubeResource {

	public static final String NAME = "Site"; // Site.class.getSimpleName();
	
}
