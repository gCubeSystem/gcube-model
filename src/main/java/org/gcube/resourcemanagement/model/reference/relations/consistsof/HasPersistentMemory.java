/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasPersistentMemoryImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;

/**
 * HasPersistentMemory indicates that the target {@link MemoryFacet} indicates a non-volatile
 * memory, i.e., a memory which does not lose the data when the device is powered down.
 * 
 * This type of memory is also known as secondary memory, external memory, auxiliary
 * storage, or secondary storage.
 * 
 * Of course more than one MemoryFacet related with HasPersistentMemory can be
 * attached to the same resource, but actually from an infrastructure management point of view,
 * we did not find any reason to further specialise the HasPersistentMemory relation.
 * 
 * Anyway, we do not exclude other specialisation required by services to select the appropriated persistent memory better.
 * For example, to discriminate between Solid State Disk and Magnetic Rotative. 
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasPersistentMemory
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasPersistentMemoryImpl.class)
@TypeMetadata(
	name = HasPersistentMemory.NAME, 
	description = "HasPersistentMemory indicates that the target {@link MemoryFacet} indicates a non-volatile "
			+ " memory, i.e., a memory which does not lose the data when the device is powered down. "
			+ "This type of memory is also known as secondary memory, external memory, auxiliary "
			+ "storage, or secondary storage. "
			+ "Of course more than one MemoryFacet related with HasPersistentMemory can be "
			+ "attached to the same resource, but actually from an infrastructure management point of view, "
			+ "we did not find any reason to further specialise the HasPersistentMemory relation. "
			+ "Anyway, we do not exclude other specialisation required by services to select the "
			+ "appropriated persistent memory better. "
			+ "For example, to discriminate between Solid State Disk and Magnetic Rotative.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasPersistentMemory<Out extends Resource, In extends MemoryFacet> extends HasMemory<Out, In> {

	public static final String NAME = "HasPersistentMemory"; // HasPersistentMemory.class.getSimpleName();

}