package org.gcube.resourcemanagement.model.reference.properties.utilities;

import org.gcube.informationsystem.types.annotations.ISProperty;
/**
 * A convenient interface for any type having a name
 *  
 * @author Manuele Simi (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 */
public interface Named {

	public static final String NAME_PROPERTY = "name";

	/**
	 * Gets the name of the facet.
	 * @return the name
	 */
	@ISProperty(name=NAME_PROPERTY, mandatory=true, nullable=false)
	public String getName();

	/**
	 * Sets the name of the facet.
	 * @param name the new name
	 */
	public void setName(String name);	

}
