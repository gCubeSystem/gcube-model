package org.gcube.resourcemanagement.model.reference.properties.utilities;

/**
 * A {@link TypedProperty} that can be validated.
 * 
 * @author Manuele Simi (ISTI CNR)
 */
public interface ValidatedTypedProperty<T,V> extends TypedProperty<T, V> {

	/**
     * Applies this validation to the property.
     *
     * @return the validation result
     */
	public Validation validate();

}
