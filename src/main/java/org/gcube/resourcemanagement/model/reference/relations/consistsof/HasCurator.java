/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasCuratorImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;

/**
 * HasCurator indicates that the target {@link ContactFacet} contains the information related to
 * a curator of the source resource, e.g., the contact points of the curator of a dataset.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasCurator
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasCuratorImpl.class)
@TypeMetadata(
	name = HasCurator.NAME, 
	description = "HasCurator indicates that the target {@link ContactFacet} contains the information related "
			+ "to a curator of the source resource, e.g., the contact points of the curator of a dataset.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasCurator<Out extends Resource, In extends ContactFacet> 
	extends HasContact<Out, In> {

	public static final String NAME = "HasCurator"; // HasCurator.class.getSimpleName();
}
