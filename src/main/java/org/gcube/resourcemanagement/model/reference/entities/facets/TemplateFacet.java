/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.TemplateFacetImpl;
import org.gcube.resourcemanagement.model.reference.properties.AttributeProperty;
import org.gcube.resourcemanagement.model.reference.properties.utilities.Named;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=TemplateFacetImpl.class)
@TypeMetadata(
	name = TemplateFacet.NAME, 
	description = "This facet define a template for a Facet to instantiate using the list of attributes specified. The type of the instatianted facet is specified in 'targetFacetType' attribute",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface TemplateFacet extends Facet, Named {
	
	public static final String NAME = "TemplateFacet"; // TemplateFacet.class.getSimpleName();
	public static final String DESCRIPTION_PROPERTY = "description";
	public static final String TARGET_TYPE_PROPERTY = "targetType";
	public static final String PROPERTIES_PROPERTY = "properties";
	
	@Override
	@ISProperty(name = NAME_PROPERTY, description = "The name of the template", mandatory = true, nullable = false)
	public String getName();

	@Override
	public void setName(String name);
	
	
	@ISProperty(name = DESCRIPTION_PROPERTY, description = "The description of the template", readonly = false, mandatory = true, nullable = false)
	public String getDescription();
	
	public void setDescription(String description);
	
	
	@ISProperty(name = TARGET_TYPE_PROPERTY, description = "The type of Facet to instantiate with the attributes valued", readonly = false, mandatory = true, nullable = false)
	public String getTargetType();
	
	public void setTargetType(String targetType);
	
	
	@ISProperty(name = PROPERTIES_PROPERTY, description = "The attributes of the template to be created when instantiated", readonly = false, mandatory = true, nullable = false, min=1)
	public List<AttributeProperty> getProperties();
	
	public void setProperties(List<AttributeProperty> properties);
	
	@JsonIgnore
	public void addProperty(AttributeProperty property);
	
}