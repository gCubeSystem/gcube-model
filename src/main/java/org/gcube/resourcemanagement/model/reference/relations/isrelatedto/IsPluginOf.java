/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.IsPluginOfImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Plugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;

/**
 * A {@link Plugin} is a piece of Software extending the capabilities of another 
 * {@link Software} (main) and requiring the main {@link Software} to be executed. 
 * The relation between the main {@link Software} and the {@link Plugin} is expressed 
 * by IsPluginOf relation. 
 * IsPluginOf is an extension of {@link DependsOn}.

 * https://wiki.gcube-system.org/gcube/GCube_Model#IsPluginOf
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as = IsPluginOfImpl.class)
@TypeMetadata(
	name = IsPluginOf.NAME, 
	description = "A {@link Plugin} is a piece of Software extending the capabilities of another {@link Software} "
			+ "(main) and requiring the main {@link Software} to be executed. "
			+ "The relation between the main {@link Software} and the {@link Plugin} "
			+ "is expressed  by IsPluginOf relation. "
			+ "IsPluginOf is an extension of {@link DependsOn}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsPluginOf<Out extends Plugin, In extends Software> extends DependsOn<Out,In> {
	
	public static final String NAME = "IsPluginOf"; // IsPluginOf.class.getSimpleName();
	
}
