/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.SimpleFacetImpl;

/**
 * A sort of catch all.
 * It does not define any property.
 * It is mainly used to one or more arbitrary properties to the resource.  
 * Before using SimpleFacet a developer should evaluate if it is possible to identify a specific Facet 
 * to capture the particular aspect of the resource. 
 * The usage of SimpleFacet should be reduced to the maximum.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Simple_Property_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=SimpleFacetImpl.class)
@TypeMetadata(
	name = SimpleFacet.NAME, 
	description = "A sort of catch all. It does not define any property. "
			+ "It is mainly used to one or more arbitrary properties to the resource. "
			+ "Before using SimpleFacet a developer should evaluate if it is possible to identify a specific Facet"
			+ "to capture the particular aspect of the resource. "
			+ "The usage of SimpleFacet should be reduced to the maximum.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface SimpleFacet extends Facet {
	
	public static final String NAME = "SimpleFacet"; // SimpleFacet.class.getSimpleName();
	
}