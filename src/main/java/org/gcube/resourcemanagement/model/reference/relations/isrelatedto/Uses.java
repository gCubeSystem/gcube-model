/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.UsesImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;

/**
 * Uses relation inform regarding the network invocation of the target 
 * {@link EService} by the source. 
 * 
 * Uses relation specialises the semantic of {@link CallsFor}.

 * https://wiki.gcube-system.org/gcube/GCube_Model#Uses
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=UsesImpl.class)
@TypeMetadata(
	name = Uses.NAME, 
	description = "Uses relation inform regarding the network invocation of the target"
			+ "{@link EService} by the source. "
			+ "Uses relation specialises the semantic of {@link CallsFor}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Uses<Out extends EService, In extends EService> 
	extends CallsFor<Out, In> {

	public static final String NAME = "Uses"; // Uses.class.getSimpleName();
	
}
