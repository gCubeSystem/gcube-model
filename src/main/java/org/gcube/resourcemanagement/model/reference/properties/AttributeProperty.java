package org.gcube.resourcemanagement.model.reference.properties;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.Attribute;
import org.gcube.informationsystem.base.reference.AttributeDefinition;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.properties.AttributePropertyImpl;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=AttributePropertyImpl.class)
@TypeMetadata(
	name = AttributeProperty.NAME, 
	description = "This class model as property any attrbute must be instantiated.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface AttributeProperty extends GCubeProperty, AttributeDefinition {

	public static final String NAME = "AttributeProperty"; // AttributeProperty.class.getSimpleName();
	
	@ISProperty(name = Attribute.NAME_PROPERTY, description = "The name of the Query Template Variable.", readonly = true, mandatory = true, nullable = false)
	@Override
	public String getName();
	
	@ISProperty(name = Attribute.DESCRIPTION_PROPERTY, readonly = false, mandatory = true, nullable = false)
	@Override
	public String getDescription();

	@ISProperty(name = AttributeDefinition.MANDATORY_PROPERTY, readonly = false, mandatory = true, nullable = false, defaultValue = "false")
	public boolean isMandatory();
	
	@ISProperty(name = AttributeDefinition.NOT_NULL_PROPERTY, readonly = false, mandatory = true, nullable = false, defaultValue = "false")
	public boolean isNotnull();
	
	@ISProperty(name = Attribute.MAX_PROPERTY, readonly = false, mandatory = true, nullable = false)
	@Override
	public Integer getMax();
	
	@ISProperty(name = Attribute.MIN_PROPERTY, readonly = false, mandatory = true, nullable = false)
	@Override
	public Integer getMin();
	
	@ISProperty(name = Attribute.REGEX_PROPERTY, readonly = false, mandatory = true, nullable = false)
	@Override
	public String getRegexp();
	
	@ISProperty(name = Attribute.PROPERTY_TYPE_PROPERTY, readonly = false, mandatory = true, nullable = false)
	@Override
	public String getPropertyType();
	
	@ISProperty(name = Attribute.DEFAULT_VALUE_PROPERTY, readonly = false, mandatory = false, nullable = true)
	@Override
	public Object getDefaultValue();
	
}
