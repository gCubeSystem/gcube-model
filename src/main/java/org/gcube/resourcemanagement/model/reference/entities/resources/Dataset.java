/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.DatasetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.CoverageFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.DescriptiveMetadataFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.LicenseFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.ProvenanceFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SubjectFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasContact;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasContributor;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasCoverage;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasCreator;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasCurator;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasMaintainer;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasOwner;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasSpatialCoverage;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasTemporalCoverage;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Involves;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsCompliantWith;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsCorrelatedTo;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsPartOf;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Manages;

/**
 * A Dataset is a set of digital objects representing data and treated collectively as a unit. 
 * It is the key resource of a HDI, even more, it is the reason the HDI exists.
 * 
 * A Dataset can be correlated to another Dataset by using {@link IsCorrelatedTo} relation.
 * 
 * Dataset has similar meaning of PE18_Dataset defined in PARTHENOS Entities Model (PE Model).
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Dataset
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as = DatasetImpl.class)
@ResourceSchema(
	facets = {
		@ResourceSchemaEntry(relation = IsIdentifiedBy.class, facet = IdentifierFacet.class, min = 1, max=1, description = "The set of Identifiers associated with the Dataset instance."),
		@ResourceSchemaEntry(relation = HasContact.class, facet = ContactFacet.class, min = 1, description = "The contact information of the entity responsible for the dataset."),
		@ResourceSchemaEntry(relation = HasContributor.class, facet = ContactFacet.class, description = "The contact information on contributors supporting the creation and development of the Dataset."),
		@ResourceSchemaEntry(relation = HasCreator.class, facet = ContactFacet.class, description = " The contact information of the creator of the Dataset."),
		@ResourceSchemaEntry(relation = HasCurator.class, facet = ContactFacet.class, description = " The contact information of the entity responsible for the curation of the dataset."),
		@ResourceSchemaEntry(relation = HasMaintainer.class, facet = ContactFacet.class, description = "The contact information of the entity responsible for the maintenance of the dataset."),
		@ResourceSchemaEntry(relation = HasOwner.class, facet = ContactFacet.class, description = "The contact information of the entity having the ownership of the dataset."),
		@ResourceSchemaEntry(facet = AccessPointFacet.class, description = "The access point to use for having access to the dataset. The embargoState can be modeled through the access policy defined in the consistsOf relation."),
		@ResourceSchemaEntry(facet = LicenseFacet.class, description = "The licence governing dataset exploitation. The duration of license (if any) is captured by the expiry date defined in the consistsOf relation."),
		@ResourceSchemaEntry(facet = EventFacet.class, description = "Any 'event' characterising the lifecycle of the dataset, e.g. collection date, last collection date."),
		@ResourceSchemaEntry(facet = ProvenanceFacet.class, description = "Any provenance record associated with the dataset."),
		@ResourceSchemaEntry(relation = HasCoverage.class, facet = CoverageFacet.class, min = 1, description = "Any coverage related information (e.g. topic, species) characterising the content of the dataset."),
		@ResourceSchemaEntry(relation = HasTemporalCoverage.class, facet = CoverageFacet.class, description = "Any temporal coverage information characterising the content of the dataset, e.g. the time-span covered by the dataset."),
		@ResourceSchemaEntry(relation = HasSpatialCoverage.class, facet = CoverageFacet.class, description = "Any geo-spatial coverage information characterising the content of the dataset, e.g. the area covered by the dataset."),
		@ResourceSchemaEntry(facet = DescriptiveMetadataFacet.class, description = "Any descriptive information associated with the dataset, e.g. for discovery purposes."),
		@ResourceSchemaEntry(facet = SubjectFacet.class, description = "Any subject/tag associated with the dataset for descriptive and discovery purposes.")
	},
	resources = {
		@RelatedResourcesEntry(source=Dataset.class, relation=Involves.class, target=Actor.class),
		@RelatedResourcesEntry(source=Dataset.class, relation=IsCorrelatedTo.class, target=Dataset.class),
		@RelatedResourcesEntry(source=Dataset.class, relation=IsCompliantWith.class, target=Schema.class, description="Any schema characterising the content of the dataset, e.g. the schema describing the 'columns' of a CSV-based dataset."),
		@RelatedResourcesEntry(source=ConcreteDataset.class, relation=IsPartOf.class, target=Dataset.class, description="The reference to the 'incarnations'/'manifestations' contributing to a dataset."),
		@RelatedResourcesEntry(source=Service.class, relation=Manages.class, target=Dataset.class, description="The link between the service that 'manages' the dataset and the dataset, e.g. the service that hosts the dataset and give access to it."),
	}
)
@TypeMetadata(
	name = Dataset.NAME, 
	description = "A Dataset is a set of digital objects representing data and treated collectively as a unit. "
			+ "It is the key resource of a HDI, even more, it is the reason the HDI exists. "
			+ "A Dataset can be correlated to another Dataset by using {@link IsCorrelatedTo} relation.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Dataset extends GCubeResource {
	
	public static final String NAME = "Dataset"; // Dataset.class.getSimpleName();
	
}
