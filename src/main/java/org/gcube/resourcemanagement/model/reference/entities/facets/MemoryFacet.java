/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.MemoryFacetImpl;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasPersistentMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasVolatileMemory;

/**
 * MemoryFacet captures information on computer memory equipping the resource and its usage.
 * 
 * Any resource describing a computing machine must have at least two types of memories 
 * i.e., persistent and volatile. For such a reason, it has been identified the ConsistsOf
 * relation called {@link HasMemory}.
 * 
 * It is in charge of the specialisation {@link HasVolatileMemory} and {@link HasPersistentMemory} 
 * to clarify the semantics of the memory.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Memory_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=MemoryFacetImpl.class)
@TypeMetadata(
	name = MemoryFacet.NAME, 
	description = "MemoryFacet captures information on computer memory equipping the resource and its usage. "
			+ "Any resource describing a computing machine must have at least two types of memories "
			+ "i.e., persistent and volatile. For such a reason, it has been identified the ConsistsOf "
			+ "relation called {@link HasMemory}. "
			+ "It is in charge of the specialisation {@link HasVolatileMemory} and {@link HasPersistentMemory} "
			+ "to clarify the semantics of the memory.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface MemoryFacet extends Facet {
	
	public static final String NAME = "MemoryFacet"; // MemoryFacet.class.getSimpleName();
	
	public enum MemoryUnit {
		Byte, kB, MB, GB, TB, PB, EB, ZB, YB
	}
	
	@ISProperty
	public long getSize();
	
	public void setSize(long size);
	
	@ISProperty
	public long getUsed();
	
	public void setUsed(long used);
	
	@ISProperty
	public MemoryUnit getUnit();
	
	public void setUnit(MemoryUnit unit);
}
