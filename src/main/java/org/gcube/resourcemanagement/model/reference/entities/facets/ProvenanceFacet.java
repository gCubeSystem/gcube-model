/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.ProvenanceFacetImpl;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * ProvenanceFacet captures information on provenance/lineage of the entire resource.
 * It is mainly used to describe provenance information of a Dataset.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Provenance_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ProvenanceFacetImpl.class)
@TypeMetadata(
	name = ProvenanceFacet.NAME, 
	description = "ProvenanceFacet captures information on provenance/lineage of the entire resource. "
			+ "It is mainly used to describe provenance information of a Dataset.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface ProvenanceFacet extends Facet {
	
	public static final String NAME = "ProvenanceFacet"; // ProvenanceFacet.class.getSimpleName();
	
	public enum Relationship {
		wasDerivedFrom, wasGeneratedBy /* .... */
	}
	
	@ISProperty(description = "Relationship with the resource indicated by ID. Please note that the provenance has not been modelled as IsRelatedTo specialization, because the source resource is not necessarly in the IS or could be deleted from the IS at any time.")
	public Relationship getRelationship();
	
	public void setRelationship(Relationship relationship);
	
	@ISProperty(description = "The ID of the referenced resource.")
	public String getReference();
	
	public void setReference(String reference);
	
	@ISProperty(description = "Provenance Document, e.g., an XML according to the reference schema.")
	public ValueSchema getDocument();
	
	public void setDocument(ValueSchema document);
	
}