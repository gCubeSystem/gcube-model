/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.LocationFacetImpl;

/**
 * LocationFacet captures information on a physical area characterising the resource it
 * is associated with. This should not be confused with {@link CoverageFacet}
 * The LocationFacet provides information of a location (eventually using 
 * latitude and longitude), instead {@link CoverageFacet} provide a way to to define the spatial 
 * or the temporal extent the resource represent.
 * 
 * It is mainly used to locate a data centre or to the geographic references of a 
 * legal body playing the role of an actor in the infrastructure.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Location_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=LocationFacetImpl.class)
@TypeMetadata(
	name = LocationFacet.NAME, 
	description = "LocationFacet captures information on a physical area characterising the resource it is associated with. "
			+ "This should not be confused with {@link CoverageFacet}. "
			+ "The LocationFacet provides information of a location (eventually using latitude and longitude), "
			+ "instead {@link CoverageFacet} provide a way to to define the spatial or the temporal extent the resource represent. "
			+ "It is mainly used to locate a data centre or to the geographic references of a legal body playing "
			+ "the role of an actor in the infrastructure.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface LocationFacet extends Facet {

	public static final String NAME = "LocationFacet"; // LocationFacet.class.getSimpleName();
	
	@ISProperty(description = "The English name of the country")
	public String getCountry();
	
	public void setCountry(String country);

	@ISProperty(description = "The City name")
	public String getLocation();
	
	public void setLocation(String location);
	
	@ISProperty(description = "Latitude")
	public String getLatitude();
	
	public void setLatitude(String latitude);
	
	@ISProperty(description = "Longitude")
	public String getLongitude();
	
	public void setLongitude(String longitude);
	
}