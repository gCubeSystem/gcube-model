/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.ContactFacetImpl;

/**
 * ContactFacet captures information on a point of contact for the resource, 
 * i.e., a person or a department serving as the coordinator or focal point 
 * of information concerning the resource.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Contact_Facet
 * 
 *  @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ContactFacetImpl.class)
@TypeMetadata(
	name = ContactFacet.NAME, 
	description = "ContactFacet captures information on a point of contact for the resource, "
			+ "i.e., a person or a department serving as the coordinator or focal point  "
			+ "of information concerning the resource.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface ContactFacet extends Facet {

	public static final String NAME = "ContactFacet"; // ContactFacet.class.getSimpleName();

	public static final String EMAIL_PROPERTY = "eMail";

	public static final String EMAIL_PATTERN = "^[a-z0-9._%+-]{1,128}@[a-z0-9.-]{1,128}$";

	@ISProperty(description = "A name describing the profession or marital status of the point of contact. e.g., Dr, Mrs, Mr.")
	public String getTitle();

	public void setTitle(String title);

	@ISProperty(description = "First Name", mandatory=true, nullable=false)
	public String getName();

	public void setName(String name);

	@ISProperty(description = "Middle Name")
	public String getMiddleName();

	public void setMiddleName(String middleName);

	@ISProperty(description = "Surname", mandatory=true, nullable=false)
	public String getSurname();

	public void setSurname(String surname);

	@ISProperty(description = "Email address", name=EMAIL_PROPERTY, mandatory=true, nullable=false, regexpr=EMAIL_PATTERN)
	public String getEMail();

	public void setEMail(String eMail);

}
