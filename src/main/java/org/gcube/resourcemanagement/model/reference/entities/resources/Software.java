/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.SoftwareImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CapabilityFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Demands;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.DependsOn;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsConfiguredBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsPluginOf;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Requires;

/**
 * Software is an entity worth being represented for management purposes. 
 * The relation {@link DependsOn} indicates dependencies between two Software 
 * captured for management purposes.
 * 
 * Software has similar meaning of D14_Software defined in CRMdig.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Software
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=SoftwareImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=IsIdentifiedBy.class, facet=SoftwareFacet.class, min=1, max=1, description="Software coordinates which identify the Software per se."),
		@ResourceSchemaEntry(facet=SoftwareFacet.class, min=1, description="Apart the one connected by the IsIdentifiedBy relation (gCube coordinates) the others identify the software in other way e.g. (Maven coordinates)."),
		@ResourceSchemaEntry(facet=CapabilityFacet.class)
	},
	resources= {
		@RelatedResourcesEntry(source=Software.class, relation=DependsOn.class, target=Software.class, description="To capture any dependency between two software artifacts."),
		@RelatedResourcesEntry(source=Software.class, relation=IsConfiguredBy.class, target=ConfigurationTemplate.class),
		@RelatedResourcesEntry(source=Software.class, relation=Requires.class, target=Service.class, description="To capture any dependency between a software artifact and a Service, e.g. A software requiring a specific database instance."),
		@RelatedResourcesEntry(source=VirtualService.class, relation=Demands.class, target=Software.class),
		@RelatedResourcesEntry(source=Plugin.class, relation=IsPluginOf.class, target=Software.class, description="To capture the relation between a Software and its Plugins.")
	}
)
@TypeMetadata(
	name = Software.NAME, 
	description = "Software is an entity worth being represented for management purposes. "
			+ "The relation {@link DependsOn} indicates dependencies between two Software captured for management purposes.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Software extends GCubeResource {

	public static final String NAME = "Software"; // Software.class.getSimpleName();
	
}
