/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.ConfigurationTemplateImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.TemplateFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsConfiguredBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsCustomizedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsDerivationOf;

/**
 * Configuration Template represents a template for a configuration.
 * It describes how a configuration has to be realised, 
 * e.g. used to define the catalogue configuration parameters template.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Configuration_Template
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ConfigurationTemplateImpl.class)
@ResourceSchema(
	facets={
			@ResourceSchemaEntry(relation=IsIdentifiedBy.class, facet=IdentifierFacet.class, min=1, max=1),
			@ResourceSchemaEntry(relation=ConsistsOf.class, facet=TemplateFacet.class)
	},
	resources = {
			@RelatedResourcesEntry(source=Configuration.class, relation=IsDerivationOf.class, target=ConfigurationTemplate.class),
			@RelatedResourcesEntry(source=Service.class, relation=IsCustomizedBy.class, target=ConfigurationTemplate.class),
			@RelatedResourcesEntry(source=Software.class, relation=IsConfiguredBy.class, target=ConfigurationTemplate.class),
	}
)
@TypeMetadata(
	name = ConfigurationTemplate.NAME, 
	description = "Configuration Template represents a template for a configuration. It describes how a configuration has to be realised, e.g. used to define the catalogue configuration parameters template.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface ConfigurationTemplate extends GCubeResource {
	
	public static final String NAME = "ConfigurationTemplate"; //ConfigurationTemplate.class.getSimpleName();
	
}