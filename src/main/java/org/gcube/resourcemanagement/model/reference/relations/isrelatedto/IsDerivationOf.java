/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.IsDerivationOfImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Configuration;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConfigurationTemplate;

/**
 * IsDerivationOf indicate that the source {@link Configuration} is an instantiation of
 * the target {@link ConfigurationTemplate}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#IsDerivationOf
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=IsDerivationOfImpl.class)
@TypeMetadata(
	name = IsDerivationOf.NAME, 
	description = "IsDerivationOf indicate that the source {@link Configuration} is an "
			+ "instantiation of the target {@link ConfigurationTemplate}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsDerivationOf<Out extends Configuration, In extends ConfigurationTemplate> 
	extends IsRelatedTo<Out, In> {
	
	public static final String NAME = "IsDerivationOf"; // IsDerivationOf.class.getSimpleName();

}
