/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.RunningPluginImpl;

/**
 * RunningPlugin allows differentiating which is the primary service and which is 
 * an additional capability of a such a service. 
 * Keeping the two concepts separated enables to share a service across 
 * VREs with a subset of its capabilities.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Running_Plugin
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=RunningPluginImpl.class)
@TypeMetadata(
	name = RunningPlugin.NAME, 
	description = "RunningPlugin allows differentiating which is the primary service and "
			+ "which is an additional capability of a such a service. "
			+ "Keeping the two concepts separated enables to share a service across "
			+ "VREs with a subset of its capabilities.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface RunningPlugin extends EService {
	
	public static final String NAME = "RunningPlugin"; // RunningPlugin.class.getSimpleName();
	
}
