/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasMaintainerImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;

/**
 * HasDeveloper indicates that the target {@link ContactFacet} contains the 
 * information related to a developer of the source resource, 
 * e.g., the contact points of the developer of a software.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasMaintainer
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasMaintainerImpl.class)
@TypeMetadata(
	name = HasMaintainer.NAME, 
	description = "HasDeveloper indicates that the target {@link ContactFacet} contains the information related "
			+ "to a developer of the source resource, e.g., the contact points of the developer of a software.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasMaintainer<Out extends Resource, In extends ContactFacet> 
	extends HasContact<Out, In> {

	public static final String NAME = "HasMaintainer"; // HasMaintainer.class.getSimpleName();
}
