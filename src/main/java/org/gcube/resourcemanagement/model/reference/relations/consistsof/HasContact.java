/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasContactImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;

/**
 * HasContact is a base relation type to capture the diverse points of contact associated with a resource.
 * This relation is abstract because if not specialised it does not add any semantic of
 * relating the resource with the target {@link ContactFacet}. 
 * Instead, every specialisation refines the reason of using the {@link ContactFacet} allowing 
 * to discriminate between two or more {@link ContactFacet} attached to the same resource.
 * 
 * The identified specialisations are 
 * {@link HasContributor}, {@link HasCreator}, {@link HasCurator}, 
 * {@link HasDeveloper}, {@link HasMaintainer}, {@link HasManager} and 
 * {@link HasOwner}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasContact
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonDeserialize(as=HasContactImpl.class)
@TypeMetadata(
	name = HasContact.NAME, 
	description = "HasContact is a base relation type to capture the diverse points of contact associated with a resource. "
			+ "This relation is abstract because if not specialised it does not add any semantic of relating "
			+ "the resource with the target {@link ContactFacet}. "
			+ "Instead, every specialisation refines the reason of using the "
			+ "{@link ContactFacet} allowing to discriminate between two or more {@link ContactFacet} attached to the same resource."
			+ "The identified specialisations are: "
			+ "{@link HasContributor}, {@link HasCreator}, {@link HasCurator}, "
			+ "{@link HasDeveloper}, {@link HasMaintainer}, {@link HasManager} and {@link HasOwner}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasContact<Out extends Resource, In extends ContactFacet> 
	extends ConsistsOf<Out, In> {

	public static final String NAME = "HasContact"; //HasContact.class.getSimpleName();
	
}