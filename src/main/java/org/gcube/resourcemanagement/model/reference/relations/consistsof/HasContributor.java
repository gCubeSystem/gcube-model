/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasContributorImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;

/**
 * HasContributor indicates that the target {@link ContactFacet} contains the information 
 * related to a contributor to the source resource, 
 * e.g., the contact points of the contributor of software or the contributor of a dataset.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasContributor
 *  
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasContributorImpl.class)
@TypeMetadata(
	name = HasContributor.NAME, 
	description = "HasContributor indicates that the target {@link ContactFacet} contains the information "
			+ "related to a contributor to the source resource, e.g., the contact points of the contributor "
			+ "of software or the contributor of a dataset.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasContributor<Out extends Resource, In extends ContactFacet> 
	extends HasContact<Out, In> {

	public static final String NAME = "HasContributor"; // HasContributor.class.getSimpleName();

}