/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.CallsForImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;

/**
 * A {@link Service} instance can require another {@link Service} instance to properly 
 * operate and this is indicated with CallsFor. 
 * Motivations similar to the ones for {@link Requires} relation conducted to the definition for this relation.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#CallsFor
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=CallsForImpl.class)
@TypeMetadata(
	name = CallsFor.NAME, 
	description = "A {@link Service} instance can require another {@link Service} instance to properly "
			+ "operate and this is indicated with CallsFor. "
			+ "Motivations similar to the ones for {@link Requires} relation conducted to the definition for this relation.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface CallsFor<Out extends Service, In extends Service> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "CallsFor"; //CallsFor.class.getSimpleName();
	
}
