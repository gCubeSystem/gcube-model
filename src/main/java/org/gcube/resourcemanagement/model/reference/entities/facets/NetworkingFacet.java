/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.NetworkingFacetImpl;

/**
 * NetworkingFacet captures information on any (computer) network interface 
 * associated with the resource.
 * It is mainly used to describe the network interface of a host. 
 * It should not be confused with the {@link AccessPointFacet} 
 * which instead describes the protocol and the endpoint of a web-based service.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Networking_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=NetworkingFacetImpl.class)
@TypeMetadata(
	name = NetworkingFacet.NAME, 
	description = "NetworkingFacet captures information on any (computer) network interface associated with the resource."
			+ "It is mainly used to describe the network interface of a host. "
			+ "It should not be confused with the {@link AccessPointFacet} "
			+ "which instead describes the protocol and the endpoint of a web-based service.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface NetworkingFacet extends Facet {
	
	public static final String NAME = "NetworkingFacet"; // NetworkingFacet.class.getSimpleName();
	
	public static final String IP_ADDRESS = "IPAddress";
	
	@ISProperty(description = "Host Name")
	public String getHostName();
	
	public void setHostName(String hostName);
	
	@ISProperty(description = "Domain Name")
	public String getDomainName();
	
	public void setDomainName(String domainName);
		
	@ISProperty(name=IP_ADDRESS, description = "Internet Protocol (IP) Address", mandatory=true, nullable=false)
	@JsonGetter(value = IP_ADDRESS)
	public String getIPAddress();
	
	@JsonSetter(value = IP_ADDRESS)
	public void setIPAddress(String ipAddress);
	
	@ISProperty(description = "Network Mask")
	public String getMask();
	
	public void setMask(String mask);
	
	@ISProperty(description = "Broadcast Address")
	public String getBroadcastAddress();
	
	public void setBroadcastAddress(String broadcastAddress);
	
}