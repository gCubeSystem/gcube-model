/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.net.URI;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.AccessPointFacetImpl;

/**
 * AccessPointFacet captures information on an 'access point' of a resource, 
 * i.e., any web-based endpoint to programmatically interact with the resource via a known protocol.
 * For example, it is used to define the network endpoint to contact the service. 
 * The endpoint can expose a well-known high-level protocol.
 * https://wiki.gcube-system.org/gcube/GCube_Model#Access_Point_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=AccessPointFacetImpl.class)
@TypeMetadata(
	name = AccessPointFacet.NAME, 
	description = "AccessPointFacet captures information on an 'access point' of a resource, i.e., "
			+ "any web-based endpoint to programmatically interact with the resource via a known protocol. "
			+ "For example, it is used to define the network endpoint to contact the service. "
			+ "The endpoint can expose a well-known high-level protocol.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface AccessPointFacet extends Facet {
	
	public static final String NAME = "AccessPointFacet"; // AccessPointFacet.class.getSimpleName();
	
	public static final String ENDPOINT_PROPERTY = "endpoint";
	public static final String AUTHORIZATION_PROPERTY = "authorization";
	
	/**
	 * A distinguishing string to be used by clients to identify the access point of interest.
	 * @return the entry name
	 */
	@ISProperty(description = "A distinguishing string to be used by clients to identify the access point of interest.")
	public String getEntryName();
	
	public void setEntryName(String entryName);
	
	/**
	 * The URI which characterises the specific endpoint instance.
	 * @return the URI
	 */
	@ISProperty(name=ENDPOINT_PROPERTY, description = "The URI which characterises the specific endpoint instance.", mandatory=true, readonly = true, nullable=false)
	public URI getEndpoint();
	
	public void setEndpoint(URI endpoint);
	
	/**
	 * The high-level protocol used by the access point. The String could contain the version if needed. 
	 * e.g., Web Map Service (WMS) and not HyperText Transfer Protocol (HTTP) which is already contained in the URI.
	 * @return the protocol
	 */
	@ISProperty(description = "The high-level protocol used by the access point. The String could contain the version if needed. e.g., Web Map Service (WMS) and not HyperText Transfer Protocol (HTTP) which is already contained in the URI.")
	public String getProtocol();
	
	public void setProtocol(String protocol);
	
	/**
	 * A human-oriented text accompanying the access point
	 * @return the description
	 */
	@ISProperty(description = "A human-oriented text accompanying the access point.")
	public String getDescription();
	
	public void setDescription(String description);
	
	@ISProperty(name=AUTHORIZATION_PROPERTY, description = "Contains authorisation information. e.g., a token, the couple username:password, etc.")
	public Property getAuthorization();
	
	public void setAuthorization(Property authorization);
	
}
