/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.IsConfiguredByImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConfigurationTemplate;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;

/**
 * The relation IsConfiguredBy indicates that the source {@link Software} 
 * requires a configuration when it is instantiated.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#IsConfiguredBy
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=IsConfiguredByImpl.class)
@TypeMetadata(
	name = IsConfiguredBy.NAME, 
	description = "The relation IsConfiguredBy indicates that the source "
				+ "{@link Software} requires a configuration when it is instantiated.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsConfiguredBy<Out extends Software, In extends ConfigurationTemplate> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "IsConfiguredBy"; // IsConfiguredBy.class.getSimpleName();
	
}
