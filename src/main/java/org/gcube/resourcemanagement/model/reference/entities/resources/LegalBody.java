/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.LegalBodyImpl;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.BelongsTo;


/**
 * LegalBody represents any legal entity playing the role of an Actor.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Legal_Body
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=LegalBodyImpl.class)
@ResourceSchema(
	resources= {
		@RelatedResourcesEntry(source=Person.class, relation=BelongsTo.class, target=LegalBody.class)
	}
)
@TypeMetadata(
	name = LegalBody.NAME, 
	description = "LegalBody represents any legal entity playing the role of an Actor.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface LegalBody extends Actor {

	public static final String NAME = "LegalBody"; // LegalBody.class.getSimpleName();
		
}
