/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.DiscoversImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;

/**
 * Any {@link EService} or its specialisations can be related with 
 * another {@link EService} with Discovers relation.
 * Discovers relation inform that the source {@link EService} 
 * discovers the target through the information system.
 * 
 * Discovers relation specialises the semantic of {@link CallsFor}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Discovers
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=DiscoversImpl.class)
@TypeMetadata(
	name = Discovers.NAME, 
	description = "Any {@link EService} or its specialisations can be related with "
			+ "another {@link EService} with Discovers relation. "
			+ "Discovers relation inform that the source {@link EService} discovers "
			+ "the target through the information system."
			+ "Discovers relation specialise the semantic of {@link CallsFor}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Discovers<Out extends EService, In extends EService> 
	extends CallsFor<Out, In> {

	public static final String NAME = "Discovers"; // Discovers.class.getSimpleName();
	
}
