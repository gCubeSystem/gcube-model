package org.gcube.resourcemanagement.model.reference.properties;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.properties.RegexPropertyImpl;
import org.gcube.resourcemanagement.model.reference.properties.utilities.PropertyValidator;
import org.gcube.resourcemanagement.model.reference.properties.utilities.ValidatedTypedProperty;
import org.gcube.resourcemanagement.model.reference.properties.utilities.Validation;

/**
 * A property validated with a regular expression.
 * 
 * @author Manuele Simi (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 *
 */
@JsonDeserialize(as=RegexPropertyImpl.class)
@TypeMetadata(
	name = RegexProperty.NAME, 
	description = "A property validated with a regular expression.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface RegexProperty extends GCubeProperty, ValidatedTypedProperty<String, String> {

	public static final String NAME = "RegexProperty"; // RegexProperty.class.getSimpleName();

	@Override
	default Validation validate() {
		return new RegexValidator().validate(this);
	}
	
	/**
	 * Validator for {@link RegexProperty} properties. 
	 * @author Manuele Simi (ISTI CNR)
	 *
	 */
	class RegexValidator implements PropertyValidator<RegexProperty> {

		@Override
		public Validation validate(RegexProperty property) {
			if (Objects.isNull(property.getValue()))
				return Validation.fail("The value of the regex property is not set.");
			if (Objects.isNull(property.getSchema()))
				return Validation.fail("The type of the regex property is not set.");
			
			Pattern pattern = Pattern.compile(property.getSchema());
		    Matcher matcher = pattern.matcher(property.getValue());
			return matcher.find() ? 
					Validation.success("Accepted!") : Validation.fail(property.getValue() + " was not a valid match!");
		}
	}
	
	
	@ISProperty
	@Override
	public String getValue();
	
	@Override
	public void setValue(String value);
	
	@ISProperty
	@Override
	public String getSchema();
	
	@Override
	public void setSchema(String type);
	
}
