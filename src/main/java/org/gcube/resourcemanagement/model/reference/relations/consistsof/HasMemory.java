/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasMemoryImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;

/**
 * HasMemory is a base relation type to capture the diverse type of memories associated with a resource.
 * This relation is abstract because if not specialised it does not add any semantic of
 * relating the resource with the target {@link MemoryFacet}.
 * 
 * It is in charge of the specialisation {@link HasVolatileMemory} and {@link HasPersistentMemory} 
 * to clarify the semantics of the memory (any resource describing a computing machine must have at least 
 * two types of memories, i.e., persistent and volatile.)
 * 
 * We do not exclude other specialisation required by services to select the appropriated memory better.
 * For example, to discriminate between Solid State Disk and Magnetic Rotative. 
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasMemory
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonDeserialize(as=HasMemoryImpl.class)
@TypeMetadata(
	name = HasMemory.NAME, 
	description = "HasMemory is a base relation type to capture the diverse type of memories associated with a resource. "
			+ "This relation is abstract because if not specialised it does not add any semantic of relating the "
			+ "resource with the target {@link MemoryFacet}. "
			+ "It is in charge of the specialisation {@link HasVolatileMemory} and {@link HasPersistentMemory} "
			+ "to clarify the semantics of the memory "
			+ "(any resource describing a computing machine must have at least two types of memories, i.e., persistent and volatile)."
			+ "We do not exclude other specialisation required by services to select the appropriated memory better. "
			+ "For example, to discriminate between Solid State Disk and Magnetic Rotative.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasMemory<Out extends Resource, In extends MemoryFacet> 
	extends ConsistsOf<Out, In> {

	public static final String NAME = "HasMemory"; // HasMemory.class.getSimpleName();
	
}