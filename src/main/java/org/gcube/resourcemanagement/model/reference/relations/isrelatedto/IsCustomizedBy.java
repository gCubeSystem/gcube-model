/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.IsCustomizedByImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConfigurationTemplate;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;

/**
 * IsCustomizedBy evidences that any {@link Service} can be customised by a {@link ConfigurationTemplate}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#IsCustomizedBy
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=IsCustomizedByImpl.class)
@TypeMetadata(
	name = IsCustomizedBy.NAME, 
	description = "IsCustomizedBy evidences that any {@link Service} can be customised by a {@link ConfigurationTemplate}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsCustomizedBy<Out extends Service, In extends ConfigurationTemplate> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "IsCustomizedBy"; //IsCustomizedBy.class.getSimpleName();
	
}
