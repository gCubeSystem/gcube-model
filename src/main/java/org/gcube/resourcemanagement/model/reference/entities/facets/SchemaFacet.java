/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.SchemaFacetImpl;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * SchemaFacet captures information on any schema, 
 * i.e., a vocabulary used to validate a document associated with a resource.
 * 
 * Examples of schema are JavaScript Object Notation (JSON) schema and XML schema.
 *  
 * JSON schema "is a vocabulary that allows you to annotate and validate JSON documents". 
 * JSON schema is under standardisation by Internet Engineering Task force (IETF) 
 * (see references at https://json-schema.org/specification.html). 
 * XSD defines the legal building blocks of an XML document.
 * DTD defines the structure and the legal elements and attributes of an XML document.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Schema_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=SchemaFacetImpl.class)
@TypeMetadata(
	name = SchemaFacet.NAME, 
	description = "SchemaFacet captures information on any schema, "
			+ "i.e., a vocabulary used to validate a document associated with a resource. "
			+ "Examples of schema are JavaScript Object Notation (JSON) schema and XML schema. "
			+ "JSON schema \"is a vocabulary that allows you to annotate and validate JSON documents\". "
			+ "JSON schema is under standardisation by Internet Engineering Task force (IETF) "
			+ "(see references at https://json-schema.org/specification.html). "
			+ "XSD defines the legal building blocks of an XML document. "
			+ "DTD defines the structure and the legal elements and attributes of an XML document.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface SchemaFacet extends Facet {
	
	public static final String NAME = "SchemaFacet"; // SchemaFacet.class.getSimpleName();
	
	public static final String NAME_PROPERTY = "name";
	
	@ISProperty(name=NAME_PROPERTY, description = "Schema Name", mandatory=true, nullable=false)
	public String getName();
	
	public void setName(String name);
	
	@ISProperty(description = "Schema Description", mandatory=true, nullable=false)
	public String getDescription();
	
	public void setDescription(String description);
	
	@ISProperty(description = "The 'value' property contains the defined 'schema' "
			+ "that in turn is validated by the schema available at the URL indicated in the ’schema’ property. "
			+ "An example could be an XSD schema instantiation as 'value' and the URL of the DTD defining the XSD as 'schema' "
			+ "i.e., https://www.w3.org/2009/XMLSchema/XMLSchema.dtd.")
	public ValueSchema getSchema();
	
	public void setSchema(ValueSchema schema);
	
}