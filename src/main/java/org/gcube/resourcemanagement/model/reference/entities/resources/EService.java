/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.EServiceImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.LicenseFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.StateFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Discovers;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Uses;

/**
 * EService is any running service that is registered in the infrastructure and 
 * made available by an access point.
 * 
 * EService has similar meaning of PE8_EService defined in PARTHENOS Entities Model (PE Model).
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#EService
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as = EServiceImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation = IsIdentifiedBy.class, facet = SoftwareFacet.class, min = 1, max=1, description = "The main software enabling the EService capabilities."),
		@ResourceSchemaEntry(facet = SoftwareFacet.class, description = "Software available in the EService environment that characterizes the specific EService instance."),
		@ResourceSchemaEntry(facet = AccessPointFacet.class, min = 1, description = "Identify the endpoints of the EService."),
		@ResourceSchemaEntry(facet = EventFacet.class, min = 1, description = "Events characterising the current status and lifecycle of the service, e.g. ActivationTime, DeploymentTime."),
		@ResourceSchemaEntry(facet = StateFacet.class, min = 1, max = 1, description = "The current status of the EService, e.g. STARTED, ready, down, failed."),
		@ResourceSchemaEntry(facet = LicenseFacet.class, description = "The specific terms of use governing the exploitation of the EService.")
	},
	resources= {
		@RelatedResourcesEntry(source=EService.class, relation=Discovers.class, target=EService.class, description="A reference to any other EService, the EService instance is discovering through query on IS."),
		@RelatedResourcesEntry(source=EService.class, relation=Uses.class, target=EService.class, description="A reference to any other EService, the EService instance is invoking.")
	}
)
@TypeMetadata(
	name = EService.NAME, 
	description = "EService is any running service that is registered in the infrastructure and made available by an access point.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface EService extends Service {
	
	public static final String NAME = "EService"; // EService.class.getSimpleName();
	
	public static EService getInstance() {
		return new EServiceImpl();
	}
	
}
