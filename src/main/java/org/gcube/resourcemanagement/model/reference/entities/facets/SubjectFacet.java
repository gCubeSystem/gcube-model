/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.SubjectFacetImpl;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * SubjectFacet captures information on subjects associated with the resource for 
 * description, classification and discovery purposes.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Subject_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=SubjectFacetImpl.class)
@TypeMetadata(
	name = SubjectFacet.NAME, 
	description = "SubjectFacet captures information on subjects associated with "
			+ "the resource for description, classification and discovery purposes.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface SubjectFacet extends Facet {
	
	public static final String NAME = "SubjectFacet"; // SubjectFacet.class.getSimpleName();
	
	@ISProperty(description = "The value of the subject according to the schema", mandatory=true, nullable=false)
	public ValueSchema getSubject();
	
	public void setSubject(ValueSchema subject);

}