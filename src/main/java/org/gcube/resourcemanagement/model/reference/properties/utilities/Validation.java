package org.gcube.resourcemanagement.model.reference.properties.utilities;

/**
 * Result of a {@link PropertyValidator}.
 * 
 * @author Manuele Simi (ISTI CNR)
 */
public class Validation {
	
	private final String message;
	private final boolean success;
	
	private Validation(String message, boolean success) {
		this.message = message;
		this.success = success;
	}
	
	public static Validation success(String message) {
		return new Validation(message, true);
	}
	
	public static Validation fail(String message) {
		return new Validation(message, false);
	}
	
	public String getMessage() {
		return message;
	}
	
	public boolean isSuccess() {
		return success;
	}
}
