package org.gcube.resourcemanagement.model.reference.entities.resources;

import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.GCubeResourceImpl;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;

/**
 * Marker type for any gCube Resource extended in the gCube model
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonDeserialize(as=GCubeResourceImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=IsIdentifiedBy.class, min=1, max=1, description="Any Resource has at least one Facet which in some way allow to identify the Resource per se.")
	}
)
@TypeMetadata(
	name = GCubeResource.NAME, 
	description = "Marker type for any gCube Resource extended in the gCube model.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface GCubeResource extends Resource {
	
	public static final String NAME = "GCubeResource"; // GCubeResource.class.getSimpleName();
	
	@JsonIgnore
	public List<? extends Facet> getIdentificationFacets();
	
}
