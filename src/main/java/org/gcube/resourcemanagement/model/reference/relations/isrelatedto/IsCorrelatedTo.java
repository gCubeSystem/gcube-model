/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.IsCorrelatedToImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;

/**
 * IsCorrelatedTo relates a {@link Dataset} to another. 
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#IsCorrelatedTo
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=IsCorrelatedToImpl.class)
@TypeMetadata(
	name = IsCorrelatedTo.NAME, 
	description = "IsCorrelatedTo relates a {@link Dataset} to another.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsCorrelatedTo<Out extends Dataset, In extends Dataset> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "IsCorrelatedTo"; // IsCorrelatedTo.class.getSimpleName();
	
}
