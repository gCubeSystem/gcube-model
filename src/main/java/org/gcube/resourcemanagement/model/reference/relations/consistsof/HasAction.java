package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasActionImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ActionFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;

/**
 * Relation among a {@link Service} and its {@link ActionFacet}.
 * 
 * @author Manuele Simi (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonDeserialize(as=HasActionImpl.class)
@TypeMetadata(
	name = HasAction.NAME, 
	description = "Relation among a {@link Service} and its {@link ActionFacet}",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasAction<Out extends Service, In extends ActionFacet> extends ConsistsOf<Out, In> {
	
	public static final String NAME = "HasAction"; //HasAction.class.getSimpleName();
	
}