package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasRemoveActionImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ActionFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;

/**
 * An action triggered when a {@link Service} is deactivated.
 * 
 * @author Manuele Simi (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasRemoveActionImpl.class)
@TypeMetadata(
	name = HasRemoveAction.NAME, 
	description = "An action triggered when a {@link Service} is deactivated.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasRemoveAction<Out extends Service, In extends ActionFacet> 
extends HasAction<Out, In> {
	
	public static final String NAME = "HasRemoveAction"; // HasRemoveAction.class.getSimpleName();

}