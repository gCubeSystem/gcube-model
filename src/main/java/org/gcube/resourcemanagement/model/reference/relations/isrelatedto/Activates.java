/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.ActivatesImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.entities.resources.RunningPlugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualMachine;

/**
 * Activates has a {@link Service} instance both as source and target as well as 
 * {@link CallsFor} but with a complete different semantic. 
 * Activates is used to indicates that the source {@link Service} of the relation 
 * enable the operation of the target Service.
 * Examples are between a {@link VirtualMachine} and an {@link HostingNode} to capture 
 * the {@link VirtualMachine} is operating the {@link HostingNode}.
 * Another example is between a {@link VirtualMachine} and an {@link EService}, 
 * e.g., between a {@link VirtualMachine} and a database instance. 
 * This relation can be created also between an {@link HostingNode} and one 
 * {@link EService}, e.g., to represent a container e.g., Tomcat and the web-service(s) 
 * is operating. 
 * Activates is also used between an {@link EService} and a {@link RunningPlugin} 
 * which enrich the functionality offered by the source service.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Activates
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ActivatesImpl.class)
@TypeMetadata(
	name = Activates.NAME, 
	description = "Activates has a {@link Service} instance both as source and target as "
			+ "well as {@link CallsFor} but with a complete different semantic. "
			+ "Activates is used to indicates that the source {@link Service} of the relation "
			+ "enable the operation of the target Service. "
			+ "Examples are between a {@link VirtualMachine} and an {@link HostingNode} to "
			+ "capture the {@link VirtualMachine} is operating the {@link HostingNode}. "
			+ "Another example is between a {@link VirtualMachine} and an {@link EService}, "
			+ "e.g., between a {@link VirtualMachine} and a database instance. "
			+ "This relation can be created also between an {@link HostingNode} and one {@link EService}, "
			+ "e.g., to represent a container e.g., Tomcat and the web-service(s) is operating. "
			+ "Activates is also used between an {@link EService} and a {@link RunningPlugin} which "
			+ "enrich the functionality offered by the source service.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Activates<Out extends Service, In extends Service> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "Activates"; //CallsFor.class.getSimpleName();
	
}
