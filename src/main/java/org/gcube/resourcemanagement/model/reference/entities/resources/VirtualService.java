/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.VirtualServiceImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Demands;

/**
 * VirtualService is an abstract service (non-physically existing service) worth being
 * represented as an existing Service for management purposes.
 * Examples of usage include cases where set of services have to 
 * be managed like an existing unit.
 * This resource is essential from infrastructure management point of view because 
 * it allows easily share a pool of services across VREs as a single unit.
 * 
 * VirtualService mainly consist of a service definition which uses relations to 
 * {@link ConfigurationTemplate}, {@link EService}, 
 * {@link Software} (using {@link Demands} relation) to properly support the sharing across VREs. 
 * 
 * The correct sharing is feasible thanks to the {@link PropagationConstraint} of the model. 
 * 
 * The IS provides only the support for resource sharing as a bundle.
 * Instead, the actions required to deploy a {@link Software} are a responsibility of the service
 * invoking the sharing operation.
 * 
 * This resource emerged thank to the experience maturated with 
 * gCube IS V.1 (gCore Based IS) where this resource was represented as a Generic Resource 
 * containing the list of the resource’s id forming the bundle which often lead to inconsistency.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Virtual_Service
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=VirtualServiceImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=IsIdentifiedBy.class, facet=SoftwareFacet.class, min=1),
	},
	resources= {
		@RelatedResourcesEntry(source=VirtualService.class, relation=Demands.class, target=Software.class)
	}
)
@TypeMetadata(
	name = VirtualService.NAME, 
	description = "VirtualService is an abstract service (non-physically existing service) worth being "
			+ "represented as an existing Service for management purposes. "
			+ "Examples of usage include cases where classes or set of services have to be managed like an existing unit. "
			+ "This resource is essential from infrastructure management point of view because "
			+ "it allows easily share a pool of services across VREs as a single unit. "
			+ "VirtualService mainly consist of a service definition which uses relations to {@link ConfigurationTemplate}, "
			+ "{@link EService}, {@link Software} (using {@link Demands} relation) to properly support the sharing across VREs. "
			+ "The correct sharing is feasible thanks to the {@link PropagationConstraint} of the model. "
			+ "The IS provides only the support for resource sharing as a bundle. "
			+ "Instead, the actions required to deploy a {@link Software} are a responsibility "
			+ "of the service invoking the sharing operation. "
			+ "This resource emerged thank to the experience maturated with gCube IS V.1 (gCore Based IS) "
			+ "where this resource was represented as a Generic Resource containing "
			+ "the list of the resource’s id forming the bundle which often lead to inconsistency.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface VirtualService extends Service {
	
	public static final String NAME = "VirtualService"; // VirtualService.class.getSimpleName();

}
