/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.RequiresImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;

/**
 * Requires is used to indicate that the source {@link Software} when activated requires 
 * to interact with a specific {@link Service} instance, i.e., the target of the relation. 
 * This relation usually reflects an administrative constraint than a technological limitation. 
 * When there are no constraint on the instance the relation is instead {@link DependsOn} 
 * which is between two {@link Software}.
 * 
 * Not necessarily all the {@link Software} running in the infrastructure are represented as 
 * resources. Only the {@link Software} required for infrastructure administration or the 
 * ones to be managed with specific policies are represented. 
 * Indeed, Requires relation is normally used as policy constraint.
 * 
 * Let imagine an open source software which uses a MySQL database as backend. 
 * In many cases, what characterises the software instance is the data present in the backend. 
 * The infrastructure manager could stipulate an agreement with a provider having a particular 
 * set of data which is not for public domain. 
 * The software once deployed, giving the business agreement, is entitled to use the instance 
 * of the provider and not a generic MySQL database instance. 
 * 
 * The infrastructure manager imposes the use of such a particular instance because of the data it contains.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Requires
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=RequiresImpl.class)
@TypeMetadata(
	name = Requires.NAME, 
	description = "Requires is used to indicate that the source {@link Software} when activated requires "
			+ "to interact with a specific {@link Service} instance, i.e., the target of the relation. "
			+ "This relation usually reflects an administrative constraint than a technological limitation. "
			+ "When there are no constraint on the instance the relation is instead {@link DependsOn} "
			+ "which is between two {@link Software}."
			+ "Not necessarily all the {@link Software} running in the infrastructure are represented as resources. "
			+ "Only the {@link Software} required for infrastructure administration or the ones to be managed "
			+ "with specific policies are represented. "
			+ "Indeed, Requires relation is normally used as policy constraint. "
			+ "Let imagine an open source software which uses a MySQL database as backend. "
			+ "In many cases, what characterises the software instance is the data present in the backend. "
			+ "The infrastructure manager could stipulate an agreement with a provider having a particular "
			+ "set of data which is not for public domain. "
			+ "The software once deployed, giving the business agreement, is entitled to use the instance "
			+ "of the provider and not a generic MySQL database instance. "
			+ "The infrastructure manager imposes the use of such a particular instance because of the data it contains.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Requires<Out extends Software, In extends Service> 
	extends IsRelatedTo<Out, In> {
	
	public static final String NAME = "Requires"; // Requires.class.getSimpleName();
	
}
