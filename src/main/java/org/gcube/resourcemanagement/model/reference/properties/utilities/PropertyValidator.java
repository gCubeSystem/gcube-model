package org.gcube.resourcemanagement.model.reference.properties.utilities;

/**
 * Validator for {@link TypedProperty}s.
 * 
 * @param <P> the property to validate
 *
 * @author Manuele Simi (ISTI - CNR)
 */
@FunctionalInterface
public interface PropertyValidator<P extends TypedProperty<?, ?>> {
	
	/**
     * Applies this validation to the given property.
     *
     * @return the validation result
     */
	public Validation validate(P property) ;
	
}
