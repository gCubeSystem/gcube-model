/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.ServiceImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CapabilityFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.DescriptiveMetadataFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SubjectFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.CallsFor;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Enables;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsCustomizedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Manages;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Requires;

/**
 * Service (Abstract) represents any typology of service worth registering in the infrastructure.
 * Service has relations with quite all other resources. 
 * If on one side, an Hybrid Data Infrastructure (HDI) is created to manage datasets, 
 * on the other side the HDI born to manage such datasets digitally. 
 * We could affirm that datasets and services are the two pillar resources 
 * around which revolves the entire infrastructure.
 * It is important to highlight that Service has a general meaning and must 
 * not be intended as a network-callable service which is represented instead 
 * by one of its specialisations called {@link EService}.
 * 
 * 
 * Giving that Service is abstract no {@link IsIdentifiedBy} association with a facet is provided
 * which in turns is responsibility of the specialisation.
 * 
 * Service could be intended as a specialisation of 
 * the entity PE1_Service defined in PARTHENOS Entities Model (PE Model) 
 * and the entity D1_Digital_Object defined in CRMdig.

 * https://wiki.gcube-system.org/gcube/GCube_Model#Service
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonDeserialize(as=ServiceImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(facet=DescriptiveMetadataFacet.class, description="Any descriptive information associated with the service, e.g. for discovery purposes."),
		@ResourceSchemaEntry(facet=SubjectFacet.class, description="Any subject/tag associated with the service for descriptive, cataloguing and discovery purposes."),
		@ResourceSchemaEntry(facet=CapabilityFacet.class, description="Any facility supported/offered by the Service.")
	},
	resources= {
		@RelatedResourcesEntry(source=Service.class, relation=CallsFor.class, target=Service.class, description="A reference to the Services needed by the target instance to work."),
		@RelatedResourcesEntry(source=Service.class, relation=Activates.class, target=Service.class, description="The source Service enables the target Service to be running. Examples are an Hosting Node which enables an EService; A VirtualMachine enabling an EService or an HostingNode; An EService enabling a RunningPlugin; A VirtualMachine enabling an HostingNode etc"),
		@RelatedResourcesEntry(source=Service.class, relation=IsCustomizedBy.class, target=ConfigurationTemplate.class, description="A reference to any configuration characterising the Service behaviour."),
		@RelatedResourcesEntry(source=Service.class, relation=Manages.class, target=Dataset.class, description="A reference to any Dataset resource managed by the Service instance."),
		@RelatedResourcesEntry(source=Service.class, relation=Enables.class, target=Software.class, description="A reference to any Software the Service is enabling (i.e. the Software is running throught the EService)."),
		@RelatedResourcesEntry(source=Service.class, relation=Requires.class, target=Service.class, description="A reference to any Service needed by a Software to properly operate, e.g. this can be used to capture a sort of run-time requirements between a software component and the Service it needs.")
	}
)
@TypeMetadata(
	name = Service.NAME, 
	description = "Service (Abstract) represents any typology of service worth registering in the infrastructure. "
			+ "Service has relations with quite all other resources. "
			+ "If on one side, an Hybrid Data Infrastructure (HDI) is created to manage datasets, "
			+ "on the other side the HDI born to manage such datasets digitally. "
			+ "We could affirm that datasets and services are the two pillar resources "
			+ "around which revolves the entire infrastructure. "
			+ "It is important to highlight that Service has a general meaning and must "
			+ "not be intended as a network-callable service which is represented instead "
			+ "by one of its specialisation called {@link EService}."
			+ "Giving that Service is abstract no {@link IsIdentifiedBy} association with "
			+ "a facet is provided which in turns is responsibility of the specialisation.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Service extends GCubeResource {

	public static final String NAME = "Service"; // Service.class.getSimpleName();
	
}
