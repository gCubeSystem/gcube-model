/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.SimplePropertyFacetImpl;

/**
 * SimplePropertyFacet captures information on any property by a simple name-value pair.
 * It is mainly used to add key-value pairs to the resource in order to describe some resource characteristics. 
 * Before using SimplePropertyFacet a developer should evaluate if it is possible to identify a specific Facet 
 * to capture the particular aspect of the resource. 
 * The usage of SimplePropertyFacet should be reduced to the maximum.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Simple_Property_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=SimplePropertyFacetImpl.class)
@TypeMetadata(
	name = SimplePropertyFacet.NAME, 
	description = "Collect name-value property",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface SimplePropertyFacet extends Facet {
	
	public static final String NAME = "SimplePropertyFacet"; // SimplePropertyFacet.class.getSimpleName();
	
	@ISProperty(mandatory=true, nullable=false)
	public String getName();
	
	public void setName(String name);
	
	@ISProperty(mandatory=true, nullable=false)
	public String getValue();
	
	public void setValue(String value);
	
}