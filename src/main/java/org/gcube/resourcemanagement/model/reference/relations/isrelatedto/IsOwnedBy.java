/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.IsOwnedByImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;
import org.gcube.resourcemanagement.model.reference.entities.resources.Site;

/**
 * Any {@link Site} is owned by an {@link Actor} and this is captured by the IsOwnedBy relation. 
 * The referenced {@link Actor} can be used as a contact point during an emergency, 
 * to agree on the scheduling of a site downtime and to request additional resources 
 * during the downtime of another site.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#IsOwnedBy
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=IsOwnedByImpl.class)
@TypeMetadata(
	name = IsOwnedBy.NAME, 
	description = "Any {@link Site} is owned by an {@link Actor} and this is captured by the IsOwnedBy relation. "
			+ "The referenced {@link Actor} can be used as a contact point during an emergency, "
			+ "to agree on the scheduling of a site downtime and to request additional resources "
			+ "during the downtime of another site.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsOwnedBy<Out extends Site, In extends Actor> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "IsOwnedBy"; // IsOwnedBy.class.getSimpleName();
	
}
