/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.VirtualMachineImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CPUFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.NetworkingFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.StateFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasPersistentMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasVolatileMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;

/**
 * VirtualMachine (VM) is the typical resource represented in a cloud infrastructure.
 * It is an emulation of a physical computer which appears to the running 
 * operative system as real hardware. 
 * A VM provides operative system functionality and potentially allows 
 * to install any software designed for the running operative system.
 * 
 * VirtualMachine could be though as a specialisation of the entity 
 * PE6_Software_Hosting_Service defined in PARTHENOS Entities Model (PE Model).
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Virtual_Machine
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=VirtualMachineImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=IsIdentifiedBy.class, facet=NetworkingFacet.class, min=1, max=1, description="The Network ID characterising the Virtual Machine."),
		@ResourceSchemaEntry(facet=CPUFacet.class, min=1, description="The CPU equipping the Virtual Machine."),
		@ResourceSchemaEntry(relation=HasPersistentMemory.class, facet=MemoryFacet.class, min=1, description="The Disk Space Capacity of the Virtual Machine."),
		@ResourceSchemaEntry(relation=HasVolatileMemory.class, facet=MemoryFacet.class, min=1, description="The RAM Capacity of the Virtual Machine."),
		@ResourceSchemaEntry(facet=EventFacet.class, min=1, description="Every event characterizing the life cycle of the Virtual Machine, e.g. the activation time."),
		@ResourceSchemaEntry(facet=StateFacet.class, min=1, max=1, description="The current state of the Virtual Machine, e.g. started, ready, down, unreachable."),
		@ResourceSchemaEntry(facet=SoftwareFacet.class, description=" Any Software characterising the Virtual Machine. Useful to report the hosted software that are not registered in the Resource Registry as Software Resource, e.g. Operating System")
	}
)
@TypeMetadata(
	name = VirtualMachine.NAME, 
	description = "VirtualMachine (VM) is the typical resource represented in a cloud infrastructure. "
			+ "It is an emulation of a physical computer which appears to the running operative system as real hardware. "
			+ "A VM provides operative system functionality and potentially allows "
			+ "to install any software designed for the running operative system.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface VirtualMachine extends Service {

	public static final String NAME = "VirtualMachine"; //VirtualMachine.class.getSimpleName();
	
}
