/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.IsCompliantWithImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;
import org.gcube.resourcemanagement.model.reference.entities.resources.Schema;

/**
 * IsCompliantWith is used to indicated a {@link Dataset} compliant with a {@link Schema}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#IsCompliantWith
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=IsCompliantWithImpl.class)
@TypeMetadata(
	name = IsCompliantWith.NAME, 
	description = "IsCompliantWith is used to indicated a {@link Dataset} compliant with a {@link Schema}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsCompliantWith<Out extends Dataset, In extends Schema> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "IsCompliantWith"; // IsCompliantWith.class.getSimpleName();
	
}
