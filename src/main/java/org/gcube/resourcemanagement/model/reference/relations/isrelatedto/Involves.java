/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.InvolvesImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;

/**
 * Involves is used to indicate that and {@link Actor} is involved in a
 * {@link Dataset}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Involves
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=InvolvesImpl.class)
@TypeMetadata(
	name = Involves.NAME, 
	description = "Involves is used to indicate that and {@link Actor} is involved in a {@link Dataset}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Involves<Out extends Dataset, In extends Actor> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "Involves"; // Involves.class.getSimpleName();
	
}
