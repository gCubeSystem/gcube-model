package org.gcube.resourcemanagement.model.reference.properties.utilities;

/**
 * @author Manuele Simi (ISTI - CNR)
 */
public interface TypedProperty<T, V> {
	
	public V getValue();
	
	public void setValue(V value);
	
	public T getSchema();
	
	public void setSchema(T type);
	
}
