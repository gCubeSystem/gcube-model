/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasSpatialCoverageImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CoverageFacet;

/**
 * HasSpatialCoverage indicates that the target {@link CoverageFacet} indicates a spatial 
 * coverage information, e.g., the geographic area indication for the dataset.
 *
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasSpatialCoverage
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasSpatialCoverageImpl.class)
@TypeMetadata(
	name = HasSpatialCoverage.NAME, 
	description = "HasSpatialCoverage indicates that the target {@link CoverageFacet} indicates "
			+ "a spatial coverage information, e.g., the geographic area indication for the dataset.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasSpatialCoverage<Out extends Resource, In extends CoverageFacet> 
	extends HasCoverage<Out, In> {

	public static final String NAME = "HasSpatialCoverage"; // HasSpatialCoverage.class.getSimpleName();

}