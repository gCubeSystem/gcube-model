/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasCoverageImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CoverageFacet;

/**
 * HasCoverage represents the type of coverage that is provided by the CoverageFacet.
 * Let consider a dataset containing information regarding the salinity of a specific
 * ocean area in a certain amount of time. What is essential in the Information System is
 * not representing the data of the salinity. Instead, to specify the temporal period and the
 * ocean area the dataset is valid. This information is captured using the same schema
 * i.e., the {@link CoverageFacet} but using different relations to distinguish between them 
 * with no need to understand the value or the schema of the coverage.
 * For such a reason it has been defined the abstract HasCoverage relation and the
 * specialisation {@link HasTemporalCoverage} and {@link HasSpatialCoverage} 
 * have been defined to refines the reason of using the {@link CoverageFacet}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasCoverage
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonDeserialize(as=HasCoverageImpl.class)
@TypeMetadata(
	name = HasCoverage.NAME, 
	description = "HasCoverage represents the type of coverage that is provided by the CoverageFacet."
			+ "Let consider a dataset containing information regarding the salinity of a specific "
			+ "ocean area in a certain amount of time. What is essential in the Information System is"
			+ "not representing the data of the salinity. Instead, to specify the temporal period and the "
			+ "ocean area the dataset is valid. This information is captured using the same schema, "
			+ "i.e., the {@link CoverageFacet} but using different relations to distinguish between them "
			+ "with no need to understand the value or the schema of the coverage. "
			+ "For such a reason it has been defined the abstract HasCoverage relation and the "
			+ "specialisation {@link HasTemporalCoverage} and {@link HasSpatialCoverage} "
			+ "have been defined to refines the reason of using the {@link CoverageFacet}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasCoverage<Out extends Resource, In extends CoverageFacet> 
	extends ConsistsOf<Out, In> {

	public static final String NAME = "HasCoverage"; //HasCoverage.class.getSimpleName();
	
}