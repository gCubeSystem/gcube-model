/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.SchemaImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.DescriptiveMetadataFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SchemaFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SubjectFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasContact;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsCompliantWith;

/**
 * Schema is any reference schema used to specify compliant values. 
 * Examples include controlled vocabularies, ontologies, and others.
 * This resource is mainly used by {@link Dataset} to evidence that 
 * is compliant with a Schema by using {@link IsCompliantWith} relation.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Schema
 *
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=SchemaImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=IsIdentifiedBy.class, facet=SchemaFacet.class, min=1, max=1),
		@ResourceSchemaEntry(relation=HasContact.class, facet=ContactFacet.class, min=1),
		@ResourceSchemaEntry(facet=DescriptiveMetadataFacet.class),
		@ResourceSchemaEntry(facet=SubjectFacet.class)
	},
	resources = {
		@RelatedResourcesEntry(source=Dataset.class, relation=IsCompliantWith.class, target=Schema.class, description="Any schema characterising the content of the dataset, e.g. the schema describing the 'columns' of a CSV-based dataset.")
	}
)
@TypeMetadata(
	name = Schema.NAME, 
	description = "Schema is any reference schema used to specify compliant values. "
			+ "Examples include controlled vocabularies, ontologies, and others. "
			+ "This resource is mainly used by {@link Dataset} to evidence that "
			+ "is compliant with a Schema by using {@link IsCompliantWith} relation.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Schema extends GCubeResource {
	
	public static final String NAME = "Schema"; // Schema.class.getSimpleName();

}