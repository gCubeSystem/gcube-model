/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasTemporalCoverageImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CoverageFacet;

/**
 * HasTemporalCoverage indicates that the target {@link CoverageFacet} indicates a temporal 
 * coverage information, e.g., the temporal period indication for the dataset.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasTemporalCoverage
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasTemporalCoverageImpl.class)
@TypeMetadata(
	name = HasTemporalCoverage.NAME, 
	description = "HasTemporalCoverage indicates that the target {@link CoverageFacet} indicates "
			+ "a temporal coverage information e.g., the temporal period indication for the dataset.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasTemporalCoverage<Out extends Resource, In extends CoverageFacet> 
	extends HasCoverage<Out, In> {

	public static final String NAME = "HasTemporalCoverage"; // HasTemporalCoverage.class.getSimpleName();
}
