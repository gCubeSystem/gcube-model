/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.ActorImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactReferenceFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.BelongsTo;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Involves;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsOwnedBy;

/**
 * Actor (Abstract) is any entity (human or machine) playing an active role in the infrastructure. 
 * Actor has two specialisations, {@link LegalBody} which represent any legal entity, and 
 * {@link Person} which is any human playing the role of Actor. 
 * An Actor can belong to a {@link LegalBody} and this is expressed using the defined 
 * {@link BelongsTo} relation
 * 
 * Actor has similar meaning of E39_Actor defined in CDOC-CRM.
 * Similarly LegalBody and Person have similar meaning of  
 * E40_Legal_Body and E21_Person respectively.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Actor
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonDeserialize(as=ActorImpl.class)
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=IsIdentifiedBy.class, facet=ContactFacet.class, min=1, max=1, description=" An Actor has at least a Contact Facet which permit to identify the Actor per se. "),
		@ResourceSchemaEntry(facet=ContactReferenceFacet.class)
	},
	resources= {
		@RelatedResourcesEntry(source=Dataset.class, relation=Involves.class, target=Actor.class),
		@RelatedResourcesEntry(source=Site.class, relation=IsOwnedBy.class, target=Actor.class)
	}
)
@TypeMetadata(
	name = Actor.NAME, 
	description = "Actor (Abstract) is any entity (human or machine) playing an active role in the infrastructure. "
			+ "Actor has two specialisations, {@link LegalBody} which represent any legal entity, "
			+ "and {@link Person} which is any human playing the role of Actor. "
			+ "An Actor can belong to a {@link LegalBody} and this is expressed using the defined {@link BelongsTo} relation.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Actor extends GCubeResource {
	
	public static final String NAME = "Actor"; // Actor.class.getSimpleName();
	
}
