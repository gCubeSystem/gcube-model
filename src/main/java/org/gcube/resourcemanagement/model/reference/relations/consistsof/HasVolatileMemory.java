/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasVolatileMemoryImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;

/**
 * HasVolatileMemory indicates that the target {@link MemoryFacet} is a volatile memory,
 * i.e., a memory which requires power to maintain the stored information.
 * Volatile memory is also known as main memory, internal memory or primary storage.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasVolatileMemory
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasVolatileMemoryImpl.class)
@TypeMetadata(
	name = HasVolatileMemory.NAME, 
	description = "HasVolatileMemory indicates that the target {@link MemoryFacet} is a volatile memory, "
			+ "i.e., a memory which requires power to maintain the stored information. "
			+ "Volatile memory is also known as main memory, internal memory or primary storage.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasVolatileMemory<Out extends Resource, In extends MemoryFacet> 
	extends HasMemory<Out, In> {

	public static final String NAME = "HasVolatileMemory"; // HasVolatileMemory.class.getSimpleName();
}
