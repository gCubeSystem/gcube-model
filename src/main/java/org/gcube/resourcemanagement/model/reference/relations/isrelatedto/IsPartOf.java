/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.IsPartOfImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConcreteDataset;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;

/**
 * IsPartOf is used when a {@link ConcreteDataset} is part of a {@link Dataset}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#IsPartOf
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=IsPartOfImpl.class)
@TypeMetadata(
	name = IsPartOf.NAME, 
	description = "IsPartOf is used when a {@link ConcreteDataset} is part of a {@link Dataset}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsPartOf<Out extends ConcreteDataset, In extends Dataset> 
	extends IsCorrelatedTo<Out, In> {

	public static final String NAME = "IsPartOf"; // IsPartOf.class.getSimpleName();
	
}
