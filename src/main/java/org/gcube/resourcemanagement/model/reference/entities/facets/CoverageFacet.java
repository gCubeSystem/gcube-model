/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.CoverageFacetImpl;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * CoverageFacet captures information on the extent of the resource, 
 * i.e., any aspect aiming at capturing an indicator of the amount/area 
 * the resource covers be it a geospatial area, a temporal area, or any other "area".
 * 
 * Let consider a dataset containing information regarding the salinity of a specific
 * ocean area in a certain amount of time. What is essential in the information system is
 * not representing the data of the salinity. Instead, to specify the temporal period and the
 * ocean area the dataset is valid. This information is captured by the CoverageFacet.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Coverage_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=CoverageFacetImpl.class)
@TypeMetadata(
	name = CoverageFacet.NAME, 
	description = "CoverageFacet captures information on the extent of the resource, "
			+ "i.e., any aspect aiming at capturing an indicator of the amount/area "
			+ "the resource covers be it a geospatial area, a temporal area, or any other 'area'. "
			+ "Let consider a dataset containing information regarding the salinity of a specific "
			+ "ocean area in a certain amount of time. What is essential in the information system is "
			+ "not representing the data of the salinity. Instead, to specify the temporal period and the"
			+ "ocean area the dataset is valid. This information is captured by the CoverageFacet.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface CoverageFacet extends Facet {
	
	public static final String NAME = "CoverageFacet"; // CoverageFacet.class.getSimpleName();
	
	@ISProperty(description = "The value indicates the 'area' covered by the dataset according to the schema", mandatory=true, nullable=false)
	public ValueSchema getCoverage();
	
	public void setCoverage(ValueSchema coverage);

}
