/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.consistsof;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasCreatorImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;

/**
 * HasCreator indicates that the target {@link ContactFacet} contains the information related to 
 * a creator of the source resource, e.g., the contact points of the creator of a dataset.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#HasCreator
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HasCreatorImpl.class)
@TypeMetadata(
	name = HasCreator.NAME, 
	description = "HasCreator indicates that the target {@link ContactFacet} contains the information related "
			+ "to a creator of the source resource, e.g., the contact points of the creator of a dataset.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface HasCreator<Out extends Resource, In extends ContactFacet> 
	extends HasContact<Out, In> {

	public static final String NAME = "HasCreator"; // HasCreator.class.getSimpleName();
}
