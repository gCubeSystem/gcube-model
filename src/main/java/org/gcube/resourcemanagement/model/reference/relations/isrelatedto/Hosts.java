/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.HostsImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.entities.resources.Site;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualMachine;

/**
 * Hosts relation is used from a {@link Site} a {@link Service} instance.
 * The target of the Hosts relation depends on the service offered by the Site. 
 * When the resources provided by a site are virtual machines, 
 * Hosts relation is created from a {@link Site} to a {@link VirtualMachine}. 
 * 
 * When, instead a Site provides web-services, Hosts relation is created with {@link EService}. 
 * 
 * If a site provides container facilities Hosts relation is created with {@link HostingNode}. 
 * 
 * By defining Hosts relation with target {@link Service}, the model is capable of representing 
 * the diverse type of federated systems and service.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Hosts
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=HostsImpl.class)
@TypeMetadata(
	name = Hosts.NAME, 
	description = "Hosts relation is used from a {@link Site} a {@link Service} instance. "
			+ "The target of the Hosts relation depends on the service offered by the Site. "
			+ "When the resources provided by a site are virtual machines, "
			+ "Hosts relation is created from a {@link Site} to a {@link VirtualMachine}. "
			+ "When, instead a Site provides web-services, Hosts relation is created with {@link EService}. "
			+ "If a site provides container facilities Hosts relation is created with {@link HostingNode}. "
			+ "By defining Hosts relation with target {@link Service}, the model is capable of representing "
			+ "the diverse type of federated systems and service.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Hosts<Out extends Site, In extends Service> 
	extends IsRelatedTo<Out, In> {
	
	public static final String NAME = "Hosts"; // Hosts.class.getSimpleName();
	
}
