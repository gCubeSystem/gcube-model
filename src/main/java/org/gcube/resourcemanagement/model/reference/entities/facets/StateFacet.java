/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.StateFacetImpl;

/**
 * StateFacet captures information on state to be associated with the resource.
 * The state is captured by any controlled vocabulary which is an integral part of the facet.
 * 
 * Examples of usage are the state of service e.g., running or down or 
 * the state of a virtual machine e.g., activated or unreachable.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#State_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=StateFacetImpl.class)
@TypeMetadata(
	name = StateFacet.NAME, 
	description = "StateFacet captures information on state to be associated with the resource. "
			+ "The state is captured by any controlled vocabulary which is an integral part of the facet. "
			+ "Examples of usage are the state of service e.g., running or down or "
			+ "the state of a virtual machine e.g., activated or unreachable.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface StateFacet extends Facet {

	public static final String NAME = "StateFacet"; // StateFacet.class.getSimpleName();

	@ISProperty(description="The value of the state", mandatory=true, nullable=false)
	public String getValue();
	
	public void setValue(String value);
	
}
