/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.CPUFacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;

/**
 * CPUFacet captures information on the Central Processing Unit (CPU) of the 
 * resource it is associated with. A resource which needs to indicate a 
 * multi-processor/multi-core CPU will consist of more than one CPUFacet. 
 * Even if more than one CPUFacet is associated with a resource 
 * (i.e., an {@link HostingNode}), we did not find any reason to differentiate the CPUs.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#CPU_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=CPUFacetImpl.class)
@TypeMetadata(
	name = CPUFacet.NAME, 
	description = "CPUFacet captures information on the Central Processing Unit (CPU) of the resource it is associated with. "
			+ "A resource which needs to indicate a multi-processor/multi-core CPU will consist of more than one CPUFacet. "
			+ "Even if more than one CPUFacet is associated with a resource (i.e., an {@link HostingNode}), we did not find any reason to differentiate the CPUs.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface CPUFacet extends Facet {
	
	public static final String NAME = "CPUFacet"; //CPUFacet.class.getSimpleName();
	
	@ISProperty(description = "CPU Model", mandatory=true, nullable=false)
	public String getModel();
	
	public void setModel(String model);
	
	@ISProperty(description = "CPU Vendor", mandatory=true, nullable=false)
	public String getVendor();
	
	public void setVendor(String vendor);
	
	@ISProperty(description = "Clock speed expressed with the unit, e.g., 1 GHz.", mandatory=true, nullable=false)
	public String getClockSpeed();
	
	public void setClockSpeed(String clockSpeed);
	
}