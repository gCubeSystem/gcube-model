/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.IdentifierFacetImpl;

/**
 * IdentifierFacet captures information on identifiers (other than the 
 * ones automatically generated by the system) that can be attached to a resource.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Identifier_Facet
 * 
 * Changelog (only model changes are logged here. Not implementation details)
 * 
 * - 1.0.1 Added STRING in IdentificationType enum.
 * - 1.0.0 First version 
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=IdentifierFacetImpl.class)
@TypeMetadata(
	name = IdentifierFacet.NAME, 
	description = "IdentifierFacet captures information on identifiers (other than the ones "
			+ "automatically generated by the system) that can be attached to a resource.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IdentifierFacet extends Facet {
	
	public static final String NAME = "IdentifierFacet"; // IdentifierFacet.class.getSimpleName();
	public static final String VALUE_PROPERTY = "value";
	public static final String IDENTIFICATION_TYPE_PROPERTY = "identificationType";
	
	public enum IdentificationType {
		URI, DOI, IRI, URL, URN, UUID, STRING
	}
	
	@ISProperty(name=VALUE_PROPERTY, description = "The identifier", mandatory=true, nullable=false)
	public String getValue();
	
	public void setValue(String value);
	
	@ISProperty(name = IDENTIFICATION_TYPE_PROPERTY, description = "The typology of identifier", mandatory=true, nullable=false)
	public IdentificationType getIdentificationType();
	
	public void setIdentificationType(IdentificationType type);
	
	@ISProperty(description = "To indicate whether the identifier is persistent or not")
	public boolean isPersistent();
	
	public void setPersistent(boolean persistent);
	
}