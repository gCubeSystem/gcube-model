/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.resources;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.resources.ConfigurationImpl;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsDerivationOf;


/**
 * Configuration is a specialisation of {@link ConfigurationTemplate} 
 * and is an instance of a configuration template characterising 
 * the behaviour and shape of the resource it is attached to.
 * The Configuration can be related to the template it derives using {@link IsDerivationOf}.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Configuration
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ConfigurationImpl.class)
@ResourceSchema(
	resources = {
			@RelatedResourcesEntry(source=Configuration.class, relation=IsDerivationOf.class, target=ConfigurationTemplate.class)
	}
)
@TypeMetadata(
	name = Configuration.NAME, 
	description = "Configuration is a specialisation of {@link ConfigurationTemplate} "
			+ "and is an instance of a configuration template characterising "
			+ "the behaviour and shape of the resource it is attached to. "
			+ "The Configuration can be related to the template it derives using {@link IsDerivationOf}.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Configuration extends ConfigurationTemplate {
	
	public static final String NAME = "Configuration"; // Configuration.class.getSimpleName();
	
}