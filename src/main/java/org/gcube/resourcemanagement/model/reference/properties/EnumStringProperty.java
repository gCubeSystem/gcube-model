package org.gcube.resourcemanagement.model.reference.properties;

import java.util.Set;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.properties.EnumStringPropertyImpl;
import org.gcube.resourcemanagement.model.reference.properties.utilities.PropertyValidator;
import org.gcube.resourcemanagement.model.reference.properties.utilities.ValidatedTypedProperty;
import org.gcube.resourcemanagement.model.reference.properties.utilities.Validation;


/**
 * A string property validated against a set of accepted values.
 * 
 * @author Manuele Simi (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 *
 */
@JsonDeserialize(as=EnumStringPropertyImpl.class)
@TypeMetadata(
	name = EnumStringProperty.NAME, 
	description = "Enum String Property",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface EnumStringProperty extends GCubeProperty, ValidatedTypedProperty<Set<String>, String> {
	
	public static final String NAME = "EnumStringProperty"; //EnumStringProperty.class.getSimpleName();
	
	@Override
	default Validation validate() {
		return new SetStringValidator().validate(this);
	}

	/**
	 * Validator for {@link ValueSchema} properties.
	 * 
	 * @author Manuele Simi (ISTI CNR)
	 *
	 */
	class SetStringValidator implements PropertyValidator<EnumStringProperty> {

		@Override
		public Validation validate(EnumStringProperty property) {
			return property.getSchema().contains(property.getValue()) ? Validation.success("Accepted!")
					: Validation.fail(property.getValue() + " is not a valid value.");
		}
	}
	
}
