package org.gcube.resourcemanagement.model.reference.properties;

import java.net.URI;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.properties.ValueSchemaImpl;
import org.gcube.resourcemanagement.model.reference.properties.utilities.PropertyValidator;
import org.gcube.resourcemanagement.model.reference.properties.utilities.ValidatedTypedProperty;
import org.gcube.resourcemanagement.model.reference.properties.utilities.Validation;

/**
 * This type aims at exposing a value which can be automatically managed by any client with no knowledge of its format.
 * 
 * The client can retrieve the schema at the provided URI, i.e., schema property, 
 * and use its content to understand/validate/manipulate the information contained in the value property.
 * The client must have the capability to understand the semantics of content retrieved at the URI endpoint. 
 *  
 * Examples of application of such type are eXtensible Markup Language (XML) values which can be 
 * validated by a Document Type Definition (DTD) or XML Schema Definition (XSD).
 *  
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ValueSchemaImpl.class)
@TypeMetadata(
	name = ValueSchema.NAME, 
	description = "This type aims at exposing a value which can be automatically managed by any client with no knowledge of its format.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface ValueSchema extends GCubeProperty, ValidatedTypedProperty<URI,String> {

	public static final String NAME = "ValueSchema"; //ValueSchema.class.getSimpleName();
	
	@Override
	default Validation validate() {
		return new SchemaValidator().validate(this);
	}
	
	/**
	 * Validator for {@link ValueSchema} properties. 
	 * @author Manuele Simi (ISTI CNR)
	 *
	 */
	class SchemaValidator implements PropertyValidator<ValueSchema> {

		@Override
		public Validation validate(ValueSchema property) {
			// TODO validate the string against the URI!
			return Validation.success("Accepted!");
		}
	}
	
	/**
	 * The value which schema is available at the URI provided in the schema property.
	 */
	@ISProperty(description = "The value which schema is available at the URI provided in the schema property.", mandatory=true, nullable=false)
	@Override
	public String getValue();
	
	@Override
	public void setValue(String value);
	
	/**
	 * An URI containing a schema used to validate/interpret the content of the value. 
	 * It is only an informative field. The validation is charge of the client.
	 */
	@ISProperty(description = "An URI containing a schema used to validate/interpret the content of the value. It is only an informative field. The validation is in charge of the client.") //, mandatory=true, nullable=false)
	@Override
	public URI getSchema();
	
	@Override
	public void setSchema(URI schema);
	
}
