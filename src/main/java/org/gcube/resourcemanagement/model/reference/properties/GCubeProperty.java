package org.gcube.resourcemanagement.model.reference.properties;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.properties.GCubePropertyImpl;

/**
 * Marker type for {@link Property} properties extended in the gCube Model.
 * 
 * @author Manuele Simi (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 *
 */
@JsonDeserialize(as=GCubePropertyImpl.class)
@TypeMetadata(
	name = GCubeProperty.NAME, 
	description = "Marker type for any properties extended in the gCube model.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface GCubeProperty extends Property {
	
	public static final String NAME = "GCubeProperty"; //GCubeProperty.class.getSimpleName();
	
}