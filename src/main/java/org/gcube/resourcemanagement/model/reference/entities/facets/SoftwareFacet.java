/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.reference.properties.utilities.Named;

/**
 * SoftwareFacet captures information on any software associated with the
 * resource.
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Software_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as = SoftwareFacetImpl.class)
@TypeMetadata(name = SoftwareFacet.NAME, description = "SoftwareFacet captures information on any software associated with the resource.", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface SoftwareFacet extends Facet, Named {

	public static final String NAME = "SoftwareFacet"; // SoftwareFacet.class.getSimpleName();

	public static final String GROUP_PROPERTY = "group";
	public static final String VERSION_PROPERTY = "version";

	public static final String DESCRIPTION_PROPERTY = "description";
	public static final String QUALIFIER_PROPERTY = "qualifier";
	public static final String OPTIONAL_PROPERTY = "optional";

	@Override
	@ISProperty(name = NAME_PROPERTY, description = "The name of the software artifact being described, "
			+ "e.g., artifactId in maven coordinates, " + "the software name for retail software "
			+ "such as 'Office' (in Microsoft™ Office 2013-SP2)", mandatory = true, nullable = false)
	public String getName();

	@Override
	public void setName(String name);

	@ISProperty(name = GROUP_PROPERTY, description = "The name of 'group' the software artifact belongs to, "
			+ "e.g., groupId in maven coordinates, " + "company name for retail software software "
			+ "such as 'Microsoft™' (in Microsoft™ Office 2013-SP2)", mandatory = true, nullable = false)
	public String getGroup();

	public void setGroup(String group);

	@ISProperty(name = VERSION_PROPERTY, description = "The particular release of the software artifact, "
			+ "e.g., version in maven coordinates, " + "the software version for retail software "
			+ "such as '2013-SP2' (in Microsoft™ Office 2013-SP2)", mandatory = true, nullable = false)
	public String getVersion();

	public void setVersion(String version);

	@ISProperty(name = DESCRIPTION_PROPERTY, description = "A human oriented description of the software artifact being described")
	public String getDescription();

	public void setDescription(String description);

	@ISProperty(name = QUALIFIER_PROPERTY, description = "A qualifier for the software, "
			+ "e.g., packaging or scope in maven coordinates, "
			+ "target architecture for retail software such as x86 or amd64")
	public String getQualifier();

	public void setQualifier(String qualifier);

	@ISProperty(name = OPTIONAL_PROPERTY, description = "Used to indicate the software optionality,"
			+ " e.g., optional in maven coordinates")
	public boolean isOptional();

	public void setOptional(boolean optional);

}
