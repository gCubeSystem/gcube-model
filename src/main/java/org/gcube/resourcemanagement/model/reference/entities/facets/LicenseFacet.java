/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.net.URL;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.LicenseFacetImpl;

/**
 * LicenseFacet captures information on any license associated with the 
 * resource to capture the policies governing its exploitation and use.
 * 
 * Example of use is the licence of a dataset e.g., Creative Commons Attribution (CC-BY) 
 * or the licence of software such as GPL. 
 * This facet is used to provide for human knowledge, but it is not excluded the usage
 * by infrastructure services which enforces the respect of the licence e.g., a service which
 * denies the usage of a dataset with Creative Commons Attribution No-Derivatives (CC-BY-ND) 
 * licence to produce a new dataset.
 *  
 * https://wiki.gcube-system.org/gcube/GCube_Model#License_Facet
 *  
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=LicenseFacetImpl.class)
@TypeMetadata(
	name = LicenseFacet.NAME, 
	description = "LicenseFacet captures information on any license associated with the  resource to capture "
			+ "the policies governing its exploitation and use. "
			+ "Example of use is the licence of a dataset e.g., Creative Commons Attribution (CC-BY) "
			+ "or the licence of software such as GPL. "
			+ "This facet is used to provide for human knowledge, but it is not excluded the usage "
			+ "by infrastructure services which enforces the respect of the licence e.g., a service which "
			+ "denies the usage of a dataset with Creative Commons Attribution No-Derivatives (CC-BY-ND)"
			+ "licence to produce a new dataset.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface LicenseFacet extends Facet {

	public static final String NAME = "LicenseFacet"; // LicenseFacet.class.getSimpleName();
	
	@ISProperty(description = "The common name of the license. e.g., European Union Public Licence (EUPL) 1.1, GNU General Public License (GPL) 2, Berkeley Software Distribution (BSD), Common Creative (CC).", mandatory=true, nullable=false)
	public String getName();
	
	public void setName(String name);

	@ISProperty(description = "The URL to the actual text of the license.", mandatory=true, nullable=false)
	public URL getTextURL();
	
	public void setTextURL(URL textURL);
	
}
