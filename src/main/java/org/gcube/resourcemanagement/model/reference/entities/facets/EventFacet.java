/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import java.util.Date;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.properties.Event;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.Changelog;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.EventFacetImpl;

/**
 * EventFacet captures information on a certain event/happening characterising the life cycle of the resource.
 * 
 * Examples of an event are the start time of a virtual machine or the activation time of an electronic service.
 *
 * https://wiki.gcube-system.org/gcube/GCube_Model#Event_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=EventFacetImpl.class)
@TypeMetadata(
	name = EventFacet.NAME, 
	description = "EventFacet captures information on a certain event/happening characterising the life cycle of the resource. "
			+ "Examples of an event are the start time of a virtual machine or the activation time of an electronic service.",
	version = EventFacet.VERSION
)
@Changelog ({
	@Change(version = EventFacet.VERSION, description = "Switching to {@link Event} Property added in the information-system-model"),
	@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
})
public interface EventFacet extends Facet {

	public static final String NAME = "EventFacet"; // EventFacet.class.getSimpleName();
	public static final String VERSION = "2.0.0";
	
	public static final String EVENT_PROPERTY = "event";
	
	@Deprecated
	/**
	 * This method will be removed soon.
	 * Use {@link EventFacet#getTheEvent()} method which return {@link Event} instance. 
	 * The date can be retrieved by using {@link Event#getWhen()} method. 
	 * 
	 * @return the date of the occurred event.
	 */
	public Date getDate();
	
	@Deprecated
	/**
	 * This method will be removed soon.
	 * Use {@link EventFacet#setEvent(Event event)}. 
	 * The date can be set in {@link Event} instance by using 
	 * {@link Event#setWhen(Date date)} method.
	 * 
	 * @param date the date of the occurred event.
	 * 		which is the same of the 'when' Date of the {@link Event} instance.
	 */
	public void setDate(Date date);
	
	@Deprecated
	/**
	 * This method will be removed soon.
	 * Use {@link EventFacet#getTheEvent()} method which return {@link Event} instance. 
	 * The deprecated 'event' String can be retrieved by using {@link Event#getWhat()} method.
	 * 
	 * @return the string of event, e.g. active
	 */
	public String getEvent();
	
	@Deprecated
	/**
	 * This method will be removed soon.
	 * Use {@link EventFacet#setEvent(Event event)}. 
	 * The deprecated 'event' String can be set in {@link Event} instance by using 
	 * {@link Event#setWhat(String what)} method.
	 * 
	 * @param event the string of the event to set, e.g. failed
	 * 		which is the same of the 'what' String of the {@link Event} instance.
	 */
	public void setEvent(String event);
	
	/**
	 * Actually is not mandatory for backward compatibility.
	 * In the next version this method will be deprecated to and it will be replaced
	 * by a method with the following signature
	 * 
	 * public Event getEvent()
	 * 
	 * This is not actually possible because we need to maintain 
	 * {@link EventFacet#getEvent()} for backward compatibility 
	 * 
	 * @return the event
	 */
	@ISProperty(name=EVENT_PROPERTY, type=Object.class, description = "The event eventually described by the five W (What, When, Who, Where, Why)", mandatory=false, nullable=false)
	public Event getTheEvent();
	
	/**
	 * Set the event
	 * @param event the event to set
	 */
	public void setEvent(Event event);
	
}