package org.gcube.resourcemanagement.model.reference.properties;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.properties.AccessPolicyImpl;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=AccessPolicyImpl.class)
@TypeMetadata(
	name = AccessPolicy.NAME, 
	description = "AccessPolicy information",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface AccessPolicy extends GCubeProperty {
	
	public static final String NAME = "AccessPolicy"; //AccessPolicy.class.getSimpleName();
	
	@ISProperty
	public ValueSchema getPolicy();

	public void setPolicy(ValueSchema policy);
	
	@ISProperty
	public String getNote();
	
	public void setNote(String note);
}
