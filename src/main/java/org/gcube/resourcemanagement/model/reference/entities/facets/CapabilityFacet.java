/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.entities.facets;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.entities.facets.CapabilityFacetImpl;

/**
 * CapabilityFacet captures a defined facility to perform a specified task supported by a given Resource.
 * It is mainly used to provide a human-readable description of the capabilities of a resource
 * (e.g., the support for transactions of an electronic device or some non-functional 
 * properties of a service like its replicability to support High-Availability (HA)).
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Capability_Facet
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=CapabilityFacetImpl.class)
@TypeMetadata(
	name = CapabilityFacet.NAME, 
	description = "CapabilityFacet captures a defined facility for performing a specified task supported by a given Resource. "
			+ "It is mainly used to provide a human-readable description of the capabilities of a resource "
			+ "(e.g., the support for transactions of an electronic device or some non-functional "
			+ "properties of a service like its replicability to support High-Availability (HA)).",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface CapabilityFacet extends Facet {

	public static final String NAME = "CapabilityFacet"; // CapabilityFacet.class.getSimpleName();

	@ISProperty(description = "The distinguishing name of the capability.", mandatory=true, nullable=false)
	public String getName();

	public void setName(String name);

	@ISProperty(description = "A human oriented description of the capability.")
	public String getDescription();

	public void setDescription(String description);

	@ISProperty(description = "A string used to specialise the capability.")
	public String getQualifier();

	public void setQualifier(String qualifier);

	

}
