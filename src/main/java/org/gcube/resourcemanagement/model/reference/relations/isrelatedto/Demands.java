/**
 * 
 */
package org.gcube.resourcemanagement.model.reference.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.DemandsImpl;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualService;

/**
 * Demands is used to properly support to share a {@link VirtualService}
 * with another context.  
 * 
 * https://wiki.gcube-system.org/gcube/GCube_Model#Demands
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=DemandsImpl.class)
@TypeMetadata(
	name = Demands.NAME, 
	description = "Demands is used to properly support to share a {@link VirtualService} with another context.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Demands<Out extends VirtualService, In extends Software> 
	extends IsRelatedTo<Out, In> {

	public static final String NAME = "Demands"; // Demands.class.getSimpleName();
	
}
