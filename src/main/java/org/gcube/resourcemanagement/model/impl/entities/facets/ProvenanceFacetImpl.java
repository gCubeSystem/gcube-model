/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.ProvenanceFacet;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=ProvenanceFacet.NAME)
public class ProvenanceFacetImpl extends FacetImpl implements ProvenanceFacet {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 1014553736569877775L;
	
	protected Relationship relationship;
	protected String reference;
	protected ValueSchema document;	
	
	/**
	 * @return the relationship
	 */
	@Override
	public Relationship getRelationship() {
		return relationship;
	}
	
	/**
	 * @param relationship the relationship to set
	 */
	@Override
	public void setRelationship(Relationship relationship) {
		this.relationship = relationship;
	}
	
	/**
	 * @return the reference
	 */
	@Override
	public String getReference() {
		return reference;
	}
	
	/**
	 * @param reference the reference to set
	 */
	@Override
	public void setReference(String reference) {
		this.reference = reference;
	}
	/**
	 * @return the document
	 */
	@Override
	public ValueSchema getDocument() {
		return document;
	}
	
	/**
	 * @param document the document to set
	 */
	@Override
	public void setDocument(ValueSchema document) {
		this.document = document;
	}
	
}
