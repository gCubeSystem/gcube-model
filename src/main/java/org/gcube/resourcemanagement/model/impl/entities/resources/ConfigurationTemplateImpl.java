/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConfigurationTemplate;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=ConfigurationTemplate.NAME)
public class ConfigurationTemplateImpl extends GCubeResourceImpl implements ConfigurationTemplate {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 7118678229898232442L;
	
}
