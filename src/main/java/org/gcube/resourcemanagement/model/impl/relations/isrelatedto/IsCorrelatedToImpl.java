package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsCorrelatedTo;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = IsCorrelatedTo.NAME)
public class IsCorrelatedToImpl<Out extends Dataset, In extends Dataset>
		extends IsRelatedToImpl<Out, In> implements IsCorrelatedTo<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -794936403415195012L;

	protected IsCorrelatedToImpl() {
		super();
	}

	public IsCorrelatedToImpl(Out source, In target) {
		super(source, target);
	}
	
	public IsCorrelatedToImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
