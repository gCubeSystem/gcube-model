package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.CallsFor;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = CallsFor.NAME)
public class CallsForImpl<Out extends Service, In extends Service> extends
		IsRelatedToImpl<Out, In> implements CallsFor<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1207199725579272666L;

	protected CallsForImpl() {
		super();
	}

	public CallsForImpl(Out source, In target) {
		super(source, target);
	}
	
	public CallsForImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
