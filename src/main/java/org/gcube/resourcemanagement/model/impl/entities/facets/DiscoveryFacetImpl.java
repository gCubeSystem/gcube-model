/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import java.util.ArrayList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.informationsystem.queries.templates.reference.properties.QueryTemplateReference;
import org.gcube.resourcemanagement.model.reference.entities.facets.DiscoveryFacet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=DiscoveryFacet.NAME)
public class DiscoveryFacetImpl extends FacetImpl implements DiscoveryFacet {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -5874007308714407993L;
	
	protected String group;
	protected String description;
	protected int min;
	protected int max;
	protected ArrayNode queries;
	protected List<QueryTemplateReference> queryTemplates;

	@Override
	public String getGroup() {
		return group;
	}

	@Override
	public void setGroup(String group) {
		this.group = group;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int getMin() {
		return min;
	}
	
	@Override
	public void setMin(int min) {
		this.min = min;
	}
	
	@Override
	public Integer getMax() {
		return max;
	}
	
	@Override
	public void setMax(Integer max) {
		this.max = max;
	}
	
	@Override
	public ArrayNode getQueries() {
		return queries;
	}
	
	@Override
	public void setQueries(ArrayNode queries) {
		this.queries = queries;
	}

	@Override
	public void addQuery(ObjectNode query) {
		if(this.queries == null) {
			ObjectMapper mapper = new ObjectMapper();
			this.queries = mapper.createArrayNode();
		}
		this.queries.add(query);
	}

	@Override
	public List<QueryTemplateReference> getQueryTemplates() {
		return queryTemplates;
	}

	@Override
	public void setQueryTemplates(List<QueryTemplateReference> queryTemplates) {
		this.queryTemplates = queryTemplates;
	}

	@Override
	public void addQueryTemplates(QueryTemplateReference queryTemplates) {
		if(this.queryTemplates == null) {
			this.queryTemplates = new ArrayList<QueryTemplateReference>();
		}
		this.queryTemplates.add(queryTemplates);
	}
	
}
