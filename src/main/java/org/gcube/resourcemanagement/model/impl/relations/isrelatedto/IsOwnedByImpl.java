package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;
import org.gcube.resourcemanagement.model.reference.entities.resources.Site;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsOwnedBy;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = IsOwnedBy.NAME)
public class IsOwnedByImpl<Out extends Site, In extends Actor> extends
		IsRelatedToImpl<Out, In> implements IsOwnedBy<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 8322983678141617096L;

	protected IsOwnedByImpl() {
		super();
	}

	public IsOwnedByImpl(Out source, In target) {
		super(source, target);
	}
	
	public IsOwnedByImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
