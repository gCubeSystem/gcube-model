package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasContributor;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasContributor.NAME)
public class HasContributorImpl<Out extends Resource, In extends ContactFacet>
		extends HasContactImpl<Out, In> implements HasContributor<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -3024614025389570960L;

	protected HasContributorImpl() {
		super();
	}

	public HasContributorImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasContributorImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
