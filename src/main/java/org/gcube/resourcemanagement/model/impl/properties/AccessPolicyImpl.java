/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.properties;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.properties.AccessPolicy;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=AccessPolicy.NAME)
public class AccessPolicyImpl extends GCubePropertyImpl implements AccessPolicy {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -531371754061410537L;

	protected ValueSchema policy;
	
	protected String note;
	
	public AccessPolicyImpl(){
		super();
	}
	
	@Override
	public ValueSchema getPolicy() {
		return this.policy;
	}

	@Override
	public void setPolicy(ValueSchema policy) {
		this.policy = policy;
	}

	@Override
	public String getNote() {
		return this.note;
	}

	@Override
	public void setNote(String note) {
		this.note = note;
	}

}
