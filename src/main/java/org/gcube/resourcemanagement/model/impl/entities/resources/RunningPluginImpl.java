/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.RunningPlugin;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=RunningPlugin.NAME)
public class RunningPluginImpl extends EServiceImpl implements RunningPlugin {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 7954507291742946502L;

}
