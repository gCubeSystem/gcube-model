/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.DescriptiveMetadataFacet;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=DescriptiveMetadataFacet.NAME)
public class DescriptiveMetadataFacetImpl extends FacetImpl implements DescriptiveMetadataFacet {
	
	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -8373583843673756878L;
	
	protected ValueSchema descriptiveMetadata;

	@Override
	public ValueSchema getDescriptiveMetadata() {
		return descriptiveMetadata;
	}

	@Override
	public void setDescriptiveMetadata(ValueSchema descriptiveMetadata) {
		this.descriptiveMetadata = descriptiveMetadata;
	}
	
}