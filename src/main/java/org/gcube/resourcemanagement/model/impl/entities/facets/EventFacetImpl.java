/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import java.util.Date;
import java.util.LinkedHashMap;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.informationsystem.model.impl.properties.EventImpl;
import org.gcube.informationsystem.model.reference.properties.Event;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = EventFacet.NAME)
public class EventFacetImpl extends FacetImpl implements EventFacet {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -4130548762073254058L;

	protected Event event;

	@Override
	@Deprecated
	/**
	 * {@inheritDoc}
	 * 
	 * This function is actually a shortcut to
	 * {@link EventFacet#getTheEvent()}.{@link Event#getWhen()}
	 */
	@JsonIgnore
	public Date getDate() {
		if(event==null) {
			return null;
		}
		return event.getWhen();
	}

	@Override
	@Deprecated
	/**
	 * {@inheritDoc}
	 * 
	 * This function is actually a shortcut to
	 * {@link EventFacet#getTheEvent()}.{@link Event#setWhen(Date date)}
	 * 
	 * It eventually instantiate {@link Event} if null.
	 */
	@JsonSetter("date")
	public void setDate(Date date) {
		if(event == null) {
			event = new EventImpl();
		}
		event.setWhen(date);
	}

	@Override
	@Deprecated
	/**
	 * {@inheritDoc} 
	 * 
	 * This function is actually a shortcut to
	 * {@link EventFacet#getTheEvent()}.{@link Event#getWhat()}
	 */
	@JsonIgnore
	public String getEvent() {
		if(event==null) {
			return null;
		}
		return event.getWhat();
	}

	@Override
	@Deprecated
	/**
	 * {@inheritDoc}
	 * 
	 * This function is actually a shortcut to
	 * {@link EventFacet#getTheEvent()}.{@link Event#setWhat(String what)}.
	 * 
	 * It eventually instantiate {@link Event} if null.
	 */
	@JsonIgnore
	public void setEvent(String event) {
		if(event == null) {
			this.event = new EventImpl();
		}
		this.event.setWhat(event);
	}

	@Override
	/**
	 * {@inheritDoc} 
	 */
	@JsonGetter(EventFacet.EVENT_PROPERTY)
	public Event getTheEvent() {
		return event;
	}

	@Override
	/**
	 * {@inheritDoc} 
	 */
	@JsonIgnore
	public void setEvent(Event event) {
		this.event = event;
	}
	
	@JsonSetter(EventFacet.EVENT_PROPERTY)
	protected void setGenericEvent(Object event) {
		if(event instanceof String) {
			setEvent((String) event);
		}
		
		if(event instanceof LinkedHashMap<?, ?>) {
			ObjectMapper objectMapper = ElementMapper.getObjectMapper();
			Event theEvent = objectMapper.convertValue(event, Event.class);
			setEvent(theEvent);
		}
	}

}