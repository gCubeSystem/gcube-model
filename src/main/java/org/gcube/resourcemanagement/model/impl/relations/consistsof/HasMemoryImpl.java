package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasMemory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasMemory.NAME)
public abstract class HasMemoryImpl<Out extends Resource, In extends MemoryFacet>
		extends ConsistsOfImpl<Out, In> implements HasMemory<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1950184335132510402L;

	protected HasMemoryImpl() {
		super();
	}

	public HasMemoryImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasMemoryImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
