package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasCurator;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasCurator.NAME)
public class HasCuratorImpl<Out extends Resource, In extends ContactFacet>
		extends HasContactImpl<Out, In> implements HasCurator<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 3557928110486108164L;

	protected HasCuratorImpl() {
		super();
	}

	public HasCuratorImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasCuratorImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
