package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasManager;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasManager.NAME)
public class HasManagerImpl<Out extends Resource, In extends ContactFacet>
		extends HasContactImpl<Out, In> implements HasManager<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7136048435035542411L;

	protected HasManagerImpl() {
		super();
	}

	public HasManagerImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasManagerImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
