package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Discovers;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Discovers.NAME)
public class DiscoversImpl<Out extends EService, In extends EService> extends
		CallsForImpl<Out, In> implements Discovers<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -1457307319196528691L;

	protected DiscoversImpl() {
		super();
	}

	public DiscoversImpl(Out source, In target) {
		super(source, target);
	}
	
	public DiscoversImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
