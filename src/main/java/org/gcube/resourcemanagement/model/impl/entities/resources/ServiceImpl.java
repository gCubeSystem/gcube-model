/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Service.NAME)
public abstract class ServiceImpl extends GCubeResourceImpl implements Service {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -6020679647779327575L;

	
}
