/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import java.net.URL;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.LicenseFacet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=LicenseFacet.NAME)
public class LicenseFacetImpl extends FacetImpl implements LicenseFacet {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 415765991191430747L;
	
	protected String name;
	protected URL textURL;
	
	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the textURL
	 */
	public URL getTextURL() {
		return textURL;
	}

	/**
	 * @param textURL the textURL to set
	 */
	public void setTextURL(URL textURL) {
		this.textURL = textURL;
	}

}
