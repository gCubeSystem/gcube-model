package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConcreteDataset;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsPartOf;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = IsPartOf.NAME)
public class IsPartOfImpl<Out extends ConcreteDataset, In extends Dataset>
		extends IsCorrelatedToImpl<Out, In> implements IsPartOf<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 322461779749769277L;

	protected IsPartOfImpl() {
		super();
	}

	public IsPartOfImpl(Out source, In target) {
		super(source, target);
	}
	
	public IsPartOfImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
