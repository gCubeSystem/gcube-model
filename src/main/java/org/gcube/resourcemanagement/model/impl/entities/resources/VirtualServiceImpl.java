/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualService;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=VirtualService.NAME)
public class VirtualServiceImpl extends ServiceImpl implements VirtualService {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 4784559176034478276L;

}
