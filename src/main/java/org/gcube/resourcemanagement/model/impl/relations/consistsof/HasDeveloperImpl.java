package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasDeveloper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasDeveloper.NAME)
public class HasDeveloperImpl<Out extends Resource, In extends ContactFacet>
		extends HasContactImpl<Out, In> implements HasDeveloper<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -4206418259355685096L;

	protected HasDeveloperImpl() {
		super();
	}

	public HasDeveloperImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasDeveloperImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
