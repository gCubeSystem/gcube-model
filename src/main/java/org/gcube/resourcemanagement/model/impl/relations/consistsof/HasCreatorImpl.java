package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasCreator;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasCreator.NAME)
public class HasCreatorImpl<Out extends Resource, In extends ContactFacet>
		extends HasContactImpl<Out, In> implements HasCreator<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7216613965814407960L;

	protected HasCreatorImpl() {
		super();
	}

	public HasCreatorImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasCreatorImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
