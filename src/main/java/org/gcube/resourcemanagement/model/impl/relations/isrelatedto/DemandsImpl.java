package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualService;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Demands;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Demands.NAME)
public class DemandsImpl<Out extends VirtualService, In extends Software>
		extends IsRelatedToImpl<Out, In> implements Demands<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 3303237315961154863L;

	protected DemandsImpl() {
		super();
	}

	public DemandsImpl(Out source, In target) {
		super(source, target);
	}
	
	public DemandsImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
