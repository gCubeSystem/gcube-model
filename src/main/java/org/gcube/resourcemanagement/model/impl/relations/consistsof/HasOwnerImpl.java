package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasOwner;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasOwner.NAME)
public class HasOwnerImpl<Out extends Resource, In extends ContactFacet>
		extends HasContactImpl<Out, In> implements HasOwner<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7980790874053216568L;

	protected HasOwnerImpl() {
		super();
	}

	public HasOwnerImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasOwnerImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
