/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=EService.NAME)
public class EServiceImpl extends ServiceImpl implements EService {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -1211338661607479729L;

}
