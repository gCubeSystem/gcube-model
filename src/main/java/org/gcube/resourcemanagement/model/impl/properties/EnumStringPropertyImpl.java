package org.gcube.resourcemanagement.model.impl.properties;

import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.properties.EnumStringProperty;

/**
 * Implementation for {@link EnumStringProperty}.
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
@JsonTypeName(value=EnumStringProperty.NAME)
public class EnumStringPropertyImpl extends GCubePropertyImpl implements EnumStringProperty {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 5453875438782091672L;
	
	private Set<String> type;
	private String value;
	
	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public Set<String> getSchema() {
		return this.type;
	}

	@Override
	public void setSchema(Set<String> type) {
		this.type = type;
	}

}
