/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.Schema;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Schema.NAME)
public class SchemaImpl extends GCubeResourceImpl implements Schema {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -2667306128901183874L;

	
}
