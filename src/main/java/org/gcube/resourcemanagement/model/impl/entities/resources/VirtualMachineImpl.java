/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.VirtualMachine;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=VirtualMachine.NAME)
public class VirtualMachineImpl extends ServiceImpl implements VirtualMachine {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 4432884828103841956L;
	
}
