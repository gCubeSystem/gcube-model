package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasVolatileMemory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasVolatileMemory.NAME)
public class HasVolatileMemoryImpl<Out extends Resource, In extends MemoryFacet>
		extends HasMemoryImpl<Out, In> implements HasVolatileMemory<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -3419282968469134859L;

	protected HasVolatileMemoryImpl() {
		super();
	}

	public HasVolatileMemoryImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasVolatileMemoryImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}
	
}
