/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Actor.NAME)
public abstract class ActorImpl extends GCubeResourceImpl implements Actor {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -7959825469925979101L;
	
}
