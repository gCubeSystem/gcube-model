package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Plugin;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsPluginOf;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = IsPluginOf.NAME)
public class IsPluginOfImpl<Out extends Plugin, In extends Software> extends DependsOnImpl<Out,In>
		implements IsPluginOf<Out,In> {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6552488952251949334L;

	protected IsPluginOfImpl() {
		super();
	}
	
	public IsPluginOfImpl(Out source, In target) {
		super(source, target);
	}
	
	public IsPluginOfImpl(Out source, In target, PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}
	
}
