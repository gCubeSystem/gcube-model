package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.DependsOn;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = DependsOn.NAME)
public class DependsOnImpl<Out extends Software, In extends Software> extends
		IsRelatedToImpl<Out, In> implements DependsOn<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6518567937425809896L;

	protected DependsOnImpl() {
		super();
	}

	public DependsOnImpl(Out source, In target) {
		super(source, target);
	}
	
	public DependsOnImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
