/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConfigurationTemplate;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsConfiguredBy;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = IsConfiguredBy.NAME)
public class IsConfiguredByImpl<Out extends Software, In extends ConfigurationTemplate>
		extends IsRelatedToImpl<Out, In> implements IsConfiguredBy<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -1819083557550259454L;

	protected IsConfiguredByImpl() {
		super();
	}

	public IsConfiguredByImpl(Out source, In target) {
		super(source, target);
	}
	
	public IsConfiguredByImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
