package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Enables;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Enables.NAME)
public class EnablesImpl<Out extends Service, In extends Software> extends
		IsRelatedToImpl<Out, In> implements Enables<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 3528290328413872086L;

	protected EnablesImpl() {
		super();
	}

	public EnablesImpl(Out source, In target) {
		super(source, target);
	}
	
	public EnablesImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
