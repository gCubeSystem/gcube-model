/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=IdentifierFacet.NAME)
public class IdentifierFacetImpl extends FacetImpl implements IdentifierFacet {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 5711870008624673728L;
	
	/*
	public static final Map<TypeVersion, String> CHANGELOG;
	
	static {
		CHANGELOG = TypeImpl.DEFAULT_CHANGELOG_MAP;
		CHANGELOG.put(new TypeVersion(IdentifierFacet.VERSION), "Added STRING in IdentificationType");
	}
	*/ 
	protected String value;
	protected IdentificationType type;
	protected boolean persistent;
	
	/**
	 * @return the value
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the type
	 */
	@Override
	public IdentificationType getIdentificationType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	@Override
	public void setIdentificationType(IdentificationType type) {
		this.type = type;
	}

	/**
	 * @return the persistent
	 */
	@Override
	public boolean isPersistent() {
		return persistent;
	}

	/**
	 * @param persistent the persistent to set
	 */
	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}

}
