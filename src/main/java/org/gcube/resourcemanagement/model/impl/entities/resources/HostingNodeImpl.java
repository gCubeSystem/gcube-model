/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=HostingNode.NAME)
public class HostingNodeImpl extends ServiceImpl implements HostingNode {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 4432884828103841956L;
	
}
