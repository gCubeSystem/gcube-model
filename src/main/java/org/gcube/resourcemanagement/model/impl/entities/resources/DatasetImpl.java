/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Dataset.NAME)
public class DatasetImpl extends GCubeResourceImpl implements Dataset {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -8344300098282501665L;
	
}
