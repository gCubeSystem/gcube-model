/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.Plugin;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Plugin.NAME)
public class PluginImpl extends SoftwareImpl implements Plugin {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 8531011342130252545L;

}
