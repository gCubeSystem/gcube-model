package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConfigurationTemplate;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsCustomizedBy;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = IsCustomizedBy.NAME)
public class IsCustomizedByImpl<Out extends Service, In extends ConfigurationTemplate>
		extends IsRelatedToImpl<Out, In> implements IsCustomizedBy<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1240330292571521L;

	protected IsCustomizedByImpl() {
		super();
	}

	public IsCustomizedByImpl(Out source, In target) {
		super(source, target);
	}
	
	public IsCustomizedByImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
