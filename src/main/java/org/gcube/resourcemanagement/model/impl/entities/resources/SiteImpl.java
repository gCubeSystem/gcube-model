/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.Site;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Site.NAME)
public class SiteImpl extends GCubeResourceImpl implements Site {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -6923303652448686159L;

}
