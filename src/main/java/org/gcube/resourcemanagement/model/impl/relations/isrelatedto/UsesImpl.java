package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Uses;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Uses.NAME)
public class UsesImpl<Out extends EService, In extends EService> extends
	CallsForImpl<Out, In> implements Uses<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6454443849562485949L;

	protected UsesImpl() {
		super();
	}

	public UsesImpl(Out source, In target) {
		super(source, target);
	}
	
	public UsesImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
