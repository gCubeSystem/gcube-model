/**
 * Implementation of common properties used across the gcube model.
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
package org.gcube.resourcemanagement.model.impl.properties;