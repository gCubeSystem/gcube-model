/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Software.NAME)
public class SoftwareImpl extends GCubeResourceImpl implements Software {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 190088853237846140L;

}
