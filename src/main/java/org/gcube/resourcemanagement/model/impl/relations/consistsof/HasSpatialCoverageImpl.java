package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.CoverageFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasSpatialCoverage;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasSpatialCoverage.NAME)
public class HasSpatialCoverageImpl<Out extends Resource, In extends CoverageFacet>
		extends HasCoverageImpl<Out, In> implements HasSpatialCoverage<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -6379008183604723927L;

	protected HasSpatialCoverageImpl() {
		super();
	}

	public HasSpatialCoverageImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasSpatialCoverageImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
