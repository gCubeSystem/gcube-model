package org.gcube.resourcemanagement.model.impl.properties;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.properties.PropertyImpl;
import org.gcube.resourcemanagement.model.reference.properties.GCubeProperty;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=GCubeProperty.NAME)
public class GCubePropertyImpl extends PropertyImpl implements GCubeProperty {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -6720338983130018971L;
	
}
