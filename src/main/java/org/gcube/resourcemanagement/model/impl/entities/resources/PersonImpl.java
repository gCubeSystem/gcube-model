/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.Person;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Person.NAME)
public class PersonImpl extends ActorImpl implements Person {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 8490450905022409272L;
	
}
