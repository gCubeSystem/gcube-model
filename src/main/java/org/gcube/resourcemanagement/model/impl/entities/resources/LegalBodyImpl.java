/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.LegalBody;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=LegalBody.NAME)
public class LegalBodyImpl extends ActorImpl implements LegalBody {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 6879797086260765029L;
	
}
