package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.LegalBody;
import org.gcube.resourcemanagement.model.reference.entities.resources.Person;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.BelongsTo;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = BelongsTo.NAME)
public class BelongsToImpl<Out extends Person, In extends LegalBody> extends
		IsRelatedToImpl<Out, In> implements BelongsTo<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -656383853602099681L;

	protected BelongsToImpl() {
		super();
	}

	public BelongsToImpl(Out source, In target) {
		super(source, target);
	}
	
	public BelongsToImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
