package org.gcube.resourcemanagement.model.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.discovery.RegistrationProvider;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.contexts.impl.entities.GCubeContext;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasContact;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GCubeModelRegistrationProvider implements RegistrationProvider {

	public static final String GCUBE_MODEL_NAME = "gCube Model";
	
	public <E extends Element, EImpl extends E> void registerContextOverride() {
		@SuppressWarnings("unchecked")
		Class<E> clz = (Class<E>) Context.class;
		@SuppressWarnings("unchecked")
		Class<EImpl> implClz = (Class<EImpl>) GCubeContext.class;
		ElementMapper.addDynamicAssociation(clz, implClz);
	}
	
	@Override
	public Collection<Package> getPackagesToRegister() {
		registerContextOverride();
		Set<Package> packages = new HashSet<>();
		packages.add(ValueSchema.class.getPackage());
		packages.add(SoftwareFacet.class.getPackage());
		packages.add(EService.class.getPackage());
		packages.add(Activates.class.getPackage());
		packages.add(HasContact.class.getPackage());
		return packages;
	}

	@Override
	public String getModelName() {
		return GCUBE_MODEL_NAME;
	}
	
}
