package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Involves;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Involves.NAME)
public class InvolvesImpl<Out extends Dataset, In extends Actor> extends
		IsRelatedToImpl<Out, In> implements Involves<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 9146386734851684864L;

	protected InvolvesImpl() {
		super();
	}

	public InvolvesImpl(Out source, In target) {
		super(source, target);
	}
	
	public InvolvesImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
