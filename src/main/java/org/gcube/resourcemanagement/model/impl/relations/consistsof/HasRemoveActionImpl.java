package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ActionFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasRemoveAction;

/**
 * An implementation of the {@link HasRemoveAction} relation.
 * 
 * @author Manuele Simi (ISTI CNR)
 * 
 */
@JsonTypeName(value = HasRemoveAction.NAME)
public class HasRemoveActionImpl<Out extends Service, In extends ActionFacet> extends HasActionImpl<Out,In>
		implements HasRemoveAction<Out,In> {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 5038048537520695788L;
	
	protected HasRemoveActionImpl() {
		super();
	}
	
	public HasRemoveActionImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasRemoveActionImpl(Out source, In target, PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}
}
