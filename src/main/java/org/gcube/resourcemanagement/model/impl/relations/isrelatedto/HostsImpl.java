package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.entities.resources.Site;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Hosts;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Hosts.NAME)
public class HostsImpl<Out extends Site, In extends Service> extends
		IsRelatedToImpl<Out, In> implements Hosts<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 9214228861851071392L;

	protected HostsImpl() {
		super();
	}

	public HostsImpl(Out source, In target) {
		super(source, target);
	}
	
	public HostsImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
