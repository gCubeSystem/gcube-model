/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=IsIdentifiedBy.NAME)
public class IsIdentifiedByImpl<S extends Resource, T extends Facet> extends
	ConsistsOfImpl<S, T> implements IsIdentifiedBy<S, T> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 8043666054054911145L;

	protected IsIdentifiedByImpl(){
		super();
	}

	public IsIdentifiedByImpl(S source, T target) {
		super(source, target);
	}
	
	public IsIdentifiedByImpl(S source, T target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
