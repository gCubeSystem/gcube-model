/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.SchemaFacet;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=SchemaFacet.NAME)
public class SchemaFacetImpl extends FacetImpl implements SchemaFacet {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -3443862680728281477L;
	
	protected String name;
	protected String description;
	protected ValueSchema schema;
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String getDescription() {
		return description;
	}
	
	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public ValueSchema getSchema() {
		return schema;
	}
	
	@Override
	public void setSchema(ValueSchema schema) {
		this.schema = schema;
	}

}
