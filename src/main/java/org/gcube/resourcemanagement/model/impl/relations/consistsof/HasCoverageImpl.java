package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.CoverageFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasCoverage;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasCoverage.NAME)
public abstract class HasCoverageImpl<Out extends Resource, In extends CoverageFacet>
		extends ConsistsOfImpl<Out, In> implements HasCoverage<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 4822679979500182850L;

	protected HasCoverageImpl() {
		super();
	}

	public HasCoverageImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasCoverageImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
