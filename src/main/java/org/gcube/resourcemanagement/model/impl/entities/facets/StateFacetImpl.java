/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.StateFacet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=StateFacet.NAME)
public class StateFacetImpl extends FacetImpl implements StateFacet {
	
	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -6149286208701421212L;
	
	protected String value;
	
	/**
	 * @return the value
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	@Override
	public void setValue(String value) {
		this.value = value;
	}

}
