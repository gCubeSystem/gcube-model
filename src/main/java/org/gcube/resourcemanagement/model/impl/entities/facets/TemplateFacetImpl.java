/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import java.util.ArrayList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.TemplateFacet;
import org.gcube.resourcemanagement.model.reference.properties.AttributeProperty;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=TemplateFacet.NAME)
public class TemplateFacetImpl extends FacetImpl implements TemplateFacet {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -2537789290477963345L;

	protected String name;
	protected String description;
	protected String targetType;
	protected List<AttributeProperty> properties;
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String getTargetType() {
		return targetType;
	}

	@Override
	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	@Override
	public List<AttributeProperty> getProperties() {
		return properties;
	}

	@Override
	public void setProperties(List<AttributeProperty> properties) {
		this.properties = properties;
	}

	@Override
	public void addProperty(AttributeProperty property) {
		if(this.properties==null) {
			this.properties = new ArrayList<>();
		}
		this.properties.add(property);
	}
	
}
