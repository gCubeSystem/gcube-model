/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConcreteDataset;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=ConcreteDataset.NAME)
public class ConcreteDatasetImpl extends DatasetImpl implements ConcreteDataset {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 5183624758026295787L;
	
}
