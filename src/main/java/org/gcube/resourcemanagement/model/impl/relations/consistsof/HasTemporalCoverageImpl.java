package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.CoverageFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasTemporalCoverage;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasTemporalCoverage.NAME)
public class HasTemporalCoverageImpl<Out extends Resource, In extends CoverageFacet>
		extends HasCoverageImpl<Out, In> implements
		HasTemporalCoverage<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -2842361366214219615L;

	protected HasTemporalCoverageImpl() {
		super();
	}

	public HasTemporalCoverageImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasTemporalCoverageImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}
	
}
