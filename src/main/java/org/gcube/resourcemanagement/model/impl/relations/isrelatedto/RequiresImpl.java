package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.entities.resources.Software;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Requires;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Requires.NAME)
public class RequiresImpl<Out extends Software, In extends Service> extends
		IsRelatedToImpl<Out, In> implements Requires<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -8258259681079991928L;

	protected RequiresImpl() {
		super();
	}

	public RequiresImpl(Out source, In target) {
		super(source, target);
	}
	
	public RequiresImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
