package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasPersistentMemory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasPersistentMemory.NAME)
public class HasPersistentMemoryImpl<Out extends Resource, In extends MemoryFacet>
		extends HasMemoryImpl<Out, In> implements HasPersistentMemory<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1964884925310764638L;

	protected HasPersistentMemoryImpl() {
		super();
	}

	public HasPersistentMemoryImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasPersistentMemoryImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
