/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;
import org.gcube.resourcemanagement.model.reference.entities.resources.Schema;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsCompliantWith;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = IsCompliantWith.NAME)
public class IsCompliantWithImpl<Out extends Dataset, In extends Schema>
		extends IsRelatedToImpl<Out, In> implements IsCompliantWith<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -3940951202209802935L;

	protected IsCompliantWithImpl() {
		super();
	}

	public IsCompliantWithImpl(Out source, In target) {
		super(source, target);
	}
	
	public IsCompliantWithImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
