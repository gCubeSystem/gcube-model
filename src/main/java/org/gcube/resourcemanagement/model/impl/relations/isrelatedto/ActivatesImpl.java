package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Activates.NAME)
public class ActivatesImpl<Out extends Service, In extends Service> extends
		IsRelatedToImpl<Out, In> implements Activates<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -2770215400873228920L;

	protected ActivatesImpl() {
		super();
	}

	public ActivatesImpl(Out source, In target) {
		super(source, target);
	}
	
	public ActivatesImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
