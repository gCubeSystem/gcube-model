package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasMaintainer;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasMaintainer.NAME)
public class HasMaintainerImpl<Out extends Resource, In extends ContactFacet>
		extends HasContactImpl<Out, In> implements HasMaintainer<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 3529906340392722967L;

	protected HasMaintainerImpl() {
		super();
	}

	public HasMaintainerImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasMaintainerImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
