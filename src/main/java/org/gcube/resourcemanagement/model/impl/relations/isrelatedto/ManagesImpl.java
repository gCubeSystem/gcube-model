package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Manages;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Manages.NAME)
public class ManagesImpl<Out extends Service, In extends Dataset> extends
		IsRelatedToImpl<Out, In> implements Manages<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -6919183339684717044L;

	protected ManagesImpl() {
		super();
	}

	public ManagesImpl(Out source, In target) {
		super(source, target);
	}

	public ManagesImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
