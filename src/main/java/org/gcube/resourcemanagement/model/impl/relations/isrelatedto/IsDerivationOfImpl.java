package org.gcube.resourcemanagement.model.impl.relations.isrelatedto;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.resources.Configuration;
import org.gcube.resourcemanagement.model.reference.entities.resources.ConfigurationTemplate;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.IsDerivationOf;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = IsDerivationOf.NAME)
public class IsDerivationOfImpl<Out extends Configuration, In extends ConfigurationTemplate>
		extends IsRelatedToImpl<Out, In> implements IsDerivationOf<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1249832537598284450L;

	protected IsDerivationOfImpl() {
		super();
	}

	public IsDerivationOfImpl(Out source, In target) {
		super(source, target);
	}

	
	public IsDerivationOfImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
