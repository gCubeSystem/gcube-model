/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.SubjectFacet;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=SubjectFacet.NAME)
public class SubjectFacetImpl extends FacetImpl implements SubjectFacet {
	
	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -266229852500187672L;

	protected ValueSchema subject;
	
	@Override
	public ValueSchema getSubject() {
		return subject;
	}

	@Override
	public void setSubject(ValueSchema subject) {
		this.subject = subject;
	}
	
}