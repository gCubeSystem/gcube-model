package org.gcube.resourcemanagement.model.impl.properties;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.properties.RegexProperty;

/**
 * Implementation for {@link RegexProperty}.
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
@JsonTypeName(value=RegexProperty.NAME)
public class RegexPropertyImpl extends GCubePropertyImpl implements RegexProperty {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -8279692341196771526L;

	private String value;
	private String type;
	
	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String getSchema() {
		return this.type;
	}

	@Override
	public void setSchema(String type) {
		this.type = type;
	}

}
