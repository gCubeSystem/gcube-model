package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasContact;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = HasContact.NAME)
public abstract class HasContactImpl<Out extends Resource, In extends ContactFacet>
		extends ConsistsOfImpl<Out, In> implements HasContact<Out, In> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6576049793532371473L;

	protected HasContactImpl() {
		super();
	}
	
	public HasContactImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasContactImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
