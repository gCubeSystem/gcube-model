package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ActionFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasAction;

/**
 * An implementation of the {@link HasAction} relation.
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
@JsonTypeName(value = HasAction.NAME)
public class HasActionImpl<Out extends Service, In extends ActionFacet> 
	extends ConsistsOfImpl<Out, In> implements HasAction<Out, In> {
	 
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -1028958506776410453L;

	protected HasActionImpl() {
		super();
	}

	public HasActionImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasActionImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}

}
