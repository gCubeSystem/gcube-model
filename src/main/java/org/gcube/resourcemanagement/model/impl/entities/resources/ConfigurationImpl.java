/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.resources;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.resourcemanagement.model.reference.entities.resources.Configuration;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Configuration.NAME)
public class ConfigurationImpl extends ConfigurationTemplateImpl implements Configuration {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -5517329395666754079L;
	
}
