/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.SimpleFacet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=SimpleFacet.NAME)
public class SimpleFacetImpl extends FacetImpl implements SimpleFacet {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 3217017583429546546L;
	
}
