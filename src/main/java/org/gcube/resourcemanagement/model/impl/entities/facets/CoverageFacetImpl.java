/**
 * 
 */
package org.gcube.resourcemanagement.model.impl.entities.facets;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CoverageFacet;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=CoverageFacet.NAME)
public class CoverageFacetImpl extends FacetImpl implements CoverageFacet {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -7590997566136002521L;
	
	protected ValueSchema coverage;

	@Override
	public ValueSchema getCoverage() {
		return coverage;
	}

	@Override
	public void setCoverage(ValueSchema coverage) {
		this.coverage = coverage;
	}

}