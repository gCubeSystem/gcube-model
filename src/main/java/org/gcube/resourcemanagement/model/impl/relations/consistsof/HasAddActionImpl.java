package org.gcube.resourcemanagement.model.impl.relations.consistsof;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.resourcemanagement.model.reference.entities.facets.ActionFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasAddAction;

/**
 * An implementation of the {@link HasAddAction} relation.
 * 
 * @author Manuele Simi (ISTI CNR)
 *
 */
@JsonTypeName(value = HasAddAction.NAME)
public class HasAddActionImpl<Out extends Service, In extends ActionFacet>
	extends HasActionImpl<Out, In> implements HasAddAction<Out, In>{
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -1255378439187531889L;

	protected HasAddActionImpl() {
		super();
	}
	
	public HasAddActionImpl(Out source, In target) {
		super(source, target);
	}
	
	public HasAddActionImpl(Out source, In target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}
}
