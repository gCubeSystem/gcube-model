package org.gcube.resourcemanagement.model.impl.entities.resources;

import java.util.ArrayList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.entities.ResourceImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.resourcemanagement.model.reference.entities.resources.GCubeResource;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=GCubeResource.NAME)
public abstract class GCubeResourceImpl extends ResourceImpl implements GCubeResource {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = 8005284153516839231L;

	@Override
	public List<? extends Facet> getIdentificationFacets() {
		List<Facet> identificationFacets = new ArrayList<>();
		for(ConsistsOf<? extends Resource, ? extends Facet> consistsOfInstance : consistsOfList){
			if (IsIdentifiedBy.class.isAssignableFrom(consistsOfInstance.getClass())) {
				identificationFacets.add(consistsOfInstance.getTarget());
			}
		}
		return identificationFacets;
	}
	
}
