#############################
gCube Model
#############################


gCube Model is a resource model built on top of the Information System (IS) Model which captures the different aspects of the resources and their relations playing significant roles in a research infrastructure empowered by gCube. 


This Model is part of research conducted in the context of a PhD. This wiki page represents just a partial view of the full rationale of the research. To have a complete overview of the rationale of the model, please refer to the PhD thesis which is publicly available at:

https://etd.adm.unipi.it/t/etd-05102019-114151/

https://openportal.isti.cnr.it/doc?id=people______::470484e51fcb9e307a418c800efc44c8

If you need to refer to such work you can cite the PhD Thesis.

.. code:: 

	@phdthesis{frosini2019transactional,
	  title={Transactional REST Information System for Federated Research Infrastructures enabling Virtual Research Environments},
	  author={Frosini, Luca},
	  year={2019},
	  school={UNIVERSITÀ DI PISA}
	}

An previous paper about this work is:



.. code:: 

	@article{frosini2018facet,
	  title={A Facet-based Open and Extensible Resource Model for Research Data Infrastructures.},
	  author={Frosini, Luca and Pagano, Pasquale},
	  journal={Grey Journal (TGJ)},
	  volume={14},
	  number={2},
	  year={2018}
	}


Overview of gCube Model Resources and (isRelatedTo) Relations
=============================================================


.. image:: ./gcube-reources-and-isrelatedto-relations.png



.. toctree::

	gcube-model-property.rst
	gcube-model-facet.rst
	gcube-model-resource.rst
	gcube-model-consistsof.rst	
	gcube-model-isrelatedto.rst
