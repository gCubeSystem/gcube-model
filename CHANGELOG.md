This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Model

## [v5.1.0-SNAPSHOT]

- Added AttributeProperty [#27639]
- Added TemplateFacet [#27639]
- Added specific implementation of Context, i.e. GCubeContext
- Changed EventFacent to use the new Event property defined in the information-system-model
- Added DiscoveryFacet
- Added QueryTemplateReference Property


## [v5.0.0]

- DescriptiveMetadataFacet methods has been renamed to comply with reorganized E/R format [#24992]


## [v4.1.0]

- Added model name to registration provider


## [v4.0.0]

- Aligned model with the model defined in Luca Frosini PhD dissertation [#20367]
- Using annotation to document the types with name, description, version and changelog [#20306][#20315]
- Upgraded gcube-bom version to 2.0.1
- Added STRING as option for IdentificationType in IdentificationFacet 


## [v3.0.0] [r4.26.0] - 2020-10-29

- Switched JSON management to gcube-jackson [#19116]


## [v2.0.0] [r4.21.0] - 2020-03-30

- Fixed distro files and pom according to new release procedure
- Reorganized packages to be aligned with IS Model organization 


## [v1.1.0] [r4.14.0] - 2019-09-11

- Refactored code to support renaming of Embedded class and to Property [#13274]
- Added Resources Schema definition [#18213]
- Moved here some Embedded Type initially defined in Information System Model


## [v1.0.0] [r4.13.0] - 2018-11-20

- First Release. The component is a renaming of org.gcube.information-system.gcube-resources which should be 2.0.0

